 ohos-image-picker
====================

### 项目介绍
+ 项目名称：ohos-image-picker
+ 所属系列：openharmony的第三方组件适配移植
+ 功能：图片选择器图片来源相册和相机拍摄
+ 项目移植状态：主功能完成
+ 调用差异：无
+ 开发版本：sdk6，DevEco Studio2.2 beta1
+ 基线版本： release 2.4.7 


### 效果演示


![](https://images.gitee.com/uploads/images/2021/0728/155020_fad3d61a_1264960.gif)
![](https://images.gitee.com/uploads/images/2021/0728/155113_9484e1fc_1264960.gif)
![](https://images.gitee.com/uploads/images/2021/0728/155129_bb11b417_1264960.gif)



### 安装教程

**1)** 在项目根目录下的build.gradle文件中，

```
allprojects {
    repositories {
         maven {
                    url 'https://s01.oss.sonatype.org/content/repositories/releases/'
                }
    }
}
```
**2)** 在entry模块的build.gradle文件中，
```
 dependencies {
    implementation('com.gitee.chinasoft_ohos:ohos-image-picker_imagepicker:1.0.0')
    implementation('com.gitee.chinasoft_ohos:ohos-image-picker_rximagepicker:1.0.0')
 }
```
在sdk6，DevEco Studio2.2 Beta1下项目可直接运行 如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件， 并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下
### 使用说明
初始化
```java
     public ImagePicker getImagePicker() {
         boolean returnAfterCapture = ef_switch_return_after_capture.isChecked();
         boolean isSingleMode = ef_switch_single.isChecked();
         boolean useCustomImageLoader = ef_switch_imageloader.isChecked();
         boolean folderMode = ef_switch_folder_mode.isChecked();
         boolean includeVideo = ef_switch_include_video.isChecked();
         boolean onlyVideo = ef_switch_only_video.isChecked();
         boolean isExclude = ef_switch_include_exclude.isChecked();
         ImagePicker imagePicker = ImagePicker.create(this)
                 .language("in_IN") // Set image picker language
                 .returnMode(returnAfterCapture ? ReturnMode.ALL : ReturnMode.NONE) // set whether pick action or camera action should return immediate result or not. Only works in single mode for image picker
                 .folderMode(folderMode) // set folder mode (false by default)
                 .includeVideo(includeVideo) // include video (false by default)
                 .onlyVideo(onlyVideo) // include video (false by default)
                 .toolbarArrowColor(Color.RED.getValue()) // set toolbar arrow up color
                 .toolbarFolderTitle("Folder") // folder selection title
                 .toolbarImageTitle("Tap to select") // image selection title
                 .toolbarDoneButtonText("DONE");// done button text
         if (useCustomImageLoader) {
             ImagePickerComponentHolder.getInstance().setImageLoader(new GrayscaleImageLoader());
         } else {
             ImagePickerComponentHolder.getInstance().setImageLoader(new DefaultImageLoader());
         }
         if (isSingleMode) {
             imagePicker.single();
         } else {
             imagePicker.multi(); // multi mode (default mode)
         }
         if (isExclude) {
             imagePicker.exclude(ZSONObject.toZSONString(images)); // don't show anything on this selected images
         } else {
             imagePicker.origin(ZSONObject.toZSONString(images)); // original selected images, used in multi mode
         }
         return imagePicker.limit(10) // max images can be selected (99 by default)
                 .showCamera(true) // show camera or not (true by default)
                 .imageDirectory("Camera") // captured image directory name ("Camera" folder by default)
                 .imageFullDirectory(getCacheDir() + Environment.DIRECTORY_PICTURES); // can be full path
     }
```
验证
```java
private void start() {
        ImagePicker imagePicker = getImagePicker();
        imagePicker.start();
    }

    private void startWithIntent() {
        ImagePicker imagePicker = getImagePicker();
        startAbilityForResult(imagePicker.getIntent(this), IpCons.RC_IMAGE_PICKER);
    }

    private void captureImage() {
        ImagePicker.cameraOnly().start(this);
    }


    private void startCustomUI() {
        ImagePicker imagePicker = getImagePicker();
        ImagePickerConfig config = imagePicker.getConfig();
        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withBundleName(getBundleName())
                .withAbilityName(CustomUIAbility.class.getName())
                .build();
        intent.setOperation(operation);

        intent.setParam(ImagePickerConfig.class.getName(), config);
        startAbilityForResult(intent, IpCons.RC_IMAGE_PICKER);
    }
```

Rx
```java
 private Observable<List<Image>> getImagePickerObservable() {
        return RxImagePicker.getInstance()
                .start(this, ImagePicker.create(this));// max images can be selected (99 by default)
    }


 imagePickerObservable = getImagePickerObservable();
                imagePickerObservable.forEach(new Action1<List<Image>>() {
                    @Override
                    public void call(List<Image> images) {
                        printImages((ArrayList<Image>) images);
                    }
                });
```
### 测试信息
CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异



### 版本迭代
- 1.0.0
+ 0.0.1-SNAPSHOT

## 版权和许可信息 ##

```
MIT @ Esa Firman
```

