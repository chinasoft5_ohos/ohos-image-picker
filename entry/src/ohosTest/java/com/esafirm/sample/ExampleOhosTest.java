package com.esafirm.sample;

import com.esafirm.imagepicker.helper.ImagePickerUtils;
import com.esafirm.imagepicker.model.Image;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * ExampleOhosTest
 */
public class ExampleOhosTest {
    /**
     * testBundleName
     */
    @Test
    public void testBundleName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("com.esafirm.sample", actualBundleName);
    }

    /**
     * testSetAnGet
     */
    @Test
    public void testSetAnGet() {
        Image image = new Image();
        String path = "absoluthpath";
        image.setPath(path);
        assertEquals(path, image.getPath());
    }

    /**
     * testStrIsEmpty
     */
    @Test
    public void testStrIsEmpty() {
        String str = "";
        boolean stringEmpty = ImagePickerUtils.isStringEmpty(str);
        assertTrue(stringEmpty);
    }

    /**
     * testStrIsNotEmpty
     */
    @Test
    public void testStrIsNotEmpty() {
        String str = "不是空的";
        boolean stringEmpty = !ImagePickerUtils.isStringEmpty(str);
        assertTrue(stringEmpty);
    }
}