package com.esafirm.sample;

import com.esafirm.imagepicker.features.*;
import com.esafirm.imagepicker.features.imageloader.DefaultImageLoader;
import com.esafirm.imagepicker.helper.LogUtil;
import com.esafirm.imagepicker.helper.ResourceUtil;
import com.esafirm.imagepicker.model.Image;
import com.esafirm.imagepicker.view.ToolBar;
import com.esafirm.rximagepicker.RxImagePicker;

import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;

import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.Switch;
import ohos.agp.components.Text;
import ohos.agp.utils.Color;
import ohos.app.Environment;

import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import ohos.utils.zson.ZSONObject;

import rx.Observable;

import rx.functions.Action1;

import java.io.IOException;

import java.util.ArrayList;
import java.util.List;

/**
 * MainAbility
 */
public class MainAbility extends FractionAbility {
    private Switch efSwitchReturnAfterCapture;
    private Switch efSwitchSingle;
    private Switch efSwitchImageloader;
    private Switch efSwitchFolderMode;
    private Switch efSwitchIncludeVideo;
    private Switch efSwitchOnlyVideo;
    private Switch efSwitchIncludeExclude;
    private ArrayList<Image> images = new ArrayList<>();
    private Text text;
    private Observable<List<Image>> imagePickerObservable;
    private MainFragment mainFragment;
    private Button buttonPickImage;
    private Button buttonPickImageRx;
    private Button buttonIntent;
    private Button buttonCamera;
    private Button buttonCustomUi;
    private Button buttonLaunchFragment;
    private ToolBar actionBar;

    @Override
    public void onStart(Intent intent) {
        try {
            int color = getResourceManager().getElement(com.esafirm.imagepicker.ResourceTable.Color_status_bar_color).getColor();
            getWindow().setStatusBarColor(color);
        } catch (IOException | NotExistException | WrongTypeException e) {
            LogUtil.loge(e.getMessage());
        }
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_main);
        initView();
        actionBar.isTitle("Image Picker Sample");
        setLisenter();
    }

    private void setLisenter() {
        buttonPickImage.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                start();
            }
        });
        buttonPickImageRx.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                toRxImage();
            }
        });
        buttonIntent.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                startWithIntent();
            }
        });
        buttonCamera.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                captureImage();
            }
        });
        buttonCustomUi.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                startCustomUI();
            }
        });
        buttonLaunchFragment.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                toLaunchFragment();
            }
        });
    }

    private void toLaunchFragment() {
        mainFragment = new MainFragment();
        getFractionManager()
                .startFractionScheduler()
                .add(ResourceTable.Id_fragment_container, mainFragment)
                .show(mainFragment)
                .submit();
    }

    private void toRxImage() {
        imagePickerObservable = getImagePickerObservable();
        imagePickerObservable.forEach(new Action1<List<Image>>() {
            @Override
            public void call(List<Image> images) {
                printImages((ArrayList<Image>) images);
            }
        });
    }

    private void initView() {
        buttonPickImage = (Button) findComponentById(ResourceTable.Id_button_pick_image);
        buttonPickImageRx = (Button) findComponentById(ResourceTable.Id_button_pick_image_rx);
        buttonIntent = (Button) findComponentById(ResourceTable.Id_button_intent);
        buttonCamera = (Button) findComponentById(ResourceTable.Id_button_camera);
        buttonCustomUi = (Button) findComponentById(ResourceTable.Id_button_custom_ui);
        buttonLaunchFragment = (Button) findComponentById(ResourceTable.Id_button_launch_fragment);
        actionBar = (ToolBar) findComponentById(ResourceTable.Id_toolbar);
        efSwitchReturnAfterCapture = (Switch) findComponentById(ResourceTable.Id_ef_switch_return_after_capture);
        efSwitchSingle = (Switch) findComponentById(ResourceTable.Id_ef_switch_single);
        efSwitchImageloader = (Switch) findComponentById(ResourceTable.Id_ef_switch_imageloader);
        efSwitchFolderMode = (Switch) findComponentById(ResourceTable.Id_ef_switch_folder_mode);
        efSwitchIncludeVideo = (Switch) findComponentById(ResourceTable.Id_ef_switch_include_video);
        efSwitchOnlyVideo = (Switch) findComponentById(ResourceTable.Id_ef_switch_only_video);
        efSwitchIncludeExclude = (Switch) findComponentById(ResourceTable.Id_ef_switch_include_exclude);
        text = (Text) findComponentById(ResourceTable.Id_text_view);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    private void start() {
        ImagePicker imagePicker = getImagePicker();
        imagePicker.start();
    }

    private void startWithIntent() {
        ImagePicker imagePicker = getImagePicker();
        startAbilityForResult(imagePicker.getIntent(this), IpCons.RC_IMAGE_PICKER);
    }

    private void captureImage() {
        ImagePicker.cameraOnly().start(this);
    }

    private void startCustomUI() {
        ImagePicker imagePicker = getImagePicker();
        ImagePickerConfig config = imagePicker.getConfig();
        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withBundleName(getBundleName())
                .withAbilityName(CustomUIAbility.class.getName())
                .build();
        intent.setOperation(operation);

        intent.setParam(ImagePickerConfig.class.getName(), config);
        startAbilityForResult(intent, IpCons.RC_IMAGE_PICKER);
    }

    /**
     * getImagePicker
     *
     * @return ImagePicker
     */
    public ImagePicker getImagePicker() {
        boolean returnAfterCapture = efSwitchReturnAfterCapture.isChecked();
        boolean isSingleMode = efSwitchSingle.isChecked();
        boolean useCustomImageLoader = efSwitchImageloader.isChecked();
        boolean folderMode = efSwitchFolderMode.isChecked();
        boolean includeVideo = efSwitchIncludeVideo.isChecked();
        boolean onlyVideo = efSwitchOnlyVideo.isChecked();
        boolean isExclude = efSwitchIncludeExclude.isChecked();
        ImagePicker imagePicker = ImagePicker.create(this)
                .language("in_IN") // Set image picker language
                .returnMode(returnAfterCapture ? ReturnMode.ALL : ReturnMode.NONE) // set whether pick action or camera action should return immediate result or not. Only works in single mode for image picker
                .folderMode(folderMode) // set folder mode (false by default)
                .includeVideo(includeVideo) // include video (false by default)
                .onlyVideo(onlyVideo) // include video (false by default)
                .toolbarArrowColor(Color.RED.getValue()) // set toolbar arrow up color
                .toolbarFolderTitle("Folder") // folder selection title
                .toolbarImageTitle("Tap to select") // image selection title
                .toolbarDoneButtonText("DONE");// done button text
        if (useCustomImageLoader) {
            ImagePickerComponentHolder.getInstance().setImageLoader(new GrayscaleImageLoader());
        } else {
            ImagePickerComponentHolder.getInstance().setImageLoader(new DefaultImageLoader());
        }
        if (isSingleMode) {
            imagePicker.single();
        } else {
            imagePicker.multi(); // multi mode (default mode)
        }
        if (isExclude) {
            imagePicker.exclude(ZSONObject.toZSONString(images)); // don't show anything on this selected images
        } else {
            imagePicker.origin(ZSONObject.toZSONString(images)); // original selected images, used in multi mode
        }
        return imagePicker.limit(10) // max images can be selected (99 by default)
                .showCamera(true) // show camera or not (true by default)
                .imageDirectory("Camera") // captured image directory name ("Camera" folder by default)
                .imageFullDirectory(getCacheDir() + Environment.DIRECTORY_PICTURES); // can be full path
    }

    @Override
    protected void onAbilityResult(int requestCode, int resultCode, Intent resultData) {
        if (mainFragment != null) {
            mainFragment.onAbilityResult(requestCode, resultCode, resultData);
        } else {
        }
        if (ImagePicker.shouldHandle(requestCode, resultCode, resultData) || (requestCode == IpCons.RC_IMAGE_CAMERA)) {
            if (resultData != null) {
                images.clear();
                images.addAll(ImagePicker.getImages(resultData));
                printImages(images);
                return;
            } else {
            }
        } else {
        }
        super.onAbilityResult(requestCode, resultCode, resultData);

    }

    private void printImages(ArrayList<Image> images) {
        text.setText("");
        for (int i = 0; i < images.size(); i++) {
            text.append(images.get(i).getUriSchema());
            text.append(ResourceUtil.getText(this, ResourceTable.String_empty_ge));
        }
    }

    private Observable<List<Image>> getImagePickerObservable() {
        return RxImagePicker.getInstance()
                .start(this, ImagePicker.create(this));// max images can be selected (99 by default)
    }
}
