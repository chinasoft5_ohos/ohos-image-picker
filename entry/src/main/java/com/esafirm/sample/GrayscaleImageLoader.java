package com.esafirm.sample;

import com.bumptech.glide.GenericTransitionOptions;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import com.esafirm.imagepicker.features.imageloader.ImageLoader;
import com.esafirm.imagepicker.features.imageloader.ImageType;
import com.esafirm.imagepicker.model.Image;

import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;

/**
 * GrayscaleImageLoader
 */
public class GrayscaleImageLoader implements ImageLoader {
    RequestOptions requestOptions = new RequestOptions().transform(new GrayscaleTransformation());

    @Override
    public void loadImage(Image image, ohos.agp.components.Image imageView, ImageType imageType) {
        new EventHandler(EventRunner.getMainEventRunner()).postTask(new Runnable() {
            @Override
            public void run() {
                Glide.with(imageView.getContext())
//                .asBitmap()
                        .load(image.getUriSchema())
                        .transition(GenericTransitionOptions.withNoTransition())
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .skipMemoryCache(false)
                        .centerCrop()
                        .apply(requestOptions)
                        .into(imageView);
            }
        });
    }
}