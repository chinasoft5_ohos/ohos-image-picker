package com.esafirm.sample;

import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool;
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation;

import ohos.agp.render.*;

import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Size;

import org.jetbrains.annotations.NotNull;

import java.security.MessageDigest;

/**
 * GrayscaleTransformation
 */
public class GrayscaleTransformation extends BitmapTransformation {
    @Override
    protected PixelMap transform(@NotNull BitmapPool bitmapPool, @NotNull PixelMap pixelMaps, int i, int i1) {
        Size size = pixelMaps.getImageInfo().size;
        int width = size.width;
        int height = size.height;
        PixelFormat config = null;
        if (pixelMaps.getImageInfo().pixelFormat != null) {
            config = pixelMaps.getImageInfo().pixelFormat;
        }else {
            config = PixelFormat.ARGB_8888;
        }
        PixelMap bitmap = bitmapPool.get(width, height, config);
        Canvas canvas = new Canvas(new Texture(bitmap));
        ColorMatrix saturation = new ColorMatrix();
        saturation.setSaturation(0f);
        Paint paint = new Paint();
//        paint.setColorFilter(new ColorFilter(saturation));
        paint.setColorMatrix(saturation);
        canvas.drawPixelMapHolder(new PixelMapHolder(pixelMaps), 0f, 0f, paint);
        return bitmap;
    }

    @Override
    public void updateDiskCacheKey(@NotNull MessageDigest messageDigest) {
        messageDigest.update("GrayscaleTransformation()".getBytes());
    }
}
