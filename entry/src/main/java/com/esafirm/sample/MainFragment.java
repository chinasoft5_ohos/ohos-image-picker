package com.esafirm.sample;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.features.ReturnMode;

import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;

import ohos.agp.components.*;

import java.util.List;

/**
 * MainFragment
 */
public class MainFragment extends Fraction {
    private Button buttonPickFragment;
    private Button buttonClose;
    private Image imgFragment;
    private Component view;

    @Override
    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        LayoutScatter localInflater = LayoutScatter.getInstance(container.getContext());
        Component result = localInflater.parse(ResourceTable.Layout_fraction_main, container, false);
        setupView(result);
        return result;
    }

    private void setupView(Component rootView) {
        buttonPickFragment = (Button) rootView.findComponentById(ResourceTable.Id_button_pick_fragment);
        buttonClose = (Button) rootView.findComponentById(ResourceTable.Id_button_close);
        imgFragment = (Image) rootView.findComponentById(ResourceTable.Id_img_fragment);
        view = rootView.findComponentById(ResourceTable.Id_view);
        buttonPickFragment.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                ImagePicker.create(MainFragment.this)
                        .returnMode(ReturnMode.ALL) // set whether pick action or camera action should return immediate result or not. Only works in single mode for image picker
                        .folderMode(true) // set folder mode (false by default)
                        .single()
                        .toolbarFolderTitle("Folder") // folder selection title
                        .toolbarImageTitle("Tap to select")
                        .toolbarDoneButtonText("DONE") // done button text
                        .start(0);// image selection title
            }
        });
        buttonClose.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                getFractionAbility()
                        .getFractionManager()
                        .startFractionScheduler()
                        .remove(MainFragment.this)
                        .submit();
            }
        });
        view.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {

            }
        });
    }

    /**
     * Check if the captured image is stored successfully
     * Then reload data
     *
     * @param requestCode int
     * @param resultCode  int
     * @param resultData  Intent
     */
    public void onAbilityResult(int requestCode, int resultCode, Intent resultData) {
        List<com.esafirm.imagepicker.model.Image> images = ImagePicker.getImages(resultData);
        if (images != null && !images.isEmpty()) {
            Glide.with(imgFragment.getContext())
                    .load(images.get(0).getUriSchema())
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .into(imgFragment);
        }
    }
}
