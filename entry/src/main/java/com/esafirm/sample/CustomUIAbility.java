package com.esafirm.sample;

import com.bumptech.glide.Glide;

import com.esafirm.imagepicker.features.ImagePickerConfig;
import com.esafirm.imagepicker.features.ImagePickerFragment;
import com.esafirm.imagepicker.features.ImagePickerInteractionListener;
import com.esafirm.imagepicker.features.cameraonly.CameraOnlyConfig;
import com.esafirm.imagepicker.view.ToolBar;

import ohos.aafwk.ability.fraction.FractionAbility;

import ohos.agp.components.Image;

import ohos.aafwk.content.Intent;

import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;

import java.util.List;

import static com.esafirm.imagepicker.features.ImagePicker.*;

/**
 * CustomUIAbility
 */
public class CustomUIAbility extends FractionAbility {
    /**
     * actionBar
     */
    public ToolBar actionBar;

    private ImagePickerFragment imagePickerFragment;
    private CameraOnlyConfig cameraOnlyConfig = null;
    private ImagePickerConfig config = null;
    private int theme;
    private Image photoPreview;

    private ToolBar.ClickListener clickListener = new ToolBar.ClickListener() {
        @Override
        public void click(int component) {
            switch (component) {
                case 1:
                    onBackPressed();
                    break;
                case 2:
                    imagePickerFragment.captureImageWithPermission();
                    break;
                case 3:
                    imagePickerFragment.onDone();
                    break;
                default:
                    break;
            }
        }
    };
    private String startType;
    private CustomInteractionListener customInteractionListener;


    @Override
    public void onStart(Intent intent) {
        try {
            int color = getResourceManager().getElement(com.esafirm.imagepicker.ResourceTable.Color_status_bar_color).getColor();
            getWindow().setStatusBarColor(color);
        } catch (IOException | NotExistException | WrongTypeException e) {
            e.printStackTrace();
        }
        super.onStart(intent);
        setResult(RESULT_CANCELED, null);
        if (intent == null) {
            terminateAbility();
            return;
        }
        config = (ImagePickerConfig) intent.getSequenceableParam(ImagePickerConfig.class.getName());
        cameraOnlyConfig = (CameraOnlyConfig) intent.getSequenceableParam(CameraOnlyConfig.class.getName());
        startType = intent.getStringParam(START_TYPE);
        if (config != null) {
            theme = config.getTheme();
        }
        setTheme(theme);
        setUIContent(ResourceTable.Layout_ability_custom_ui);
        setupView();
        customInteractionListener = new CustomInteractionListener();
        imagePickerFragment = new ImagePickerFragment(config, cameraOnlyConfig, customInteractionListener);
        getFractionManager()
                .startFractionScheduler()
                .add(com.esafirm.imagepicker.ResourceTable.Id_ef_imagepicker_fragment_placeholder, imagePickerFragment)
                .show(imagePickerFragment)
                .submit();
    }


    @Override
    public void onBackPressed() {
        if (!imagePickerFragment.handleBack()) {
            super.onBackPressed();
        }
    }


    private void setupView() {
        actionBar = (ToolBar) findComponentById(ResourceTable.Id_toolbar);
        actionBar.setOnActionClickListener(clickListener);
        photoPreview = (Image) findComponentById(ResourceTable.Id_photo_preview);
    }

    /**
     * CustomInteractionListener
     */
    public class CustomInteractionListener implements ImagePickerInteractionListener {

        @Override
        public void setTitle(String title) {
            actionBar.setTitle(title);
        }

        @Override
        public void cancel() {
            terminateAbility();
        }

        @Override
        public void finishPickImages(Intent result) {
            setResult(RESULT_OK, result);
            terminateAbility();
        }

        @Override
        public void selectionChanged(List<com.esafirm.imagepicker.model.Image> imageList) {
            if (imageList != null && imageList.size() > 0) {
                actionBar.setVisible(true);
            } else {
                actionBar.setVisible(false);
            }
            if (imageList.isEmpty()) {
                photoPreview.setImageElement(null);
            } else {
                String bitmap = imageList.get(imageList.size() - 1).getUriSchema();
                Glide.with(getContext())
                        .load(bitmap)
                        .fitCenter()
                        .into(photoPreview);
            }
        }
    }

    @Override
    public void onRequestPermissionsFromUserResult(int requestCode, String[] permissions, int[] grantResults) {
        imagePickerFragment.onRequestPermissionsFromUserResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onAbilityResult(int requestCode, int resultCode, Intent resultData) {
        super.onAbilityResult(requestCode, resultCode, resultData);
        imagePickerFragment.onAbilityResult(requestCode, resultCode, resultData, startType);
    }
}
