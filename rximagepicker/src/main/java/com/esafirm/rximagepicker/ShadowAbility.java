package com.esafirm.rximagepicker;


import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.features.ImagePickerAbility;
import com.esafirm.imagepicker.model.Image;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.IntentParams;
import ohos.aafwk.content.Operation;

import ohos.app.Context;

import java.util.List;

/**
 * ShadowAbility
 */
public class ShadowAbility extends Ability {
    /**
     * RC_IMAGE_PICKER
     */
    public final static int RC_IMAGE_PICKER = 12;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_shadow);
        Intent newIntent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withBundleName(getBundleName())
                .withAbilityName(ImagePickerAbility.class.getName())
                .build();
        newIntent.setOperation(operation);
        if (getIntent().getParams() != null) {
            newIntent.setParams(getIntent().getParams());
        }
        startAbilityForResult(newIntent, RC_IMAGE_PICKER);
    }

    @Override
    protected void onAbilityResult(int requestCode, int resultCode, Intent resultData) {
        super.onAbilityResult(requestCode, resultCode, resultData);
        if (resultData != null) {
            List<Image> images = ImagePicker.getImages(resultData);
            RxImagePicker.getInstance().onHandleResult(images);
        }
        terminateAbility();
    }

    /**
     * getStartIntent
     *
     * @param context Context
     * @param bundle  IntentParams
     * @return Intent
     */
    public static Intent getStartIntent(Context context, IntentParams bundle) {
        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withBundleName(context.getBundleName())
                .withAbilityName(ShadowAbility.class.getName())
                .build();
        intent.setOperation(operation);
        if (bundle != null) {
            intent.setParams(bundle);
        }
        return intent;
    }
}