package com.esafirm.rximagepicker;

import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.features.ImagePickerAbility;
import com.esafirm.imagepicker.model.Image;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.IntentParams;
import ohos.aafwk.content.Operation;

import ohos.app.Context;

import rx.Observable;

import rx.Subscriber;

import rx.functions.Action0;

import rx.subscriptions.Subscriptions;

import java.util.List;

import static com.esafirm.rximagepicker.ShadowAbility.getStartIntent;

/**
 * ImagePickerObservable
 */
public class ImagePickerObservable implements Observable.OnSubscribe<List<Image>> {
    private ImagePicker builder;
    private Context context;
    private Ability ability;

    /**
     * 初始化
     *
     * @param builder ImagePicker
     * @param context Ability
     */
    public ImagePickerObservable(ImagePicker builder, Ability context) {
        this.builder = builder;
        this.context = context.getApplicationContext();
        this.ability = context;
    }

    @Override
    public void call(Subscriber<? super List<Image>> subscriber) {
        startImagePicker();
        subscriber.add(Subscriptions.create(new Action0() {
            @Override
            public void call() {
                finishImagePicker();
            }
        }));
        subscriber.onCompleted();
    }

    private void finishImagePicker() {
        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withBundleName(context.getBundleName())
                .withAbilityName(ImagePickerAbility.class.getName())
                .build();
        intent.setOperation(operation);
        intent.addFlags(Intent.FLAG_ABILITY_NEW_MISSION);
        intent.addFlags(Intent.FLAG_ABILITY_CLEAR_MISSION);
        ability.startAbility(intent);
    }

    private void startImagePicker() {
        IntentParams bundle = builder.getIntent(context).getParams();
        Intent intent = getStartIntent(context, bundle);
        intent.addFlags(Intent.FLAG_ABILITY_NEW_MISSION);
        ability.startAbility(intent);
    }
}