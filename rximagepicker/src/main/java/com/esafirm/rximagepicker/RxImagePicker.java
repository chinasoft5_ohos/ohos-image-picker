package com.esafirm.rximagepicker;

import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.model.Image;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.IntentParams;

import rx.Observable;

import rx.subjects.PublishSubject;
import rx.subjects.SerializedSubject;

import java.util.List;

import static com.esafirm.rximagepicker.ShadowAbility.getStartIntent;

/**
 * RxImagePicker
 */
public class RxImagePicker {
    private static RxImagePicker INSTANCE = null;
    private SerializedSubject subject = new SerializedSubject<List<Image>, List<Image>>(PublishSubject.create());

    private RxImagePicker() {
    }

    /**
     * 单例
     *
     * @return RxImagePicker
     */
    public static RxImagePicker getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new RxImagePicker();
        }
        return INSTANCE;
    }

    /**
     * start
     *
     * @param context     Ability
     * @param imagePicker ImagePicker
     * @return Observable<List < Image>>
     */
    public Observable<List<Image>> start(Ability context, ImagePicker imagePicker) {
        startImagePicker(context, imagePicker);
        return subject;
    }

    private void startImagePicker(Ability context, ImagePicker imagePicker) {
        IntentParams bundle = imagePicker.getIntent(context).getParams();
        Intent intent = getStartIntent(context, bundle);
        intent.addFlags(Intent.FLAG_ABILITY_NEW_MISSION);
        intent.addFlags(Intent.FLAG_START_FOREGROUND_ABILITY);
        context.startAbility(intent);
    }

    /**
     * onHandleResult
     *
     * @param images List<Image> images
     */
    public void onHandleResult(List<Image> images) {
        subject.onNext(images);
    }

}