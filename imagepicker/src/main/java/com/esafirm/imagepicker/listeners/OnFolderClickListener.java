package com.esafirm.imagepicker.listeners;

import com.esafirm.imagepicker.model.Folder;

/**
 * OnFolderClickListener
 */
public interface OnFolderClickListener {
    /**
     * onFolderClick
     *
     * @param bucket Folder
     */
    void onFolderClick(Folder bucket);

}