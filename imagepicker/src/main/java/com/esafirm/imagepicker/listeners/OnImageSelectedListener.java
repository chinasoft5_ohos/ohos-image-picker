package com.esafirm.imagepicker.listeners;

import com.esafirm.imagepicker.model.Image;

import java.util.List;

/**
 * OnImageSelectedListener
 */
public interface OnImageSelectedListener {
    /**
     * onSelectionUpdate
     * @param selectedImage List<Image>
     */
    void onSelectionUpdate(List<Image> selectedImage);
}