package com.esafirm.imagepicker.listeners;

/**
 * OnImageClickListener
 */
public interface OnImageClickListener {
    /**
     * onImageClick
     *
     * @param isSelected boolean
     * @return boolean
     */
    boolean onImageClick(boolean isSelected);
}