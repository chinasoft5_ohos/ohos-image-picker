package com.esafirm.imagepicker.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by boss1088 on 8/22/16.
 */
public class Folder implements Serializable {
    /**
     * 当前文件夹的路径
     */
    public String path;

    private String folderName;
    private List<Image> images;

    /**
     * 初始化
     *
     * @param folderName String
     */
    public Folder(String folderName) {
        this.folderName = folderName;
        images = new ArrayList<>();
    }

    /**
     * getFolderName
     *
     * @return String
     */
    public String getFolderName() {
        return folderName;
    }

    /**
     * setFolderName
     *
     * @param folderName String
     */
    public void setFolderName(String folderName) {
        this.folderName = folderName;
    }

    /**
     * getImages
     *
     * @return List<Image>
     */
    public List<Image> getImages() {
        return images;
    }

    /**
     * setImages
     *
     * @param images List<Image>
     */
    public void setImages(List<Image> images) {
        this.images = images;
    }

    /**
     * 只要文件夹的路径和名字相同，就认为是相同的文件夹
     *
     * @param o object
     * @return 是否相同
     */
    @Override
    public boolean equals(Object o) {
        try {
            Folder other = (Folder) o;
            return this.path.equalsIgnoreCase(other.path) && this.folderName.equalsIgnoreCase(other.folderName);
        } catch (ClassCastException e) {
            return super.equals(o);
        }
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}