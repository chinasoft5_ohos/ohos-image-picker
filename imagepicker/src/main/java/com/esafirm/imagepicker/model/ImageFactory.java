package com.esafirm.imagepicker.model;

import com.esafirm.imagepicker.helper.ImagePickerUtils;

import ohos.aafwk.ability.DataUriUtils;

import ohos.utils.net.Uri;

import java.util.ArrayList;
import java.util.List;

/**
 * ImageFactory
 */
public class ImageFactory {
    /**
     * singleImage
     *
     * @param uri  Uri
     * @param path String
     * @return List<Image>
     */
    public static List<Image> singleImage(Uri uri, String path) {
        List<Image> images = new ArrayList<>();
        images.add(new Image(DataUriUtils.getId(uri), ImagePickerUtils.getNameFromFilePath(path), path));
        return images;
    }
}