package com.esafirm.imagepicker.model;

import com.esafirm.imagepicker.helper.ImagePickerUtils;

import ohos.aafwk.ability.DataUriUtils;

import ohos.media.photokit.metadata.AVStorage;

import ohos.utils.Parcel;
import ohos.utils.Sequenceable;
import ohos.utils.net.Uri;

import java.util.Locale;

/**
 * Image
 */
public class Image implements Sequenceable {
    /**
     * PRODUCER
     */
    public static final Producer PRODUCER = new Producer() {
        /**
         * createFromParcel
         *
         * @param in Parcel
         * @return Image
         */
        public Image createFromParcel(Parcel in) {
            // Initialize an instance first, then do customized unmarshlling.
            Image instance = new Image();
            instance.unmarshalling(in);
            return instance;
        }
    };

    private long id;
    private String name;
    private String path;
    private Uri uriHolder = null;
    private Uri uri;
    private String uriSchema; //图片的路径
    private String mediaType;
    private long duration;

    /**
     * getDuration
     *
     * @return long
     */
    public long getDuration() {
        return duration;
    }

    /**
     * 构造函数
     */
    public Image() {
    }

    /**
     * 构造函数
     *
     * @param id   long
     * @param name String
     * @param path String
     */
    public Image(long id, String name, String path) {
        this.id = id;
        this.name = name;
        this.path = path;
    }

    @Override
    public boolean marshalling(Parcel out) {
        return out.writeLong(id)
                && out.writeString(name)
                && out.writeString(path)
                && out.writeString(mediaType)
                && out.writeLong(duration)
                && out.writeString(uriSchema);
    }

    @Override
    public boolean unmarshalling(Parcel in) {
        this.id = in.readLong();
        this.name = in.readString();
        this.path = in.readString();
        this.mediaType = in.readString();
        this.duration = in.readLong();
        this.uriSchema = in.readString();
        return true;
    }

    /**
     * getId
     *
     * @return long
     */
    public long getId() {
        return id;
    }

    /**
     * getName
     *
     * @return String
     */
    public String getName() {
        return name;
    }

    /**
     * getPath
     *
     * @return String
     */
    public String getPath() {
        return path;
    }

    /**
     * getUriSchema
     *
     * @return String
     */
    public String getUriSchema() {
        return uriSchema;
    }

    /**
     * setUriSchema
     *
     * @param uriSchema String
     */
    public void setUriSchema(String uriSchema) {
        this.uriSchema = uriSchema;
    }

    /**
     * getUri
     *
     * @return Uri
     */
    public Uri getUri() {
        Uri contentUri;
        if (uriHolder != null) {
            if (ImagePickerUtils.isVideoFormat(this)) {
                contentUri = AVStorage.Video.Media.EXTERNAL_DATA_ABILITY_URI;
            } else {
                contentUri = AVStorage.Images.Media.EXTERNAL_DATA_ABILITY_URI;
            }
            uriHolder = DataUriUtils.attachId(contentUri, id);
        }
        return uriHolder;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        } else if (o == null || getClass() != o.getClass()) {
            return false;
        } else {
        }
        Image image = (Image) o;
        return image.path.toLowerCase(Locale.CHINA).equals(path.toLowerCase(Locale.CHINA));
    }

    /**
     * setId
     *
     * @param id long
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * setName
     *
     * @param name String
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * setPath
     *
     * @param path String
     */
    public void setPath(String path) {
        this.path = path;
    }

    /**
     * setUriHolder
     *
     * @param uriHolder Uri
     */
    public void setUriHolder(Uri uriHolder) {
        this.uriHolder = uriHolder;
    }

    /**
     * setUri
     *
     * @param uri Uri
     */
    public void setUri(Uri uri) {
        this.uri = uri;
    }

    /**
     * getMediaType
     *
     * @return String
     */
    public String getMediaType() {
        return mediaType;
    }

    /**
     * setMediaType
     *
     * @param mediaType String
     */
    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }

    /**
     * setDuration
     *
     * @param duration long
     */
    public void setDuration(long duration) {
        this.duration = duration;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}