package com.esafirm.imagepicker.model;

import com.esafirm.imagepicker.helper.ImagePickerUtils;

import ohos.bundle.AbilityInfo;

import java.io.Serializable;

import java.util.ArrayList;

/**
 * description : camera的Options
 *
 * @since 2021-04-15
 */
public class Options implements Serializable {
    private int count = 1;
    private int requestCode = 0;
    private int spanCount = 4;
    private String path;
    private boolean frontfacing = false;
    private int videoDurationLimitinSeconds = 40;
    private Mode mode = Mode.ALL;
    private ArrayList<String> preSelectedUrls = new ArrayList<>();
    private AbilityInfo.DisplayOrientation screenOrientation = AbilityInfo.DisplayOrientation.UNSPECIFIED;

    private Options() {
        path = ImagePickerUtils.getPath();
    }

    /**
     * 初始化options
     *
     * @return Options
     */
    public static Options init() {
        return new Options();
    }

    /**
     * 获取录像最大时长
     *
     * @return 录像最大时长
     */
    public int getVideoDurationLimitinSeconds() {
        return videoDurationLimitinSeconds;
    }

    /**
     * 设置录像的最大时长
     *
     * @param videoDurationLimitinSececonds 录像的最大时长
     * @return Options对象
     */
    public Options setVideoDurationLimitinSeconds(int videoDurationLimitinSececonds) {
        this.videoDurationLimitinSeconds = videoDurationLimitinSececonds;
        return this;
    }

    /**
     * 获取已选中的list
     *
     * @return 已选中的list
     */
    public ArrayList<String> getPreSelectedUrls() {
        return preSelectedUrls;
    }

    /**
     * 设置已选中的list
     *
     * @param preSelectedUrls 已选中的list
     * @return Options对象
     */
    public Options setPreSelectedUrls(ArrayList<String> preSelectedUrls) {
        check();
        this.preSelectedUrls = preSelectedUrls;
        return this;
    }

    /**
     * get Height
     *
     * @return Height
     */
    public int getHeight() {
        int height = 0;
        return height;
    }

    /**
     * get Width
     *
     * @return Width
     */
    public int getWidth() {
        int width = 0;
        return width;
    }

    /**
     * 获取是否是前置摄像头
     *
     * @return true：是前置摄像头；false：非前置摄像头
     */
    public boolean isFrontfacing() {
        return this.frontfacing;
    }

    /**
     * 设置默认开启前置摄像头
     *
     * @param frontfacing true：前置摄像头； false：后置摄像头
     * @return Options对象
     */
    public Options setFrontfacing(boolean frontfacing) {
        this.frontfacing = frontfacing;
        return this;
    }

    private void check() {
        if (this == null) {
            throw new NullPointerException("call init() method to initialise Options class");
        }
    }

    /**
     * 获取最大选中的个数
     *
     * @return 最大选中的个数
     */
    public int getCount() {
        return count;
    }

    /**
     * 设置最大选中的个数
     *
     * @param count 最大选中的个数
     * @return Options
     */
    public Options setCount(int count) {
        check();
        this.count = count;
        return this;
    }

    /**
     * 获取跳转到Pix页面的RequestCode
     *
     * @return 跳转到Pix页面的RequestCode
     * @throws NullPointerException
     */
    public int getRequestCode() {
        if (this.requestCode == 0) {
            throw new NullPointerException("requestCode in Options class is null");
        }
        return requestCode;
    }

    /**
     * 设置跳转到Pix页面的RequestCode
     *
     * @param requestCode 跳转到Pix页面的RequestCode
     * @return Options
     */
    public Options setRequestCode(int requestCode) {
        check();
        this.requestCode = requestCode;
        return this;
    }

    /**
     * 获取文件保存路径
     *
     * @return 文件保存路径
     */
    public String getPath() {
        return this.path;
    }

    /**
     * 设置文件保存路径
     *
     * @param path 文件保存路径
     * @return Options
     */
    public Options setPath(String path) {
        check();
        this.path = path;
        return this;
    }

    /**
     * getScreenOrientation
     *
     * @return screenOrientation
     */
    public AbilityInfo.DisplayOrientation getScreenOrientation() {
        return screenOrientation;
    }

    /**
     * set ScreenOrientation
     *
     * @param screenOrientation screenOrientation
     * @return Options
     */
    public Options setScreenOrientation(AbilityInfo.DisplayOrientation screenOrientation) {
        check();
        this.screenOrientation = screenOrientation;
        return this;
    }

    /**
     * 获取listContainer的provider的列数
     *
     * @return listContainer的provider的列数
     */
    public int getSpanCount() {
        return spanCount;
    }

    /**
     * 设置listContainer的provider的列数
     *
     * @param spanCount listContainer的provider的列数
     * @return Options
     * @throws IllegalArgumentException
     */
    public Options setSpanCount(int spanCount) {
        check();
        this.spanCount = spanCount;
        if (spanCount < 1 || spanCount > 5) {
            throw new IllegalArgumentException("span count can not be set below 0 or more than 5");
        }
        return this;
    }

    /**
     * 获取选择文件的格式
     *
     * @return 选择文件的格式
     */
    public Mode getMode() {
        return mode;
    }

    /**
     * 设置选择文件的格式
     *
     * @param mode 选择文件的格式
     * @return Options
     */
    public Options setMode(Mode mode) {
        this.mode = mode;
        return this;
    }

    /**
     * 文件选择的格式
     */
    public enum Mode {
        ALL,
        PICTURE,
        VIDEO
    }
}
