package com.esafirm.imagepicker.adapter;

import com.esafirm.imagepicker.ResourceTable;
import com.esafirm.imagepicker.features.imageloader.ImageLoader;
import com.esafirm.imagepicker.helper.Utility;
import com.esafirm.imagepicker.listeners.OnFolderClickListener;
import com.esafirm.imagepicker.model.Folder;

import ohos.agp.components.*;
import ohos.app.Context;

import java.util.ArrayList;
import java.util.List;

/**
 * FolderPickerAdapter
 */
public class FolderPickerAdapter extends BaseListAdapter<Folder> {
    private Component itemView;
    private ImageLoader imageLoader;
    private OnFolderClickListener onFolderClickListener;
    private final int mImageSize;

    /**
     * 构造函数
     *
     * @param context     Context
     * @param columns     int
     * @param data        List<Folder>
     * @param imageLoader ImageLoader
     * @param listener    OnFolderClickListener
     */
    public FolderPickerAdapter(Context context, int columns, List<Folder> data, ImageLoader imageLoader, OnFolderClickListener listener) {
        super(context, columns, data);
        this.imageLoader = imageLoader;
        this.onFolderClickListener = listener;
        mImageSize = Utility.getImageItemWidth(context);
    }

    @Override
    protected void convert(ViewHolder holder, int finalRealPosition1, Folder mData) {
        Image image = holder.getView(ResourceTable.Id_image);
        Text tv_name = holder.getView(ResourceTable.Id_tv_name);
        Text tv_number = holder.getView(ResourceTable.Id_tv_number);
        itemView.setLayoutConfig(new ListContainer.LayoutConfig(mImageSize, mImageSize));
        tv_name.setText(mData.getFolderName());
        tv_number.setText(mData.getImages().size() + "");
        imageLoader.loadImage(mData.getImages().get(0), image, null);
        image.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                onFolderClickListener.onFolderClick(mData);
            }
        });
    }

    @Override
    protected Component getItemLayout() {
        itemView = LayoutScatter.getInstance(mContext).parse(ResourceTable.Layout_ef_imagepicker_item_folder, null, false);
        return itemView;
    }

    /**
     * setData
     *
     * @param folders List<Folder>
     */
    public void setData(List<Folder> folders) {
        if (mData == null) {
            mData = new ArrayList<>();
        } else {
            mData.clear();
        }
        if (folders != null) {
            mData.addAll(folders);
        }
    }
}
