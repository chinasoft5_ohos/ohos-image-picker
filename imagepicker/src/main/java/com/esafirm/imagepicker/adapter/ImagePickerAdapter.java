package com.esafirm.imagepicker.adapter;

import com.esafirm.imagepicker.ResourceTable;
import com.esafirm.imagepicker.features.imageloader.ImageLoader;
import com.esafirm.imagepicker.features.imageloader.ImageType;
import com.esafirm.imagepicker.helper.ImagePickerUtils;
import com.esafirm.imagepicker.helper.LogUtil;
import com.esafirm.imagepicker.helper.ResourceUtil;
import com.esafirm.imagepicker.listeners.OnImageClickListener;
import com.esafirm.imagepicker.listeners.OnImageSelectedListener;
import com.esafirm.imagepicker.model.Image;

import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.ability.DataAbilityRemoteException;

import ohos.agp.components.*;

import ohos.app.Context;

import ohos.media.image.PixelMap;
import ohos.media.photokit.metadata.AVMetadataHelper;
import ohos.media.photokit.metadata.AVStorage;

import ohos.utils.net.Uri;

import java.io.FileDescriptor;
import java.io.FileNotFoundException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.esafirm.imagepicker.features.IpCons.MODE_SINGLE;

/**
 * ImagePickerAdapter
 */
public class ImagePickerAdapter extends BaseListAdapter<Image> {
    private Component itemView;
    private ArrayList<Image> selectedImages;
    private OnImageSelectedListener imageSelectedListener;
    private OnImageClickListener itemClickListener;
    private HashMap<Long, String> videoDurationHolder = new HashMap<>();
    private ImageLoader imageLoader;
    private ViewHolder mHolder;
    private DataAbilityHelper helper;
    private AVMetadataHelper avMetadataHelper;
    private int lastClickPos;
    private int mode;
    private int mCrrentPos;

    /**
     * 构造函数
     *
     * @param context   Context
     * @param gridCount int
     * @param mData     List<Image>
     */
    public ImagePickerAdapter(Context context, int gridCount, List<Image> mData) {
        super(context, gridCount, mData);
    }

    /**
     * 构造函数
     *
     * @param context              Context
     * @param columns              int
     * @param imageLoader          ImageLoader
     * @param selectedImages       ArrayList<Image>
     * @param onImageClickListener OnImageClickListener
     * @param mode                 int
     */
    public ImagePickerAdapter(Context context, int columns, ImageLoader imageLoader, ArrayList<Image> selectedImages, OnImageClickListener onImageClickListener, int mode) {
        super(context, columns, selectedImages);
        itemClickListener = onImageClickListener;
        this.mode = mode;
        this.imageLoader = imageLoader;
        this.selectedImages = new ArrayList<>();
        if (selectedImages != null && selectedImages.size() != 0) {
            this.selectedImages.addAll(selectedImages);
        }
        helper = DataAbilityHelper.creator(mContext);
        avMetadataHelper = new AVMetadataHelper();
    }

    @Override
    protected void convert(ViewHolder holder, int finalRealPosition1, Image data) {
        mHolder = holder;
        mCrrentPos = finalRealPosition1;
        ohos.agp.components.Image image = holder.getView(ResourceTable.Id_image_view);
        ohos.agp.components.Image done = holder.getView(ResourceTable.Id_done);
        Component alphaView = holder.getView(ResourceTable.Id_view_alpha);
        Text fileTypeIndicator = holder.getView(ResourceTable.Id_ef_item_file_type_indicator);
        boolean isSelect = isSelected(data);
        alphaView.setVisibility(isSelect ? Component.VISIBLE : Component.HIDE);
        done.setVisibility(isSelect ? Component.VISIBLE : Component.HIDE);
        if (ImagePickerUtils.isVideoFormat(data)) {
            PixelMap pixelMap = getPixMap(data);
            image.setPixelMap(pixelMap);
        } else {
            imageLoader.loadImage(data, image, ImageType.GALLERY);
        }
        boolean showFileTypeIndicator = false;
        String fileTypeLabel = "gif";
        if (ImagePickerUtils.isGifFormat(data)) {
            fileTypeLabel = ResourceUtil.getText(mContext, ResourceTable.String_ef_gif);
            showFileTypeIndicator = true;
        }
        if (ImagePickerUtils.isVideoFormat(data)) {
            if (!videoDurationHolder.containsKey(data.getId())) {
                videoDurationHolder.put(data.getId(),
                        ImagePickerUtils.getVideoDurationLabel(data.getDuration()));
            }
            fileTypeLabel = videoDurationHolder.get(data.getId());
            showFileTypeIndicator = true;
        }
        fileTypeIndicator.setText(fileTypeLabel);
        fileTypeIndicator.setVisibility(showFileTypeIndicator ? Component.VISIBLE : Component.HIDE);
        itemView.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                boolean shouldSelect = itemClickListener.onImageClick(isSelect);
                if (isSelect) {
                    removeSelectedImage(data, finalRealPosition1);
                } else if (shouldSelect) {
                    addSelected(data, finalRealPosition1);
                } else {
                }
            }
        });
    }

    private PixelMap getPixMap(Image data) {
        Uri path = Uri.appendEncodedPathToUri(AVStorage.Video.Media.EXTERNAL_DATA_ABILITY_URI, "" + data.getId());
        FileDescriptor fileDescriptor = null;
        try {
            fileDescriptor = helper.openFile(path, "r");
        } catch (DataAbilityRemoteException | FileNotFoundException e) {
            LogUtil.loge(e.getMessage());
        }
        avMetadataHelper.setSource(fileDescriptor);
        PixelMap pixelMap = avMetadataHelper.fetchVideoPixelMapByIndex(0);
        return pixelMap;
    }

    @Override
    protected Component getItemLayout() {
        itemView = LayoutScatter.getInstance(mContext).parse(ResourceTable.Layout_ef_imagepicker_item_image, null, false);
        return itemView;
    }

    /**
     * getSelectedImages
     *
     * @return List<Image>
     */
    public List<Image> getSelectedImages() {
        return selectedImages;
    }

    /**
     * setData
     *
     * @param images List<Image>
     */
    public void setData(List<Image> images) {
        if (mData == null) {
            mData = new ArrayList<>();
        } else {
            mData.clear();
        }
        mData.addAll(images);
    }

    private boolean isSelected(Image image) {

        for (int i = 0; i < this.selectedImages.size(); i++) {
            if (selectedImages.get(i).getPath().equals(image.getPath())) {
                if (mode == MODE_SINGLE) {
                    lastClickPos = mCrrentPos;
                }
                return true;
            }
        }
        return false;
    }

    /**
     * setImageSelectedListener
     *
     * @param listener OnImageSelectedListener
     */
    public void setImageSelectedListener(OnImageSelectedListener listener) {
        this.imageSelectedListener = listener;
    }

    /**
     * removeAllSelectedSingleClick
     */
    public void removeAllSelectedSingleClick() {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                selectedImages.clear();
                selectedImages.add(mData.get(lastClickPos));
                removeSelectedImage(mData.get(lastClickPos), lastClickPos);

            }
        };
        mutateSelection(runnable);
    }

    private void addSelected(Image image, int position) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                selectedImages.add(image);
                updateView(image, position);
                lastClickPos = position;
            }
        };
        mutateSelection(runnable);
    }

    private void removeSelectedImage(Image image, int position) {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                selectedImages.remove(image);
                updateView(image, position);
            }
        };
        mutateSelection(runnable);
    }

    private void mutateSelection(Runnable runnable) {
        runnable.run();
        if (imageSelectedListener != null) {
            imageSelectedListener.onSelectionUpdate(selectedImages);
        }
    }

    /**
     * 局部刷新
     *
     * @param itemIndex int
     * @param data      Image
     */
    public void updateView(Image data, int itemIndex) {
        if (mHolder == null) {
            return;
        }
        int listPos = getPos(itemIndex);
        int index = getListOfPos(itemIndex);
        ComponentContainer component = (ComponentContainer) items.get(listPos);
        Component rootView = component.getComponentAt(index);
        ohos.agp.components.Image done = (ohos.agp.components.Image) rootView.findComponentById(ResourceTable.Id_done);
        ohos.agp.components.Image imageView = (ohos.agp.components.Image) rootView.findComponentById(ResourceTable.Id_image_view);
        Component alphaView = rootView.findComponentById(ResourceTable.Id_view_alpha);
        Text fileTypeIndicator = (Text) rootView.findComponentById(ResourceTable.Id_ef_item_file_type_indicator);
        boolean isSelect1 = isSelected(data);
        alphaView.setVisibility(isSelect1 ? Component.VISIBLE : Component.HIDE);
        done.setVisibility(isSelect1 ? Component.VISIBLE : Component.HIDE);
        if (ImagePickerUtils.isVideoFormat(data)) {
            PixelMap pixelMap = getPixMap(data);
            imageView.setPixelMap(pixelMap);
        } else {
            imageLoader.loadImage(data, imageView, null);
        }
        boolean showFileTypeIndicator = false;
        String fileTypeLabel = "gif";
        if (ImagePickerUtils.isGifFormat(data)) {
            fileTypeLabel = ResourceUtil.getText(mContext, ResourceTable.String_ef_gif);
            showFileTypeIndicator = true;
        }
        if (ImagePickerUtils.isVideoFormat(data)) {
            if (!videoDurationHolder.containsKey(data.getId())) {
                videoDurationHolder.put(data.getId(),
                        ImagePickerUtils.getVideoDurationLabel(data.getDuration()));
            }
            fileTypeLabel = videoDurationHolder.get(data.getId());
            showFileTypeIndicator = true;
        }
        fileTypeIndicator.setText(fileTypeLabel);
        fileTypeIndicator.setVisibility(showFileTypeIndicator ? Component.VISIBLE : Component.HIDE);
        rootView.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                boolean shouldSelect = itemClickListener.onImageClick(isSelect1);
                if (isSelect1) {
                    removeSelectedImage(data, itemIndex);
                } else if (shouldSelect) {
                    addSelected(data, itemIndex);
                } else {
                }
            }
        });
    }
}
