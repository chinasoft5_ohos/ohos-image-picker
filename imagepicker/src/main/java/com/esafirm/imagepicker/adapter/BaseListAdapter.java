package com.esafirm.imagepicker.adapter;

import com.esafirm.imagepicker.ResourceTable;
import com.esafirm.imagepicker.helper.SizeUtil;

import ohos.agp.components.*;

import ohos.app.Context;

import java.util.HashMap;
import java.util.List;

/**
 * BaseListAdapter
 *
 * @param <T> T
 */
public abstract class BaseListAdapter<T> extends BaseItemProvider {
    /**
     * items
     */
    public HashMap<Integer, Object> items;

    /**
     * mData
     */
    protected List<T> mData;
    /**
     * mContext
     */
    protected Context mContext;

    /**
     * 每一行有几个 item  默认是一个
     */
    private int gridCount = 1;
    private int mItemWidth;
    private int horizentalSpace = SizeUtil.dp2px(2);
    private int veticalSpace = SizeUtil.dp2px(2);

    /**
     * 构造
     *
     * @param context   Context
     * @param gridCount int
     * @param mData     数据
     */
    public BaseListAdapter(Context context, int gridCount, List<T> mData) {
        this.gridCount = gridCount;
        this.mData = mData;
        mContext = context;
        mItemWidth = SizeUtil.getScreenWidth() / gridCount;
        items = new HashMap<>();
    }

    /**
     * getCount
     *
     * @return int
     */
    @Override
    public int getCount() {
        if (gridCount == 1) {
            return mData == null ? 0 : mData.size();
        } else {
            int needLines = 0;
            if (mData != null) {
                if (mData.size() % gridCount == 0) {
                    needLines = mData.size() / gridCount;
                } else {
                    needLines = (mData.size() / gridCount) + 1;
                }
            }
            return needLines;
        }
    }

    /**
     * getItem
     *
     * @param position int
     * @return Object
     */
    @Override
    public Object getItem(int position) {
        if (gridCount == 1) {
            return mData == null ? null : mData.get(position);
        } else {
            if (mData != null) {
                int startIndex = position * gridCount;
                int endIndex = (position + 1) * gridCount;
                if (endIndex > mData.size()) {
                    endIndex = mData.size();
                }
                return mData.subList(startIndex, endIndex);
            }
            return null;
        }
    }

    /**
     * getItemId
     *
     * @param position int
     * @return long
     */
    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Component getComponent(int position, Component convertView, ComponentContainer componentContainer) {
        ViewHolder holder = null;
        Component itemView = convertView;
        if (itemView == null) {
            itemView = LayoutScatter.getInstance(mContext).parse(ResourceTable.Layout_item_container, null, false);
            holder = ViewHolder.create(itemView, mContext);
            itemView.setTag(holder);
        } else {
            holder = (ViewHolder) (itemView.getTag());
        }
        items.put(position, itemView);
        DirectionalLayout container = holder.getView(ResourceTable.Id_container);
        if (position != getCount() - 1) {
            ComponentContainer.LayoutConfig layoutConfig = container.getLayoutConfig();
            layoutConfig.setMarginBottom(veticalSpace);
            container.setLayoutConfig(layoutConfig);
        }
        container.removeAllComponents();
        if (gridCount == 1) {
            handleSingle(position, container);
        } else {
            handleGrid(position, holder, container);
        }
        return itemView;
    }

    private void handleSingle(int position, DirectionalLayout container) {
        Component itemLayout = getItemLayout();
        container.addComponent(itemLayout);
        ViewHolder holder1 = ViewHolder.create(itemLayout, mContext);
        setClickLisenter(position, container);
        convert(holder1, position, mData.get(position));
    }

    private void handleGrid(int position, ViewHolder holder, DirectionalLayout container) {
        List<T> itemsData = (List<T>) getItem(position);
        for (int i = 0; i < itemsData.size(); i++) {
            Component childItem = getItemLayout();
            int realPosition = position * gridCount + i;
            DirectionalLayout.LayoutConfig itemLayoutConfig = new DirectionalLayout.LayoutConfig(mItemWidth, DirectionalLayout.LayoutConfig.MATCH_CONTENT);
            if (i != itemsData.size() - 1) {
                itemLayoutConfig.setMarginRight(horizentalSpace);
            }
            container.addComponent(childItem, itemLayoutConfig);
            ViewHolder holder2 = ViewHolder.create(childItem, mContext);
            if (realPosition > mData.size()) {
                realPosition = mData.size();
            }
            childItem.setTag(holder);
            final int pos = realPosition;
            setGridCilckLisenter(childItem, pos);
            convert(holder2, realPosition, mData.get(realPosition));
        }
    }

    private void setGridCilckLisenter(Component childItem, int pos) {
        childItem.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (lisenter != null) {
                    lisenter.onItemClick(pos, BaseListAdapter.this, component, mData.get(pos));
                }
            }
        });
    }

    private void setClickLisenter(int position, DirectionalLayout container) {
        container.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (lisenter != null) {
                    lisenter.onItemClick(position, BaseListAdapter.this, component, mData.get(position));
                }
            }
        });
    }

    /**
     * 设置数据
     *
     * @param holder             ViewHolder
     * @param finalRealPosition1 实际点击的位置
     * @param data               数据
     */
    protected abstract void convert(ViewHolder holder, int finalRealPosition1, T data);

    /**
     * 获取itemView
     *
     * @return 返回item布局
     */
    protected abstract Component getItemLayout();

    /**
     * getGridCount
     *
     * @return int
     */
    public int getGridCount() {
        return gridCount;
    }

    /**
     * setGridCount
     *
     * @param gridCount int
     */
    public void setGridCount(int gridCount) {
        this.gridCount = gridCount;
        mItemWidth = SizeUtil.getScreenWidth() / gridCount;
    }

    /**
     * 获取数据集合
     *
     * @return List<T>
     */
    public List<T> getData() {
        return mData;
    }

    /**
     * setDataList
     *
     * @param mData List<T>
     */
    public void setDataList(List<T> mData) {
        this.mData = mData;
        notifyDataChanged();
    }

    /**
     * OnItemClickLisenter
     */
    public interface OnItemClickLisenter {
        /**
         * item点击事件
         *
         * @param position  int
         * @param adapter   BaseListAdapter
         * @param component Component
         * @param data      Object
         */
        void onItemClick(int position, BaseListAdapter adapter, Component component, Object data);
    }

    private OnItemClickLisenter lisenter;

    /**
     * setOnItemClickLisenter
     *
     * @param lisenter OnItemClickLisenter
     */
    public void setOnItemClickLisenter(OnItemClickLisenter lisenter) {
        this.lisenter = lisenter;
    }

    /**
     * 获取是 第几行的
     *
     * @param itemIndex int
     * @return int
     */
    protected int getPos(int itemIndex) {
        int listPos = itemIndex / gridCount;
        return listPos;
    }

    /**
     * 获取是行中的第几个
     *
     * @param itemIndex int
     * @return int
     */
    protected int getListOfPos(int itemIndex) {
        int index = itemIndex % gridCount;
        return index;
    }

    /**
     * 设置间距
     *
     * @param horizentalSpace 水平距离间距
     * @param veticalSpace    垂直距离间距
     */
    public void setItemSpace(int horizentalSpace, int veticalSpace) {
        this.horizentalSpace = horizentalSpace;
        this.veticalSpace = veticalSpace;
    }

    /**
     * 设置水平间距
     *
     * @param horizentalSpace 水平距离间距
     */
    public void setHorizentalSpace(int horizentalSpace) {
        this.horizentalSpace = horizentalSpace;
    }

    /**
     * 设置垂直间距
     *
     * @param veticalSpace 垂直距离间距
     */
    public void setVeticalSpace(int veticalSpace) {
        this.veticalSpace = veticalSpace;
    }
}
