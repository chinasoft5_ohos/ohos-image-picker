/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.esafirm.imagepicker.adapter;

import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.app.Context;

import java.util.HashMap;
import java.util.Map;

/**
 * ViewHolder
 */
public class ViewHolder {
    private final Context context;
    private Map<Integer, Component> views;
    private Component convertView;

    private ViewHolder(Component view, Context context) {
        convertView = view;
        this.context = context;
        views = new HashMap<>();
    }

    /**
     * create
     *
     * @param view    Component
     * @param context Context
     * @return ViewHolder
     */
    public static ViewHolder create(Component view, Context context) {
        return new ViewHolder(view, context);
    }

    /**
     * getView
     *
     * @param viewId int
     * @param <T>    T
     * @return Component
     */
    public <T extends Component> T getView(int viewId) {
        Component view = views.get(viewId);
        if (view == null) {
            view = convertView.findComponentById(viewId);
            views.put(viewId, view);
        }
        return (T) view;
    }

    /**
     * getConvertView
     *
     * @return Component
     */
    public Component getConvertView() {
        return convertView;
    }

    /**
     * setText
     *
     * @param viewId int
     * @param text   String
     */
    public void setText(int viewId, String text) {
        Text textView = getView(viewId);
        textView.setText(text);
    }

    /**
     * setText
     *
     * @param viewId int
     * @param textId int
     */
    public void setText(int viewId, int textId) {
        Text textView = getView(viewId);
        textView.setText(textId);
    }

    /**
     * setTextColor
     *
     * @param viewId int
     * @param color  Color
     */
    public void setTextColor(int viewId, Color color) {
        Text textView = getView(viewId);
        textView.setTextColor(color);
    }

    /**
     * setOnClickListener
     *
     * @param viewId        int
     * @param clickListener Component.ClickedListener
     */
    public void setOnClickListener(int viewId, Component.ClickedListener clickListener) {
        Component view = getView(viewId);
        view.setClickedListener(clickListener);
    }

    /**
     * setBackgroundResource
     *
     * @param viewId int
     * @param resId  int
     */
    public void setBackgroundResource(int viewId, int resId) {
        Component view = getView(viewId);
        view.setBackground(new ShapeElement(context, resId));
    }
}
