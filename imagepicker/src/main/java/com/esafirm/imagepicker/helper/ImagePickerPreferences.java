package com.esafirm.imagepicker.helper;

import ohos.app.Context;

import ohos.data.DatabaseHelper;
import ohos.data.preferences.Preferences;

/**
 * ImagePickerPreferences
 */
public class ImagePickerPreferences {
    /**
     * writeExternalRequested
     */
    public static final String PREF_WRITE_EXTERNAL_STORAGE_REQUESTED = "writeExternalRequested";
    /**
     * cameraRequested
     */
    public static final String PREF_CAMERA_REQUESTED = "cameraRequested";
    private final DatabaseHelper databaseHelper;
    private final String fileName;
    private final Preferences preferences;

    /**
     * ImagePickerPreferences
     *
     * @param context Context
     */
    public ImagePickerPreferences(Context context) {
        databaseHelper = new DatabaseHelper(context);
        fileName = "My awesome app prefs";
        preferences = databaseHelper.getPreferences(fileName);
    }

    /**
     * Set a permission is requested
     *
     * @param permission String
     */
    public void setPermissionRequested(String permission) {
        preferences.putBoolean(permission, true);
        preferences.flush();
    }

    /**
     * Check if a permission is requestted or not (false by default)
     *
     * @param permission String
     * @return boolean
     */
    public boolean isPermissionRequested(String permission) {
        return preferences.getBoolean(permission, false);
    }
}
