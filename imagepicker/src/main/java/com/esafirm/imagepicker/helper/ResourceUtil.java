/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.esafirm.imagepicker.helper;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Point;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Rect;
import ohos.media.image.common.Size;

import java.io.IOException;
import java.io.InputStream;

/**
 * @Author: AriesHoo on 2018/7/19 9:49
 * @E-Mail: AriesHoo@126.com
 * Function: 资源文件获取帮助类
 * Description:
 */
public class ResourceUtil {

    /**
     * 系统方法无法单独获取到状态栏高度，只能获取到状态栏高度与底部导航栏高度之和，所以这里给一个固定高度
     */
    private static final int STATUSBAR_DEFAULT_HEIGHT = 72;
    private Context mContext;

    /**
     * ResourceUtil
     *
     * @param context Context
     */
    public ResourceUtil(Context context) {
        this.mContext = context;
    }

    /**
     * getText
     *
     * @param context Context
     * @param res     int
     * @return String
     */
    public static String getText(Context context, int res) {
        return context.getString(res);
    }

    /**
     * getTextArray
     *
     * @param res int
     * @return String[]
     */
    public String[] getTextArray(int res) {
        return mContext.getStringArray(res);
    }

    /**
     * getDrawable
     *
     * @param res int
     * @return ShapeElement
     */
    public ShapeElement getDrawable(int res) {
        return new ShapeElement(mContext, res);
    }

    /**
     * 获取media的图片
     *
     * @param res int
     * @return Element
     */
    public Element getImageDrawable(int res) {
        PixelMapElement drawable = null;
        try {
            drawable = new PixelMapElement(
                    mContext.getResourceManager().getResource(res));
        } catch (IOException | NotExistException e) {
            LogUtil.loge(e.getMessage());
        }
        return drawable;
    }

    /**
     * 通过颜色id 获取elemet对象
     *
     * @param res int
     * @return Element
     */
    public Element getColorDrawable(int res) {
        int color = getColor(res);
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setRgbColor(RgbColor.fromArgbInt(color));
        return shapeElement;
    }

    /**
     * getShapeDrawable
     *
     * @param res int
     * @return ShapeElement
     */
    public ShapeElement getShapeDrawable(int res) {
        return new ShapeElement(mContext, res);
    }

    /**
     * getPixelMapElement
     *
     * @param res int
     * @return PixelMapElement
     */
    public PixelMapElement getPixelMapElement(int res) {

        try {
            return new PixelMapElement(mContext.getResourceManager().getResource(res));
        } catch (IOException | NotExistException e) {
            LogUtil.loge(e.getMessage());
        }
        return null;
    }

    /**
     * getColor
     *
     * @param res int
     * @return int
     */
    public int getColor(int res) {
        int result = 0;
        try {
            result = mContext.getResourceManager().getElement(res).getColor();
        } catch (IOException | NotExistException | WrongTypeException e) {
            LogUtil.loge(e.getMessage());
        }
        return result;
    }

    /**
     * getDimension
     *
     * @param res int
     * @return float
     */
    public float getDimension(int res) {
        float result = 0;
        try {
            result = mContext.getResourceManager().getElement(res).getFloat();
        } catch (IOException | NotExistException | WrongTypeException e) {
            LogUtil.loge(e.getMessage());
        }
        return result;
    }

    /**
     * getDimensionPixelSize
     *
     * @param res int
     * @return int
     */
    public int getDimensionPixelSize(int res) {
        int result = 0;
        try {
            result = (int) mContext.getResourceManager().getElement(res).getFloat();
        } catch (IOException | NotExistException | WrongTypeException e) {
            LogUtil.loge(e.getMessage());
        }
        return result;
    }

    /**
     * getStringArray
     *
     * @param res int
     * @return String[]
     */
    public String[] getStringArray(int res) {
        String[] result = new String[0];
        try {
            result = mContext.getResourceManager().getElement(res).getStringArray();
        } catch (IOException | NotExistException | WrongTypeException e) {
            LogUtil.loge(e.getMessage());
        }
        return result;
    }

    /**
     * getAttrColor
     *
     * @param attrRes int
     * @return int
     */
    public int getAttrColor(int attrRes) {
        int result = 0;
        try {
            result = mContext.getResourceManager().getElement(attrRes).getColor();
        } catch (IOException | NotExistException | WrongTypeException e) {
            LogUtil.loge(e.getMessage());
        }
        return result;
    }

    /**
     * getAttrFloat
     *
     * @param attrRes int
     * @return float
     */
    public float getAttrFloat(int attrRes) {
        return getAttrFloat(attrRes, 1.0f);
    }

    /**
     * getAttrFloat
     *
     * @param attrRes int
     * @param def     float
     * @return float
     */
    public float getAttrFloat(int attrRes, float def) {
        float result = def;
        try {
            result = mContext.getResourceManager().getElement(attrRes).getFloat();
        } catch (IOException | NotExistException | WrongTypeException e) {
            LogUtil.loge(e.getMessage());
        }
        return result == 0 ? def : result;
    }

    /**
     * getPixelMapFromRes
     *
     * @param context Context
     * @param res     int
     * @return PixelMap
     */
    public static PixelMap getPixelMapFromRes(Context context, int res) {
        InputStream stream = null;
        try {
            stream = context.getResourceManager().getResource(res);
            ImageSource.SourceOptions sourceOptions = new ImageSource.SourceOptions();
            sourceOptions.formatHint = "image/png";
            ImageSource imageSource = ImageSource.create(stream, sourceOptions);
            ImageSource.DecodingOptions decodingOptions = new ImageSource.DecodingOptions();
            decodingOptions.desiredSize = new Size(0, 0);
            decodingOptions.desiredRegion = new Rect(0, 0, 0, 0);
            decodingOptions.desiredPixelFormat = PixelFormat.ARGB_8888;
            return imageSource.createPixelmap(decodingOptions);
        } catch (IOException | NotExistException e) {
            LogUtil.loge(e.getMessage());
        } finally {
            if (stream != null) {
                try {
                    stream.close();
                } catch (IOException e) {
                    LogUtil.loge(e.getMessage());
                }
            }
        }
        return null;
    }

    /**
     * 获取状态栏高度 * * @return
     *
     * @param context Context
     * @return int
     */
    public static int getStatusBarHeight(Context context) {
        int result = 0;
        Point point1 = new Point();
        Point point2 = new Point();
        Display display = DisplayManager.getInstance().getDefaultDisplay(context).get();
        display.getSize(point1);
        display.getRealSize(point2);
        result = (int) (point2.getPointY() - point1.getPointY());
        return Math.min(result, STATUSBAR_DEFAULT_HEIGHT);
    }
}
