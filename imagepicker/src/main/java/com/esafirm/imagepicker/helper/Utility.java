/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.esafirm.imagepicker.helper;

import com.esafirm.imagepicker.model.Options;

import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.ability.DataAbilityRemoteException;

import ohos.agp.components.Component;
import ohos.agp.components.ListContainer;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

import ohos.data.resultset.ResultSet;

import ohos.media.photokit.metadata.AVStorage;

import ohos.utils.net.Uri;

import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * description : Utility
 *
 * @since 2021-04-15
 */
public class Utility {
    /**
     * get DateDifference
     *
     * @param calendar calendar
     * @return Date信息
     */
    public static String getDateDifference(Calendar calendar) {
        Date date = calendar.getTime();
        Calendar lastMonth = Calendar.getInstance();
        Calendar lastWeek = Calendar.getInstance();
        Calendar recent = Calendar.getInstance();
        lastMonth.add(Calendar.DAY_OF_MONTH, -(Calendar.DAY_OF_MONTH));
        lastWeek.add(Calendar.DAY_OF_MONTH, -7);
        recent.add(Calendar.DAY_OF_MONTH, -2);
        if (calendar.before(lastMonth)) {
            return new SimpleDateFormat("MMMM", Locale.getDefault()).format(date);
        } else if (calendar.after(lastMonth) && calendar.before(lastWeek)) {
            return "Last Month";
        } else if (calendar.after(lastWeek) && calendar.before(recent)) {
            return "Last Week";
        } else {
            return "Recent";
        }
    }

    /**
     * 获取查询图片、视频、图片和视频的集合
     *
     * @param helper DataAbilityHelper
     * @param mode   Options.Mode
     * @return 查询的图片、视频、图片和视频的集合
     * @throws DataAbilityRemoteException DataAbilityRemoteException
     */
    public static ResultSet getImageVideoResult(DataAbilityHelper helper,
                                                Options.Mode mode) throws DataAbilityRemoteException {
        Uri uri;
        switch (mode) {
            case VIDEO:
                uri = AVStorage.Video.Media.EXTERNAL_DATA_ABILITY_URI;
                break;
            case PICTURE:
                uri = AVStorage.Images.Media.EXTERNAL_DATA_ABILITY_URI;
                break;
            default:
                uri = AVStorage.Files.fetchResource("external");
                break;
        }
        return helper.query(uri, null, null);
    }

    /**
     * 根据抽屉布局滑动的百分比控制各控件的显示和隐藏
     *
     * @param slideOffset    滑动的百分比
     * @param arrowUp       arrow_up
     * @param horizontalList 水平方向的listContainer
     * @param recyclerView   竖直方向的listContainer
     * @param topbar         topbar
     * @param clickme        拍照按钮
     * @param sendButton     确认选中按钮
     * @param longSelection  是否是多选模式
     */
    public static void manipulateVisibility(double slideOffset, Component arrowUp,
                                            ListContainer horizontalList, ListContainer recyclerView,
                                            Component topbar, Component clickme,
                                            Component sendButton, boolean longSelection) {
        horizontalList.setAlpha((float) (1 - slideOffset));
        arrowUp.setAlpha((float) (1 - slideOffset));
        clickme.setAlpha((float) (1 - slideOffset));
        if (longSelection) {
            sendButton.setAlpha((float) (1 - slideOffset));
        }
        topbar.setAlpha((float) slideOffset);
        recyclerView.setAlpha((float) slideOffset);
        if ((1 - slideOffset) == 0 && horizontalList.getVisibility() == Component.VISIBLE) {
            horizontalList.setVisibility(Component.HIDE);
            arrowUp.setVisibility(Component.HIDE);
            clickme.setVisibility(Component.HIDE);
            sendButton.setVisibility(Component.HIDE);
        }
        if (horizontalList.getVisibility() == Component.HIDE && (1 - slideOffset) > 0) {
            horizontalList.setVisibility(Component.VISIBLE);
            arrowUp.setVisibility(Component.VISIBLE);
            clickme.setVisibility(Component.VISIBLE);
            if (longSelection) {
                sendButton.setVisibility(Component.VISIBLE);
            }
        }
        if ((slideOffset) > 0 && recyclerView.getVisibility() == Component.INVISIBLE) {
            recyclerView.setVisibility(Component.VISIBLE);
            topbar.setVisibility(Component.VISIBLE);
        }
        if (recyclerView.getVisibility() == Component.VISIBLE && (slideOffset) == 0) {
            recyclerView.setVisibility(Component.INVISIBLE);
            topbar.setVisibility(Component.HIDE);
        }
    }

    /**
     * 根据屏幕宽度与密度计算GridView显示的列数， 最少为二列，并获取Item宽度
     *
     * @param ability 上下文对象
     * @return 图片宽度
     */
    public static int getImageItemWidth(Context ability) {
        DisplayManager displayManager = DisplayManager.getInstance();
        Display display = displayManager.getDefaultDisplay(ability).get();
        int screenWidth = display.getAttributes().width;
        int densityDpi = display.getAttributes().densityDpi;
        int cols = screenWidth / densityDpi;
        cols = Math.max(cols, 2);
        int columnSpace = (int) (1 * display.getAttributes().densityPixels);
        LogUtil.loge(columnSpace + "");
        return screenWidth / cols;
    }
}
