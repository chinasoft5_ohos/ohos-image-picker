package com.esafirm.imagepicker.helper;

import com.esafirm.imagepicker.ResourceTable;
import com.esafirm.imagepicker.features.ImagePickerConfig;
import com.esafirm.imagepicker.features.ReturnMode;
import com.esafirm.imagepicker.features.common.BaseConfig;

import ohos.app.Context;

/**
 * ConfigUtils
 */
public class ConfigUtils {
    /**
     * checkConfig
     * if (config.getMode() != IpCons.MODE_SINGLE
     * && (config.getReturnMode() == ReturnMode.GALLERY_ONLY
     * || config.getReturnMode() == ReturnMode.ALL)) {
     * throw new IllegalStateException("ReturnMode.GALLERY_ONLY and ReturnMode.ALL is only applicable in Single Mode!");
     * }
     *
     * @param config ImagePickerConfig
     * @return ImagePickerConfig
     * @throws IllegalStateException ImagePickerConfig cannot be null
     */
    public static ImagePickerConfig checkConfig(ImagePickerConfig config) {
        if (config == null) {
            throw new IllegalStateException("ImagePickerConfig cannot be null");
        }
        return config;
    }

    /**
     * shouldReturn
     *
     * @param config   BaseConfig
     * @param isCamera boolean
     * @return boolean
     */
    public static boolean shouldReturn(BaseConfig config, boolean isCamera) {
        ReturnMode mode = config.getReturnMode();
        if (isCamera) {
            return mode == ReturnMode.ALL || mode == ReturnMode.CAMERA_ONLY;
        } else {
            return mode == ReturnMode.ALL || mode == ReturnMode.GALLERY_ONLY;
        }
    }

    /**
     * getFolderTitle
     *
     * @param context Context
     * @param config  ImagePickerConfig
     * @return String
     */
    public static String getFolderTitle(Context context, ImagePickerConfig config) {
        final String folderTitle = config.getFolderTitle();
        return ImagePickerUtils.isStringEmpty(folderTitle)
                ? context.getString(ResourceTable.String_ef_title_folder)
                : folderTitle;
    }

    /**
     * getImageTitle
     *
     * @param context Context
     * @param config  ImagePickerConfig
     * @return String
     */
    public static String getImageTitle(Context context, ImagePickerConfig config) {
        final String configImageTitle = config.getImageTitle();
        return ImagePickerUtils.isStringEmpty(configImageTitle)
                ? context.getString(ResourceTable.String_ef_title_select_image)
                : configImageTitle;
    }

    /**
     * getDoneButtonText
     *
     * @param context Context
     * @param config  ImagePickerConfig
     * @return String
     */
    public static String getDoneButtonText(Context context, ImagePickerConfig config) {
        final String doneButtonText = config.getDoneButtonText();
        return ImagePickerUtils.isStringEmpty(doneButtonText)
                ? context.getString(ResourceTable.String_ef_done)
                : doneButtonText;
    }
}
