package com.esafirm.imagepicker.helper;

/**
 * IpCrasher
 */
public class IpCrasher {
    /**
     * openIssue
     *
     * @throws IllegalStateException   IllegalStateException
     */
    public static void openIssue() {
        throw new IllegalStateException("This should not happen. Please open an issue!");
    }
}
