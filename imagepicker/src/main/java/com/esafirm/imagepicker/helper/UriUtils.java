package com.esafirm.imagepicker.helper;

import ohos.utils.net.Uri;

import java.io.File;

import java.io.IOException;

/**
 * UriUtils
 */
public class UriUtils {
    /**
     * return FileProvider.getUriForFile(appContext, providerName, file);
     *
     * @param file File
     * @return Uri
     */
    public static Uri uriForFile(File file) {
        Uri uri = null;
        if (file != null) {
            try {
                uri = Uri.getUriFromFileCanonicalPath(file);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return uri;
    }
}

