/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.esafirm.imagepicker.helper;

import ohos.aafwk.ability.AbilityPackage;

import ohos.app.Context;

import ohos.global.configuration.Configuration;
import ohos.global.configuration.LocaleProfile;

import java.util.Locale;

/**
 * LocaleManager
 */
public class LocaleManager {
    private static String language;

    /**
     * setLanguage
     *
     * @param newLanguage String
     */
    public static void setLanguage(String newLanguage) {
        language = newLanguage;
    }

    private static String getLanguage() {
        return language != null && !language.isEmpty()
                ? language
                : Locale.getDefault().getLanguage();
    }

    /**
     * updateResources
     *
     * @param context Context
     * @param locale  Locale
     */
    public static void updateResources(Context context, Locale locale) {
        Locale.setDefault(locale);
        Configuration configuration = context.getResourceManager().getConfiguration();
        configuration.setLocaleProfile(new LocaleProfile(new Locale[]{locale}));
        ((AbilityPackage) context).getResourceManager().updateConfiguration(configuration, context.getResourceManager().getDeviceCapability());
    }

    /**
     * getSystem
     *
     * @param context Context
     * @return Locale
     */
    public static Locale getSystem(Context context) {
        return context.getResourceManager().getConfiguration().getFirstLocale();
    }
}
