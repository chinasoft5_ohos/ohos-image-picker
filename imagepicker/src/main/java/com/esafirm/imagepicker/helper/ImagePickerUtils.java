package com.esafirm.imagepicker.helper;

import com.esafirm.imagepicker.ResourceTable;
import com.esafirm.imagepicker.features.ImagePickerSavePath;
import com.esafirm.imagepicker.model.Image;

import ohos.agp.utils.TextTool;
import ohos.app.Context;
import ohos.app.Environment;

import ohos.media.photokit.metadata.AVMetadataHelper;

import ohos.utils.net.Uri;

import java.io.File;

import java.net.URLConnection;

import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.Locale;
import java.util.regex.Pattern;

/**
 * ImagePickerUtils
 */
public class ImagePickerUtils {
    private static Context mContext;

    /**
     * init
     *
     * @param context Context
     */
    public static void init(Context context) {
        mContext = context;
    }

    /**
     * isStringEmpty
     *
     * @param str String
     * @return boolean
     */
    public static boolean isStringEmpty(String str) {
        return str == null || str.length() == 0;
    }

    /**
     * createFileInDirectory
     *
     * @param savePath ImagePickerSavePath
     * @param context  Context
     * @return File
     */
    private static File createFileInDirectory(ImagePickerSavePath savePath, Context context) {
        final String path = savePath.getPath();
        File mediaStorageDir;
        if (savePath.isFullPath()) {
            mediaStorageDir = new File(path);
        } else {
            File parent = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
            mediaStorageDir = new File(parent, path);
        }
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        return mediaStorageDir;
    }

    /**
     * createImageFile
     *
     * @param savePath ImagePickerSavePath
     * @param context  Context
     * @return File
     */
    public static File createImageFile(ImagePickerSavePath savePath, Context context) {
        final File mediaStorageDir = createFileInDirectory(savePath, context);
        if (mediaStorageDir == null) {
            return null;
        }
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss_SSS", Locale.getDefault()).format(new Date());
        File result = new File(mediaStorageDir, "IMG_" + timeStamp + ".jpg");
        int counter = 0;
        while (result.exists()) {
            counter++;
            result = new File(mediaStorageDir, "IMG_" + timeStamp + "(" + counter + ").jpg");
        }
        return result;
    }

    /**
     * getNameFromFilePath
     *
     * @param path String
     * @return String
     */
    public static String getNameFromFilePath(String path) {
        if (path.contains(File.separator)) {
            return path.substring(path.lastIndexOf(File.separator) + 1);
        }
        return path;
    }

    /**
     * isGifFormat
     *
     * @param image Image
     * @return boolean
     */
    public static boolean isGifFormat(Image image) {
        return isGifFormat(image.getPath());
    }

    /**
     * isGifFormat
     *
     * @param path String
     * @return boolean
     */
    public static boolean isGifFormat(String path) {
        String extension = getExtension(path);
        return extension.equalsIgnoreCase("gif");
    }

    /**
     * isVideoFormat
     *
     * @param image Image
     * @return boolean
     */
    public static boolean isVideoFormat(Image image) {
        String extension = getExtension(image.getPath());
        String mimeType = TextTool.isNullOrEmpty(extension)
                ? URLConnection.guessContentTypeFromName(image.getPath())
                : MimeUtils.guessMimeTypeFromExtension(extension);
        return mimeType != null && mimeType.startsWith("video");
    }

    /**
     * getVideoDurationLabel
     *
     * @param context Context
     * @param file    File
     * @return String
     */
    public static String getVideoDurationLabel(Context context, File file) {
        AVMetadataHelper retriever = new AVMetadataHelper();
        retriever.setSource(context, Uri.getUriFromFile(file));
        Long duration = Long.parseLong(0 + "");
        if (retriever.resolveMetadata(AVMetadataHelper.AV_KEY_DURATION) != null) {
            duration = Long.parseLong(retriever.resolveMetadata(AVMetadataHelper.AV_KEY_DURATION));
        }
        retriever.release();
        long second = (duration / 1000) % 60;
        long minute = (duration / (1000 * 60)) % 60;
        long hour = (duration / (1000 * 60 * 60)) % 24;
        if (hour > 0) {
            return String.format("%02d:%02d:%02d", hour, minute, second);
        } else {
            return String.format("%02d:%02d", minute, second);
        }
    }

    /**
     * getVideoDurationLabel
     *
     * @param duration long
     * @return String
     */
    public static String getVideoDurationLabel(long duration) {
        long second = (duration / 1000) % 60;
        long minute = (duration / (1000 * 60)) % 60;
        long hour = (duration / (1000 * 60 * 60)) % 24;
        if (hour > 0) {
            return String.format("%02d:%02d:%02d", hour, minute, second);
        } else {
            return String.format("%02d:%02d", minute, second);
        }
    }

    /**
     * getExtension
     *
     * @param path String
     * @return String
     */
    private static String getExtension(String path) {
        String extension = getFileExtensionFromUrl(path);
        if (!TextTool.isNullOrEmpty(extension)) {
            return extension;
        }
        if (path.contains(".")) {
            return path.substring(path.lastIndexOf(".") + 1, path.length());
        } else {
            return "";
        }
    }

    /**
     * getFileExtensionFromUrl
     *
     * @param url String
     * @return String
     */
    public static String getFileExtensionFromUrl(String url) {
        String newStr = "";
        if (!TextTool.isNullOrEmpty(url)) {
            int fragment = url.lastIndexOf('#');
            if (fragment > 0) {
                newStr = url.substring(0, fragment);
            }
            int query = newStr.lastIndexOf('?');
            if (query > 0) {
                newStr = newStr.substring(0, query);
            }
            int filenamePos = newStr.lastIndexOf(getText(ResourceTable.String_xie_gang));
            String filename =
                    filenamePos >= 0 ? newStr.substring(filenamePos + 1) : newStr;
            if (!filename.isEmpty() &&
                    Pattern.matches(ResourceUtil.getText(mContext, ResourceTable.String_pattern), filename)) {
                int dotPos = filename.lastIndexOf('.');
                if (dotPos >= 0) {
                    return filename.substring(dotPos + 1);
                }
            }
        }
        return "";
    }

    /**
     * getPath
     *
     * @return String
     */
    public static String getPath() {
        return ResourceUtil.getText(mContext, ResourceTable.String_path_Camera);
    }

    /**
     * getText
     *
     * @param res int
     * @return String
     */
    public static String getText(int res) {
        return mContext.getString(res);
    }
}
