package com.esafirm.imagepicker.helper;

import com.esafirm.imagepicker.ResourceTable;

import ohos.agp.components.element.Element;
import ohos.agp.components.element.PixelMapElement;
import ohos.app.Context;

import ohos.global.resource.NotExistException;

import java.io.IOException;

/**
 *
 */
public class ViewUtils {

    /**
     * getArrowIcon
     *
     * @param context Context
     * @return 返回图片的element
     */
    public static Element getArrowIcon(Context context) {
        final int backResourceId;
        if (context.getResourceManager().getConfiguration().isLayoutRTL) {
            backResourceId = ResourceTable.Media_ef_ic_arrow_forward;
        } else {
            backResourceId = ResourceTable.Media_ef_ic_arrow_back;
        }
        return getImageDrawable(context.getApplicationContext(), backResourceId);
    }

    /**
     * getImageDrawable
     *
     * @param context Context
     * @param res     int
     * @return Element
     */
    public static Element getImageDrawable(Context context, int res) {
        PixelMapElement drawable = null;
        try {
            drawable = new PixelMapElement(
                    context.getResourceManager().getResource(res));
        } catch (IOException | NotExistException e) {
            LogUtil.loge(e.getMessage());
        }
        return drawable;
    }
}
