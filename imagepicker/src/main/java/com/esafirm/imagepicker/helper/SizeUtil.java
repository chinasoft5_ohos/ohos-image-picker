/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.esafirm.imagepicker.helper;

import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayManager;

import ohos.app.Context;

/**
 * @Author: AriesHoo on 2019/4/12 11:19
 * @E-Mail: AriesHoo@126.com
 * @Function: 尺寸转换工具
 * @Description:
 */
public class SizeUtil {
    private static Display display;

    /**
     * getDisplayMetrics
     *
     * @param context Context
     */
    public static void getDisplayMetrics(Context context) {
        display = DisplayManager.getInstance().getDefaultDisplay(context).get();
    }

    /**
     * 获取屏幕宽度
     *
     * @return int
     */
    public static int getScreenWidth() {
        return display.getAttributes().width;
    }

    /**
     * 获取屏幕高度
     *
     * @return int
     */
    public static int getScreenHeight() {
        return display.getAttributes().height;
    }

    /**
     * px2dp
     *
     * @param pxValue float
     * @return int
     */
    public static int px2dp(float pxValue) {
        final float scale = display.getAttributes().densityPixels;
        return (int) (pxValue / scale + 0.5f);
    }

    /**
     * dp2px
     *
     * @param dpValue float
     * @return int
     */
    public static int dp2px(float dpValue) {
        final float scale = display.getAttributes().densityPixels;
        return (int) (dpValue * scale + 0.5f);
    }

    /**
     * px2sp
     *
     * @param pxValue float
     * @return int
     */
    public static int px2sp(float pxValue) {
        final float fontScale = display.getAttributes().scalDensity;
        return (int) (pxValue / fontScale + 0.5f);
    }

    /**
     * sp2px
     *
     * @param spValue float
     * @return int
     */
    public static int sp2px(float spValue) {
        final float fontScale = display.getAttributes().scalDensity;
        return (int) (spValue * fontScale + 0.5f);
    }
}
