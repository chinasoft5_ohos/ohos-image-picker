/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.esafirm.imagepicker.helper;

import com.esafirm.imagepicker.ResourceTable;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * MimeUtils
 */
public final class MimeUtils {
    private static  Map<String, String> mimeTypeToExtensionMap = new HashMap<String, String>();
    private static  Map<String, String> extensionToMimeTypeMap = new HashMap<String, String>();

    static {
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "andrew-inset", "ez");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "dsptype", "tsp");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "hta", "hta");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "mac-binhex40", "hqx");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "mathematica", "nb");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "msaccess", "mdb");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "oda", "oda");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "ogg", "ogg");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "ogg", "oga");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "pdf", "pdf");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "pgp-keys", "key");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "pgp-signature", "pgp");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "pics-rules", "prf");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "pkix-cert", "cer");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "rar", "rar");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "rdf+xml", "rdf");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "rss+xml", "rss");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "zip", "zip");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "vnd.cinderella", "cdy");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "vnd.ms-pki.stl", "stl");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "vnd.oasis.opendocument.database", "odb");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "vnd.oasis.opendocument.formula", "odf");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "vnd.oasis.opendocument.graphics", "odg");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "vnd.oasis.opendocument.graphics-template", "otg");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "vnd.oasis.opendocument.image", "odi");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "vnd.oasis.opendocument.spreadsheet", "ods");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "vnd.oasis.opendocument.spreadsheet-template", "ots");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "vnd.oasis.opendocument.text", "odt");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "vnd.oasis.opendocument.text-master", "odm");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "vnd.oasis.opendocument.text-template", "ott");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "vnd.oasis.opendocument.text-web", "oth");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "msword", "doc");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "msword", "dot");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "vnd.openxmlformats-officedocument.wordprocessingml.document", "docx");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "vnd.openxmlformats-officedocument.wordprocessingml.template", "dotx");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "vnd.ms-excel", "xls");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "vnd.ms-excel", "xlt");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "vnd.openxmlformats-officedocument.spreadsheetml.sheet", "xlsx");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "vnd.openxmlformats-officedocument.spreadsheetml.template", "xltx");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "vnd.ms-powerpoint", "ppt");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "vnd.ms-powerpoint", "pot");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "vnd.ms-powerpoint", "pps");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "vnd.openxmlformats-officedocument.presentationml.presentation", "pptx");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "vnd.openxmlformats-officedocument.presentationml.template", "potx");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "vnd.openxmlformats-officedocument.presentationml.slideshow", "ppsx");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "vnd.rim.cod", "cod");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "vnd.smaf", "mmf");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "vnd.stardivision.calc", "sdc");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "vnd.stardivision.draw", "sda");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "vnd.stardivision.impress", "sdd");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "vnd.stardivision.impress", "sdp");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "vnd.stardivision.math", "smf");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "vnd.stardivision.writer", "sdw");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "vnd.stardivision.writer", "vor");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "vnd.stardivision.writer-global", "sgl");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "vnd.sun.xml.calc", "sxc");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "vnd.sun.xml.calc.template", "stc");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "vnd.sun.xml.draw", "sxd");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "vnd.sun.xml.draw.template", "std");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "vnd.sun.xml.impress", "sxi");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "vnd.sun.xml.impress.template", "sti");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "vnd.sun.xml.math", "sxm");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "vnd.sun.xml.writer", "sxw");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "vnd.sun.xml.writer.global", "sxg");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "vnd.sun.xml.writer.template", "stw");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "vnd.visio", "vsd");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-abiword", "abw");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-apple-diskimage", "dmg");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-bcpio", "bcpio");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-bittorrent", "torrent");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-cdf", "cdf");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-cdlink", "vcd");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-chess-pgn", "pgn");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-cpio", "cpio");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-debian-package", "deb");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-debian-package", "udeb");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-director", "dcr");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-director", "dir");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-director", "dxr");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-dms", "dms");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-doom", "wad");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-dvi", "dvi");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-font", "pfa");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-font", "pfb");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-font", "gsf");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-font", "pcf");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-font", "pcf.Z");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-freemind", "mm");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-futuresplash", "spl");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "futuresplash", "spl");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-gnumeric", "gnumeric");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-go-sgf", "sgf");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-graphing-calculator", "gcf");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-gtar", "tgz");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-gtar", "gtar");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-gtar", "taz");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-hdf", "hdf");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-ica", "ica");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-internet-signup", "ins");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-internet-signup", "isp");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-iphone", "iii");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-iso9660-image", "iso");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-jmol", "jmz");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-kchart", "chrt");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-killustrator", "kil");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-koan", "skp");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-koan", "skd");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-koan", "skt");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-koan", "skm");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-kpresenter", "kpr");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-kpresenter", "kpt");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-kspread", "ksp");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-kword", "kwd");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-kword", "kwt");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-latex", "latex");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-lha", "lha");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-lzh", "lzh");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-lzx", "lzx");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-maker", "frm");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-maker", "maker");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-maker", "frame");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-maker", "fb");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-maker", "book");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-maker", "fbdoc");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-mif", "mif");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-ms-wmd", "wmd");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-ms-wmz", "wmz");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-msi", "msi");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-ns-proxy-autoconfig", "pac");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-nwc", "nwc");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-object", "o");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-oz-application", "oza");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-pem-file", "pem");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-pkcs12", "p12");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-pkcs12", "pfx");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-pkcs7-certreqresp", "p7r");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-pkcs7-crl", "crl");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-quicktimeplayer", "qtl");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-shar", "shar");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-shockwave-flash", "swf");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-stuffit", "sit");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-sv4cpio", "sv4cpio");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-sv4crc", "sv4crc");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-tar", "tar");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-texinfo", "texinfo");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-texinfo", "texi");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-troff", "t");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-troff", "roff");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-troff-man", "man");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-ustar", "ustar");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-wais-source", "src");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-wingz", "wz");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-webarchive", "webarchive");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-webarchive-xml", "webarchivexml");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-x509-ca-cert", "crt");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-x509-user-cert", "crt");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-x509-server-cert", "crt");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-xcf", "xcf");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-xfig", "fig");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "xhtml+xml", "xhtml");
        add("audio" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "3gpp", "3gpp");
        add("audio" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "aac", "aac");
        add("audio" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "aac-adts", "aac");
        add("audio" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "amr", "amr");
        add("audio" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "amr-wb", "awb");
        add("audio" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "basic", "snd");
        add("audio" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "flac", "flac");
        add("application" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-flac", "flac");
        add("audio" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "imelody", "imy");
        add("audio" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "midi", "mid");
        add("audio" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "midi", "midi");
        add("audio" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "midi", "ota");
        add("audio" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "midi", "kar");
        add("audio" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "midi", "rtttl");
        add("audio" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "midi", "xmf");
        add("audio" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "mobile-xmf", "mxmf");
        // add ".mp3" first so it will be the default for guessExtensionFromMimeType
        add("audio" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "mpeg", "mp3");
        add("audio" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "mpeg", "mpga");
        add("audio" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "mpeg", "mpega");
        add("audio" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "mpeg", "mp2");
        add("audio" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "mpeg", "m4a");
        add("audio" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "mpegurl", "m3u");
        add("audio" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "prs.sid", "sid");
        add("audio" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-aiff", "aif");
        add("audio" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-aiff", "aiff");
        add("audio" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-aiff", "aifc");
        add("audio" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-gsm", "gsm");
        add("audio" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-matroska", "mka");
        add("audio" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-mpegurl", "m3u");
        add("audio" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-ms-wma", "wma");
        add("audio" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-ms-wax", "wax");
        add("audio" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-pn-realaudio", "ra");
        add("audio" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-pn-realaudio", "rm");
        add("audio" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-pn-realaudio", "ram");
        add("audio" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-realaudio", "ra");
        add("audio" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-scpls", "pls");
        add("audio" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-sd2", "sd2");
        add("audio" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-wav", "wav");
        add("image" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-ms-bmp", "bmp");
        add("image" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "bmp", "bmp");
        add("image" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "gif", "gif");
        add("image" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-icon", "ico");
        add("image" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "ico", "cur");
        add("image" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "ico", "ico");
        add("image" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "ief", "ief");
        add("image" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "jpeg", "jpeg");
        add("image" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "jpeg", "jpg");
        add("image" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "jpeg", "jpe");
        add("image" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "pcx", "pcx");
        add("image" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "png", "png");
        add("image" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "svg+xml", "svg");
        add("image" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "svg+xml", "svgz");
        add("image" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "tiff", "tiff");
        add("image" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "tiff", "tif");
        add("image" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "vnd.djvu", "djvu");
        add("image" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "vnd.djvu", "djv");
        add("image" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "vnd.wap.wbmp", "wbmp");
        add("image" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "webp", "webp");
        add("image" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-cmu-raster", "ras");
        add("image" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-coreldraw", "cdr");
        add("image" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-coreldrawpattern", "pat");
        add("image" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-coreldrawtemplate", "cdt");
        add("image" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-corelphotopaint", "cpt");
        add("image" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-jg", "art");
        add("image" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-jng", "jng");
        add("image" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-photoshop", "psd");
        add("image" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-portable-anymap", "pnm");
        add("image" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-portable-bitmap", "pbm");
        add("image" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-portable-graymap", "pgm");
        add("image" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-portable-pixmap", "ppm");
        add("image" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-rgb", "rgb");
        add("image" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-xbitmap", "xbm");
        add("image" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-xpixmap", "xpm");
        add("image" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-xwindowdump", "xwd");
        add("model" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "iges", "igs");
        add("model" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "iges", "iges");
        add("model" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "mesh", "msh");
        add("model" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "mesh", "mesh");
        add("model" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "mesh", "silo");
        add("text" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "calendar", "ics");
        add("text" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "calendar", "icz");
        add("text" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "comma-separated-values", "csv");
        add("text" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "css", "css");
        add("text" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "html", "htm");
        add("text" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "html", "html");
        add("text" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "h323", "323");
        add("text" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "iuls", "uls");
        add("text" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "mathml", "mml");
        add("text" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "plain", "txt");
        add("text" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "plain", "asc");
        add("text" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "plain", "text");
        add("text" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "plain", "diff");
        add("text" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "plain", "po");
        // reserve "pot" for vnd.ms-powerpoint
        add("text" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "richtext", "rtx");
        add("text" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "rtf", "rtf");
        add("text" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "text", "phps");
        add("text" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "tab-separated-values", "tsv");
        add("text" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "xml", "xml");
        add("text" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-bibtex", "bib");
        add("text" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-boo", "boo");
        add("text" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-c++hdr", "hpp");
        add("text" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-c++hdr", "h++");
        add("text" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-c++hdr", "hxx");
        add("text" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-c++hdr", "hh");
        add("text" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-c++src", "cpp");
        add("text" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-c++src", "c++");
        add("text" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-c++src", "cc");
        add("text" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-c++src", "cxx");
        add("text" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-chdr", "h");
        add("text" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-component", "htc");
        add("text" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-csh", "csh");
        add("text" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-csrc", "c");
        add("text" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-dsrc", "d");
        add("text" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-haskell", "hs");
        add("text" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-java", "java");
        add("text" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-literate-haskell", "lhs");
        add("text" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-moc", "moc");
        add("text" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-pascal", "p");
        add("text" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-pascal", "pas");
        add("text" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-pcs-gcd", "gcd");
        add("text" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-setext", "etx");
        add("text" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-tcl", "tcl");
        add("text" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-tex", "tex");
        add("text" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-tex", "ltx");
        add("text" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-tex", "sty");
        add("text" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-tex", "cls");
        add("text" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-vcalendar", "vcs");
        add("text" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-vcard", "vcf");
        add("video" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "3gpp", "3gpp");
        add("video" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "3gpp", "3gp");
        add("video" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "3gpp2", "3gpp2");
        add("video" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "3gpp2", "3g2");
        add("video" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "avi", "avi");
        add("video" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "dl", "dl");
        add("video" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "dv", "dif");
        add("video" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "dv", "dv");
        add("video" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "fli", "fli");
        add("video" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "m4v", "m4v");
        add("video" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "mp2ts", "ts");
        add("video" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "mpeg", "mpeg");
        add("video" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "mpeg", "mpg");
        add("video" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "mpeg", "mpe");
        add("video" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "mp4", "mp4");
        add("video" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "mpeg", "VOB");
        add("video" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "quicktime", "qt");
        add("video" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "quicktime", "mov");
        add("video" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "vnd.mpegurl", "mxu");
        add("video" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "webm", "webm");
        add("video" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-la-asf", "lsf");
        add("video" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-la-asf", "lsx");
        add("video" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-matroska", "mkv");
        add("video" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-mng", "mng");
        add("video" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-ms-asf", "asf");
        add("video" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-ms-asf", "asx");
        add("video" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-ms-wm", "wm");
        add("video" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-ms-wmv", "wmv");
        add("video" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-ms-wmx", "wmx");
        add("video" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-ms-wvx", "wvx");
        add("video" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-sgi-movie", "movie");
        add("video" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-webex", "wrf");
        add("x-conference" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-cooltalk", "ice");
        add("x-epoc" + ImagePickerUtils.getText(ResourceTable.String_xie_gang) + "x-sisx-app", "sisx");
        applyOverrides();
    }

    /**
     * If we have an existing x -> y mapping, we do not want to
     * override it with another mapping x -> y2.
     * If a mime type maps to several extensions
     * the first extension added is considered the most popular
     * so we do not want to overwrite it later.
     *
     * @param mimeType  String
     * @param extension String
     */
    private static void add(String mimeType, String extension) {
        if (!mimeTypeToExtensionMap.containsKey(mimeType)) {
            mimeTypeToExtensionMap.put(mimeType, extension);
        }
        if (!extensionToMimeTypeMap.containsKey(extension)) {
            extensionToMimeTypeMap.put(extension, mimeType);
        }
    }

    private static InputStream getContentTypesPropertiesStream() {
        String userTable = System.getProperty("content.types.user.table");
        if (userTable != null) {
            File f = new File(userTable);
            if (f.exists()) {
                try {
                    return new FileInputStream(f);
                } catch (IOException ignored) {
                    LogUtil.loge(ignored.getMessage());
                }
            }
        }
        File f = new File(System.getProperty("java.home"), "lib" + File.separator + "content-types.properties");
        if (f.exists()) {
            try {
                return new FileInputStream(f);
            } catch (IOException ignored) {
                LogUtil.loge(ignored.getMessage());
            }
        }
        return null;
    }

    /**
     * This isn't what the RI does. The RI doesn't have hard-coded defaults, so supplying your
     * own "content.types.user.table" means you don't get any of the built-ins, and the built-ins
     * come from "$JAVA_HOME/lib/content-types.properties".
     */
    private static void applyOverrides() {
        InputStream stream = getContentTypesPropertiesStream();
        if (stream == null) {
            return;
        }
        try {
            Properties overrides = new Properties();
            overrides.load(stream);
            for (Map.Entry<Object, Object> entry : overrides.entrySet()) {
                String extension = (String) entry.getKey();
                String mimeType = (String) entry.getValue();
                add(mimeType, extension);
            }
        } catch (IOException ignored) {
            LogUtil.loge(ignored.getMessage());
        } finally {
            try {
                stream.close();
            } catch (IOException e) {
                LogUtil.loge(e.getMessage());
            }
        }
    }

    private MimeUtils() {
    }

    /**
     * Returns true if the given MIME type has an entry in the map.
     *
     * @param mimeType A MIME type (i.e. text/plain)
     * @return True iff there is a mimeType entry in the map.
     */
    public static boolean hasMimeType(String mimeType) {
        if (mimeType == null || mimeType.isEmpty()) {
            return false;
        } else {
        }
        return mimeTypeToExtensionMap.containsKey(mimeType);
    }

    /**
     * Returns the MIME type for the given extension.
     *
     * @param extension A file extension without the leading '.'
     * @return The MIME type for the given extension or null iff there is none.
     */
    public static String guessMimeTypeFromExtension(String extension) {
        if (extension == null || extension.isEmpty()) {
            return null;
        } else {
        }
        return extensionToMimeTypeMap.get(extension);
    }

    /**
     * Returns true if the given extension has a registered MIME type.
     *
     * @param extension A file extension without the leading '.'
     * @return True iff there is an extension entry in the map.
     */
    public static boolean hasExtension(String extension) {
        if (extension == null || extension.isEmpty()) {
            return false;
        } else {
        }
        return extensionToMimeTypeMap.containsKey(extension);
    }

    /**
     * Returns the registered extension for the given MIME type. Note that some
     * MIME types map to multiple extensions. This call will return the most
     * common extension for the given MIME type.
     *
     * @param mimeType A MIME type (i.e. text/plain)
     * @return The extension for the given MIME type or null iff there is none.
     */
    public static String guessExtensionFromMimeType(String mimeType) {
        if (mimeType == null || mimeType.isEmpty()) {
            return null;
        } else {
        }
        return mimeTypeToExtensionMap.get(mimeType);
    }
}

