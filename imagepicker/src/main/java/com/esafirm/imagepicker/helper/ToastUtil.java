/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.esafirm.imagepicker.helper;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Text;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.utils.TextAlignment;
import ohos.agp.window.dialog.ToastDialog;
import ohos.agp.window.service.DisplayAttributes;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;

/**
 * description : 关于操作Toast的工具类
 *
 * @since 2021-04-12
 */
public class ToastUtil {
    /**
     * LENGTH_LONG 的显示时长
     */
    public static final int LENGTH_LONG = 4000;
    /**
     * LENGTH_SHORT 的显示时长
     */
    public static final int LENGTH_SHORT = 3000;

    /**
     * ToastLayout
     */
    private enum ToastLayout {
        DEFAULT,
        CENTER,
        TOP,
        BOTTOM,
    }

    /**
     * 短时显示toast
     *
     * @param mContext context
     * @param content  toast内容
     */
    public static void showShort(Context mContext, String content) {
        createTost(mContext, content, LENGTH_SHORT, ToastLayout.BOTTOM);
    }

    /**
     * 长时显示toast
     *
     * @param mContext context
     * @param content  toast内容
     */
    public static void showLong(Context mContext, String content) {
        createTost(mContext, content, LENGTH_LONG, ToastLayout.DEFAULT);
    }

    /**
     * 显示toast
     *
     * @param mContext context
     * @param content  toast内容
     */
    public static void show(Context mContext, String content) {
        createTost(mContext, content, LENGTH_SHORT, ToastLayout.DEFAULT);
    }

    /**
     * 显示toast
     *
     * @param mContext context
     * @param content  toast内容
     * @param duration toast显示时长
     */
    public static void show(Context mContext, String content, int duration) {
        createTost(mContext, content, duration, ToastLayout.DEFAULT);
    }

    /**
     * 显示toast
     *
     * @param mContext context
     * @param content  toast内容
     * @param layout   显示toast的布局
     */
    public static void show(Context mContext, String content, ToastLayout layout) {
        createTost(mContext, content, LENGTH_SHORT, layout);
    }

    /**
     * 显示toast
     *
     * @param mContext context
     * @param content  toast内容
     * @param duration toast显示时长
     * @param layout   显示toast的布局
     */
    public static void show(Context mContext, String content, int duration, ToastLayout layout) {
        createTost(mContext, content, duration, layout);
    }

    /**
     * 显示toast
     *
     * @param mContext context
     * @param content  toast内容
     */
    public static void showShort(Context mContext, int content) {
        createTost(mContext, getString(mContext, content), LENGTH_SHORT, ToastLayout.DEFAULT);
    }

    /**
     * 长时显示toast
     *
     * @param mContext context
     * @param content  toast内容
     */
    public static void showLong(Context mContext, int content) {
        createTost(mContext, getString(mContext, content), LENGTH_LONG, ToastLayout.DEFAULT);
    }

    /**
     * 显示toast
     *
     * @param mContext context
     * @param content  toast内容
     */
    public static void show(Context mContext, int content) {
        createTost(mContext, getString(mContext, content), LENGTH_SHORT, ToastLayout.DEFAULT);
    }

    /**
     * 显示toast
     *
     * @param mContext context
     * @param content  toast内容
     * @param duration toast显示时长
     */
    public static void show(Context mContext, int content, int duration) {
        createTost(mContext, getString(mContext, content), duration, ToastLayout.DEFAULT);
    }

    /**
     * 显示toast
     *
     * @param mContext context
     * @param content  toast内容
     * @param layout   显示toast的布局
     */
    public static void show(Context mContext, int content, ToastLayout layout) {
        createTost(mContext, getString(mContext, content), LENGTH_SHORT, layout);
    }

    /**
     * 显示toast
     *
     * @param mContext context
     * @param content  toast内容
     * @param duration toast显示时长
     * @param layout   显示toast的布局
     */
    public static void show(Context mContext, int content, int duration, ToastLayout layout) {
        createTost(mContext, getString(mContext, content), duration, layout);
    }

    private static void createTost(Context mContext, String content, int duration, ToastLayout layout) {
        DirectionalLayout toastLayout = new DirectionalLayout(mContext);
        DirectionalLayout.LayoutConfig toastLayoutConfig =
                new DirectionalLayout.LayoutConfig(DirectionalLayout.LayoutConfig.MATCH_PARENT,
                        DirectionalLayout.LayoutConfig.MATCH_CONTENT);
        toastLayout.setAlignment(LayoutAlignment.CENTER);
        toastLayout.setLayoutConfig(toastLayoutConfig);

        DirectionalLayout.LayoutConfig textConfig =
                new DirectionalLayout.LayoutConfig(DirectionalLayout.LayoutConfig.MATCH_CONTENT,
                        DirectionalLayout.LayoutConfig.MATCH_CONTENT);
        Text text = new Text(mContext);
        text.setText(content);
        text.setTextColor(new Color(Color.getIntColor("#ffffff")));
        text.setPadding(vp2px(mContext, 16), vp2px(mContext, 4),
                vp2px(mContext, 16), vp2px(mContext, 4));
        text.setTextSize(vp2px(mContext, 12));
        text.setBackground(buildDrawableByColorRadius(Color.getIntColor("#70000000"), vp2px(mContext, 20)));
        text.setLayoutConfig(textConfig);
        text.setMaxTextWidth(SizeUtil.getScreenWidth() * 4 / 5);
        text.setMultipleLine(true);
        text.setMaxTextLines(5);
        text.setTextAlignment(TextAlignment.CENTER);
        toastLayout.addComponent(text);
        int mLayout = LayoutAlignment.CENTER | LayoutAlignment.BOTTOM;
        ToastDialog toastDialog = new ToastDialog(mContext);
        toastDialog.setComponent(toastLayout);
        toastDialog.setSize(DirectionalLayout.LayoutConfig.MATCH_CONTENT,
                DirectionalLayout.LayoutConfig.MATCH_CONTENT);
        toastDialog.setAlignment(mLayout);
        toastDialog.setTransparent(true);
        toastDialog.setDuration(duration);
        toastDialog.setOffset(0, 500);
        toastDialog.show();
    }

    private static String getString(Context mContent, int resId) {
        try {
            return mContent.getResourceManager().getElement(resId).getString();
        } catch (NotExistException | WrongTypeException | IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    private static ohos.agp.components.element.Element buildDrawableByColorRadius(int color, float radius) {
        ShapeElement drawable = new ShapeElement();
        drawable.setShape(0);
        drawable.setRgbColor(RgbColor.fromArgbInt(color));
        drawable.setCornerRadius(radius);
        return drawable;
    }

    private static int vp2px(Context context, float vp) {
        DisplayAttributes attributes = DisplayManager.getInstance().getDefaultDisplay(context).get().getAttributes();
        return (int) (attributes.densityPixels * vp);
    }
}
