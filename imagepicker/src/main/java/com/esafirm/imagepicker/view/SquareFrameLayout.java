package com.esafirm.imagepicker.view;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.StackLayout;
import ohos.app.Context;

/**
 * SquareFrameLayout
 */
public class SquareFrameLayout extends StackLayout implements Component.EstimateSizeListener {
    private Image componentAt;

    /**
     * 初始化
     *
     * @param context Context
     */
    public SquareFrameLayout(Context context) {
        super(context);
        init();
    }

    /**
     * 初始化
     *
     * @param context Context
     * @param attrSet AttrSet
     */
    public SquareFrameLayout(Context context, AttrSet attrSet) {
        super(context, attrSet);
        init();
    }

    /**
     * 初始化
     *
     * @param context   Context
     * @param attrSet   AttrSet
     * @param styleName String
     */
    public SquareFrameLayout(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init();
    }

    private void init() {
        setEstimateSizeListener(this);
    }

    @Override
    public boolean onEstimateSize(int i, int i1) {
        int width = getWidth();
        setEstimatedSize(width, width);
        componentAt = (Image) getComponentAt(0);
        Component alphaView = getComponentAt(1);
        Component text = getComponentAt(3);
        Component image = getComponentAt(2);
        if (componentAt != null) {
            componentAt.estimateSize(i, i);
        }
        if (alphaView != null) {
            alphaView.estimateSize(i, i);
        }
        if (text != null) {
            text.estimateSize(i1, i1);
        }
        if (image != null) {
            image.estimateSize(i, i1);
        }
        return true;
    }
}