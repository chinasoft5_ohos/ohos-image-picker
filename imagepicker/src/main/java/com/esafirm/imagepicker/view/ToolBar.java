/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.esafirm.imagepicker.view;

import com.esafirm.imagepicker.ResourceTable;

import ohos.agp.components.*;
import ohos.app.Context;

/**
 * ToolBar
 */
public class ToolBar extends StackLayout {
    private Image back;
    private Image camara;
    private Text title;
    private Text finish;
    private ClickListener listener;

    /**
     * 初始化
     *
     * @param context Context
     */
    public ToolBar(Context context) {
        this(context, null);
    }

    /**
     * 初始化
     *
     * @param context Context
     * @param attrSet AttrSet
     */
    public ToolBar(Context context, AttrSet attrSet) {
        this(context, attrSet, "");
    }

    /**
     * 初始化
     *
     * @param context   Context
     * @param attrSet   AttrSet
     * @param styleName String
     */
    public ToolBar(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init(context);
    }

    private void init(Context context) {
        Component parse = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_ef_toolbar, null, false);
        addComponent(parse);
        back = (Image) parse.findComponentById(ResourceTable.Id_back);
        camara = (Image) parse.findComponentById(ResourceTable.Id_camara);
        title = (Text) parse.findComponentById(ResourceTable.Id_title);
        finish = (Text) parse.findComponentById(ResourceTable.Id_finish);
        back.setClickedListener(new ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (listener != null) {
                    listener.click(1);
                }
            }
        });
        camara.setClickedListener(new ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (listener != null) {
                    listener.click(2);
                }
            }
        });
        finish.setClickedListener(new ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (listener != null) {
                    listener.click(3);
                }
            }
        });
    }

    /**
     * isTitle
     *
     * @param content String
     */
    public void isTitle(String content) {
        back.setVisibility(Component.HIDE);
        finish.setVisibility(Component.HIDE);
        camara.setVisibility(Component.HIDE);
        title.setText(content);
    }

    /**
     * setVisible
     *
     * @param visible boolean
     */
    public void setVisible(boolean visible) {
        finish.setVisibility(visible ? Component.VISIBLE : Component.HIDE);
    }

    /**
     * setTitle
     *
     * @param text String
     */
    public void setTitle(String text) {
        title.setText(text);
    }

    /**
     * setOnActionClickListener
     *
     * @param lisenter ClickListener
     */
    public void setOnActionClickListener(ClickListener lisenter) {
        this.listener = lisenter;
    }

    /**
     * ClickListener
     */
    public interface ClickListener {
        void click(int id);
    }

}
