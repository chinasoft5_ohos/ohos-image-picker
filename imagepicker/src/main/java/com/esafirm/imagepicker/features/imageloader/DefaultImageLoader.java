package com.esafirm.imagepicker.features.imageloader;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import com.esafirm.imagepicker.model.Image;

/**
 * DefaultImageLoader
 */
public class DefaultImageLoader implements ImageLoader {

    /**
     * loadImage
     *
     * @param image     Image
     * @param imageView imageView
     * @param imageType ImageType
     */
    @Override
    public void loadImage(Image image, ohos.agp.components.Image imageView, ImageType imageType) {
        Glide.with(imageView.getContext())
                .load(image.getUriSchema())
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .centerCrop()
                .into(imageView);
    }
}
