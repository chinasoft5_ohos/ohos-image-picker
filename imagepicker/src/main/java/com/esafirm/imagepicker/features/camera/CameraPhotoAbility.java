/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.esafirm.imagepicker.features.camera;

import com.esafirm.imagepicker.ResourceTable;
import com.esafirm.imagepicker.helper.FileUtils;
import com.esafirm.imagepicker.helper.ImagePickerUtils;
import com.esafirm.imagepicker.helper.LogUtil;
import com.esafirm.imagepicker.model.Options;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.PixelMapElement;
import ohos.app.dispatcher.TaskDispatcher;
import ohos.app.dispatcher.task.Revocable;

import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;

import ohos.global.resource.NotExistException;

import ohos.media.camera.params.Metadata;
import ohos.media.image.ImageReceiver;
import ohos.media.image.common.ImageFormat;

import ohos.multimodalinput.event.TouchEvent;

import ohos.utils.net.Uri;

import java.io.*;

import java.util.*;

/**
 * CameraPhotoAbility
 */
public class CameraPhotoAbility extends Ability implements Component.ClickedListener {
    /**
     * Pix选中的图片的list集合
     */
    public static final String IMAGE_RESULTS = "image_result";
    private static final String TAG = "TakePictureAbilitySlice";
    private static final int NOTIFY_PROVIDER = 100;
    private EventHandler cameraEventHandler;
    private EventHandler mainHandler;
    private CameraView cameraView;
    private String picturePath;
    private int flashMode = Metadata.FlashMode.FLASH_CLOSE;
    private String pictureName = "picture_001.jpg";
    private Image takePic;
    private Image takePicBg;
    private StackLayout bottomButtons;
    private StackLayout flash;
    private StackLayout shiftCamera;
    private Options options = null;
    private ArrayList<com.esafirm.imagepicker.model.Image> mList = new ArrayList<>();
    private TaskDispatcher mParallelTaskDispatcher;
    private Revocable mRevocable;
    private Image previewPhoto;
    private Image back;
    private Image confirm;
    private File myFile;
    private String absolutePath;
    private ImageReceiver imageReceiver;
    private DependentLayout selectImage;
    private final ImageReceiver.IImageArrivalListener imageArrivalListener = new ImageReceiver.IImageArrivalListener() {
        @Override
        public void onImageArrival(ImageReceiver imageReceiver1) {
            imageReceiver = imageReceiver1;
            pictureName = Calendar.getInstance().getTimeInMillis() + ".jpg";
            myFile = new File(picturePath, pictureName);
            absolutePath = null;
            parseData();
            mainHandler.postTask(new Runnable() {
                @Override
                public void run() {
                    bottomButtons.setVisibility(Component.HIDE);
                    cameraView.setVisibility(Component.HIDE);
                    selectImage.setVisibility(Component.VISIBLE);
                    previewPhoto.setPixelMap(FileUtils.getPixMap(absolutePath));
                }
            });
        }
    };

    private void parseData() {
        try {
            absolutePath = myFile.getCanonicalPath();
        } catch (IOException e) {
            e.printStackTrace();
        }
        ohos.media.image.Image myImage = imageReceiver.readNextImage();
        ohos.media.image.Image.Component component = myImage.getComponent(ImageFormat.ComponentType.JPEG);
        byte[] bytes = new byte[component.remaining()];
        component.read(bytes);
        FileOutputStream output = null;
        try {
            output = new FileOutputStream(myFile);
            // 写图像数据
            output.write(bytes);
            output.flush();
        } catch (IOException e) {
            LogUtil.loge(TAG, "save picture occur exception!" + e.getLocalizedMessage());
        } finally {
            release(myImage, output);
        }
    }

    private void release(ohos.media.image.Image myImage, FileOutputStream output) {
        myImage.release();
        if (output != null) {
            try {
                output.close();
            } catch (IOException e) {
                LogUtil.loge(TAG, "image release occur exception!");
            }
        }
    }

    private void save(ImageReceiver imageReceiver, File myFile, String absolutePath) {
        ImageSaver imageSaver = new ImageSaver(imageReceiver.readNextImage(), myFile);
        cameraEventHandler.postTask(imageSaver);
    }

    /**
     * onStart
     *
     * @param intent Intent
     */
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        getWindow().setTransparent(true);
        getWindow().setBackgroundColor(new RgbColor(0xffffffff));
        setUIContent(ResourceTable.Layout_ability_camera);
        options = intent.getSerializableParam("options");
        mainHandler = new EventHandler(EventRunner.getMainEventRunner());
        if (options == null) {
            throw new SecurityException("options must not be null!");
        }
        File dir = getExternalFilesDir(options.getPath());
        if (!dir.exists()) {
            if (!dir.mkdir()) {
                terminateAbility();
            }
        }
        picturePath = dir + ImagePickerUtils.getText(ResourceTable.String_xie_gang);
        initView();
    }

    private void initView() {
        setDisplayOrientation(options.getScreenOrientation());
        if (findComponentById(ResourceTable.Id_camera_view) instanceof CameraView) {
            cameraView = (CameraView) findComponentById(ResourceTable.Id_camera_view);
        }
        cameraView.setFrontCamera(options.isFrontfacing());
        cameraView.setImageArrivalListener(imageArrivalListener);
        if (findComponentById(ResourceTable.Id_take_pic_bg) instanceof Image) {
            takePicBg = (Image) findComponentById(ResourceTable.Id_take_pic_bg);
        }
        if (findComponentById(ResourceTable.Id_take_pic) instanceof Image) {
            takePic = (Image) findComponentById(ResourceTable.Id_take_pic);
        }
        takePic.setClickedListener(component -> {
            if (options.getMode() == Options.Mode.VIDEO) {
                return;
            }
            cameraView.capture();
        });
        takePic.setTouchEventListener(new Component.TouchEventListener() {
            @Override
            public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
                if (touchEvent.getAction() == TouchEvent.PRIMARY_POINT_DOWN) {
                    takePicBg.setVisibility(Component.VISIBLE);
                }
                if (touchEvent.getAction() == TouchEvent.PRIMARY_POINT_UP) {
                    takePicBg.setVisibility(Component.HIDE);
                }
                return true;
            }
        });
        if (findComponentById(ResourceTable.Id_bottomButtons) instanceof StackLayout) {
            bottomButtons = (StackLayout) findComponentById(ResourceTable.Id_bottomButtons);
        }
        if (findComponentById(ResourceTable.Id_flash) instanceof StackLayout) {
            flash = (StackLayout) findComponentById(ResourceTable.Id_flash);
        }
        if (findComponentById(ResourceTable.Id_front) instanceof StackLayout) {
            shiftCamera = (StackLayout) findComponentById(ResourceTable.Id_front);
        }
        selectImage = (DependentLayout) findComponentById(ResourceTable.Id_select_image);
        previewPhoto = (Image) findComponentById(ResourceTable.Id_image_view);
        back = (Image) findComponentById(ResourceTable.Id_back);
        confirm = (Image) findComponentById(ResourceTable.Id_confirm);
        flash.setClickedListener(this);
        shiftCamera.setClickedListener(this);
        back.setClickedListener(this);
        confirm.setClickedListener(this);
        cameraEventHandler = new EventHandler(EventRunner.create("TakePictureAbilitySlice"));
    }

    /**
     * onClick
     *
     * @param component Component
     */
    @Override
    public void onClick(Component component) {
        int id = component.getId();
        if (id == ResourceTable.Id_flash) {
            switchFlash();
        }
        if (id == ResourceTable.Id_front) {
            AnimatorProperty animatorProperty = shiftCamera.createAnimatorProperty();
            animatorProperty.scaleXFrom(1).scaleX(0).setDuration(150)
                    .setStateChangedListener(new Animator.StateChangedListener() {
                        @Override
                        public void onStart(Animator animator) {
                        }

                        @Override
                        public void onStop(Animator animator) {
                        }

                        @Override
                        public void onCancel(Animator animator) {
                        }

                        @Override
                        public void onEnd(Animator animator) {
                            animatorProperty.scaleXFrom(0).scaleX(1).setDuration(150)
                                    .setStateChangedListener(null).start();
                        }

                        @Override
                        public void onPause(Animator animator) {
                        }

                        @Override
                        public void onResume(Animator animator) {
                        }
                    }).start();
            cameraView.switchCamera();
        }
        if (id == ResourceTable.Id_back) {
            bottomButtons.setVisibility(Component.VISIBLE);
            cameraView.setVisibility(Component.VISIBLE);
            selectImage.setVisibility(Component.HIDE);
            if (myFile != null) {
                boolean delete = myFile.delete();
                LogUtil.loge(String.valueOf(delete));
            }
        }
        if (id == ResourceTable.Id_confirm) {
            save(imageReceiver, myFile, absolutePath);
        }
    }

    private void switchFlash() {
        int height = flash.getHeight();
        if (flash.getComponentAt(0) instanceof Image) {
            Image iv = (Image) flash.getComponentAt(0);
            flash.createAnimatorProperty().moveToY(height).setDuration(100)
                    .setStateChangedListener(new Animator.StateChangedListener() {
                        @Override
                        public void onStart(Animator animator) {
                        }

                        @Override
                        public void onStop(Animator animator) {
                        }

                        @Override
                        public void onCancel(Animator animator) {
                        }

                        @Override
                        public void onEnd(Animator animator) {
                            animationOnEnd(iv, height);
                        }

                        @Override
                        public void onPause(Animator animator) {
                        }

                        @Override
                        public void onResume(Animator animator) {
                        }
                    }).start();
        }
    }

    private void animationOnEnd(Image iv, int height) {
        iv.setTranslationY(-(height / 2));
        if (flashMode == Metadata.FlashMode.FLASH_AUTO) {
            flashMode = Metadata.FlashMode.FLASH_CLOSE;
            try {
                PixelMapElement flashDrawable =
                        new PixelMapElement(getResourceManager()
                                .getResource(ResourceTable.Media_flash_off));
                iv.setBackground(flashDrawable);
            } catch (IOException | NotExistException e) {
                e.printStackTrace();
            }
        } else if (flashMode == Metadata.FlashMode.FLASH_CLOSE) {
            flashMode = Metadata.FlashMode.FLASH_ALWAYS_OPEN;
            try {
                PixelMapElement flashDrawable =
                        new PixelMapElement(getResourceManager()
                                .getResource(ResourceTable.Media_flash_on));
                iv.setBackground(flashDrawable);
            } catch (IOException | NotExistException e) {
                e.printStackTrace();
            }
        } else {
            flashMode = Metadata.FlashMode.FLASH_AUTO;
            try {
                PixelMapElement flashDrawable =
                        new PixelMapElement(getResourceManager()
                                .getResource(ResourceTable.Media_flash_auto));
                iv.setBackground(flashDrawable);
            } catch (IOException | NotExistException e) {
                e.printStackTrace();
            }
        }
        cameraView.setCameraFlashMode(flashMode);
        iv.createAnimatorProperty().moveToY(0).setDuration(50)
                .setStateChangedListener(null).start();
    }

    private void returnImages(Uri uri) {
        com.esafirm.imagepicker.model.Image img = new com.esafirm.imagepicker.model.Image();
        img.setUriSchema(uri.toString());
        img.setPath(absolutePath);
        img.setMediaType("image/JPEG");
        Intent resultIntent = new Intent();
        resultIntent.setParam(CameraPhotoAbility.IMAGE_RESULTS, img);
        setResult(options.getRequestCode(), resultIntent);
        terminateAbility();
    }

    /**
     * 开启Pix
     *
     * @param abilitySlice abilitySlice
     * @param options      options
     */
    public static void start(AbilitySlice abilitySlice, Options options) {
        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName(abilitySlice.getBundleName())
                .withAbilityName(CameraPhotoAbility.class.getName())
                .build();
        intent.setOperation(operation);
        intent.setParam("options", options);
        abilitySlice.startAbilityForResult(intent, options.getRequestCode());
    }

    /**
     * 开启Pix
     *
     * @param ability ability
     * @param options options
     */
    public static void start(Ability ability, Options options) {
        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName(ability.getBundleName())
                .withAbilityName(CameraPhotoAbility.class.getName())
                .build();
        intent.setOperation(operation);
        intent.setParam("options", options);
        ability.startAbilityForResult(intent, options.getRequestCode());
    }

    /**
     * 开启Pix
     *
     * @param ability     ability
     * @param requestCode requestCode
     */
    public static void start(Ability ability, int requestCode) {
        start(ability, Options.init().setRequestCode(requestCode).setCount(1));
    }

    /**
     * onStop
     */
    @Override
    protected void onStop() {
        super.onStop();
        if (mRevocable != null) {
            mRevocable.revoke();
        }
        if (mParallelTaskDispatcher != null) {
            mParallelTaskDispatcher = null;
        }
    }

    private class ImageSaver implements Runnable {
        private final ohos.media.image.Image myImage;
        private final File myFile;

        /**
         * 构造函数
         *
         * @param image ohos.media.image.Image image
         * @param file  File
         */
        ImageSaver(ohos.media.image.Image image, File file) {
            myImage = image;
            myFile = file;
        }

        /**
         * run
         */
        @Override
        public void run() {
            try {
                Uri uri = FileUtils.saveImageToGallery(pictureName,
                        FileUtils.getPixMap(myFile.getCanonicalPath()), CameraPhotoAbility.this);
                returnImages(uri);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
