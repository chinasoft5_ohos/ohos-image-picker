package com.esafirm.imagepicker.features.common;

import com.esafirm.imagepicker.features.ImagePickerSavePath;
import com.esafirm.imagepicker.features.ReturnMode;

import ohos.agp.utils.TextTool;

import ohos.utils.Parcel;
import ohos.utils.Sequenceable;
import ohos.utils.zson.ZSONObject;

/**
 * BaseConfig
 */
public class BaseConfig implements Sequenceable {
    /**
     * PRODUCER
     */
    public static final Producer PRODUCER = new Producer() {
        /**
         * createFromParcel
         *
         * @param in Parcel
         * @return Image
         */
        public BaseConfig createFromParcel(Parcel in) {
            // Initialize an instance first, then do customized unmarshlling.
            BaseConfig instance = new BaseConfig();
            instance.unmarshalling(in);
            return instance;
        }
    };

    private ImagePickerSavePath savePathObject;
    private String savePath;
    private ReturnMode returnMode;

    /**
     * getReturnMode
     *
     * @return ReturnMode
     */
    public ReturnMode getReturnMode() {
        return returnMode;
    }

    /**
     * getImageDirectory
     *
     * @return ImagePickerSavePath
     */
    public ImagePickerSavePath getImageDirectory() {
        return savePathObject;
    }

    /**
     * setSavePath
     *
     * @param savePath String
     */
    public void setSavePath(String savePath) {
        this.savePath = savePath;
        if (!TextTool.isNullOrEmpty(savePath)) {
            this.savePathObject = ZSONObject.stringToClass(savePath, ImagePickerSavePath.class);
        }
    }

    /**
     * setImageDirectory
     *
     * @param dirName String
     */
    public void setImageDirectory(String dirName) {
        savePathObject = new ImagePickerSavePath(dirName, false);
    }

    /**
     * setImageFullDirectory
     *
     * @param path String
     */
    public void setImageFullDirectory(String path) {
        savePathObject = new ImagePickerSavePath(path, true);
    }

    /**
     * setReturnMode
     *
     * @param returnMode ReturnMode
     */
    public void setReturnMode(ReturnMode returnMode) {
        this.returnMode = returnMode;
    }

    @Override
    public boolean hasFileDescriptor() {
        return true;
    }

    @Override
    public boolean marshalling(Parcel dest) {
        return dest.writeString(this.savePath) && dest.writeInt(this.returnMode == null ? -1 : this.returnMode.ordinal());
    }

    @Override
    public boolean unmarshalling(Parcel in) {
        this.savePath = in.readString();
        int tmpReturnMode = in.readInt();
        this.returnMode = tmpReturnMode == -1 ? null : ReturnMode.values()[tmpReturnMode];
        return true;
    }
}
