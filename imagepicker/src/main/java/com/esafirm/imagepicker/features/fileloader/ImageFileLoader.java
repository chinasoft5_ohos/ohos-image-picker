package com.esafirm.imagepicker.features.fileloader;

import com.esafirm.imagepicker.features.common.ImageLoaderListener;

import java.io.File;
import java.util.ArrayList;

public interface ImageFileLoader {

    /**
     * loadDeviceImages
     *
     * @param isFolderMode     boolean
     * @param onlyVideo        boolean
     * @param includeVideo     boolean
     * @param includeAnimation boolean
     * @param excludedImages   ArrayList<File>
     * @param listener         ImageLoaderListener
     */
    void loadDeviceImages(
            boolean isFolderMode,
            boolean onlyVideo,
            boolean includeVideo,
            boolean includeAnimation,
            ArrayList<File> excludedImages,
            ImageLoaderListener listener
    );

    /**
     * abortLoadImages
     */
    void abortLoadImages();
}
