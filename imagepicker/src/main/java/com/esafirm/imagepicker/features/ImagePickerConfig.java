package com.esafirm.imagepicker.features;

import com.esafirm.imagepicker.features.common.BaseConfig;
import com.esafirm.imagepicker.model.Image;

import ohos.agp.utils.TextTool;

import ohos.utils.Parcel;
import ohos.utils.Sequenceable;
import ohos.utils.zson.ZSONArray;

import java.io.File;
import java.util.ArrayList;

/**
 * ImagePickerConfig
 */
public class ImagePickerConfig extends BaseConfig implements Sequenceable {
    /**
     * NO_COLOR
     */
    public static final int NO_COLOR = -1;
    /**
     * Sequenceable
     */
    public static final Sequenceable.Producer PRODUCER = new Sequenceable.Producer() {
        /**
         * createFromParcel
         *
         * @param in Parcel
         * @return Image
         */
        public ImagePickerConfig createFromParcel(Parcel in) {
            // Initialize an instance first, then do customized unmarshlling.
            ImagePickerConfig instance = new ImagePickerConfig();
            instance.unmarshalling(in);
            return instance;
        }
    };

    private String selectedImages;
    private String excludedImages;
    private String folderTitle;
    private String imageTitle;
    private String doneButtonText;
    private int arrowColor = NO_COLOR;
    private int mode;
    private int limit;
    private int theme;
    private boolean folderMode;
    private boolean includeVideo;
    private boolean onlyVideo;
    private boolean includeAnimation;
    private boolean showCamera;
    private transient String language;
    private String exclude;
    private ArrayList<File> list;

    /**
     * getArrowColor
     *
     * @return int
     */
    public int getArrowColor() {
        return arrowColor;
    }

    /**
     * setArrowColor
     *
     * @param arrowColor int
     */
    public void setArrowColor(int arrowColor) {
        this.arrowColor = arrowColor;
    }

    /**
     * getMode
     *
     * @return int
     */
    public int getMode() {
        return mode;
    }

    /**
     * setMode
     *
     * @param mode int
     */
    public void setMode(int mode) {
        this.mode = mode;
    }

    /**
     * getLimit
     *
     * @return int
     */
    public int getLimit() {
        return limit;
    }

    /**
     * setLimit
     *
     * @param limit int
     */
    public void setLimit(int limit) {
        this.limit = limit;
    }

    /**
     * isShowCamera
     *
     * @return boolean
     */
    public boolean isShowCamera() {
        return showCamera;
    }

    /**
     * setShowCamera
     *
     * @param showCamera boolean
     */
    public void setShowCamera(boolean showCamera) {
        this.showCamera = showCamera;
    }

    /**
     * isIncludeVideo
     *
     * @return boolean
     */
    public boolean isIncludeVideo() {
        return includeVideo;
    }

    /**
     * setIncludeVideo
     *
     * @param includeVideo boolean
     */
    public void setIncludeVideo(boolean includeVideo) {
        this.includeVideo = includeVideo;
    }

    /**
     * isOnlyVideo
     *
     * @return boolean
     */
    public boolean isOnlyVideo() {
        return onlyVideo;
    }

    /**
     * setOnlyVideo
     *
     * @param onlyVideo boolean
     */
    public void setOnlyVideo(boolean onlyVideo) {
        this.onlyVideo = onlyVideo;
    }

    /**
     * isIncludeAnimation
     *
     * @return boolean
     */
    public boolean isIncludeAnimation() {
        return includeAnimation;
    }

    /**
     * setIncludeAnimation
     *
     * @param includeAnimation boolean
     */
    public void setIncludeAnimation(boolean includeAnimation) {
        this.includeAnimation = includeAnimation;
    }

    /**
     * getFolderTitle
     *
     * @return String
     */
    public String getFolderTitle() {
        return folderTitle;
    }

    /**
     * setFolderTitle
     *
     * @param folderTitle String
     */
    public void setFolderTitle(String folderTitle) {
        this.folderTitle = folderTitle;
    }

    /**
     * getImageTitle
     *
     * @return String
     */
    public String getImageTitle() {
        return imageTitle;
    }

    /**
     * setImageTitle
     *
     * @param imageTitle String
     */
    public void setImageTitle(String imageTitle) {
        this.imageTitle = imageTitle;
    }

    /**
     * getDoneButtonText
     *
     * @return String
     */
    public String getDoneButtonText() {
        return doneButtonText;
    }

    /**
     * setDoneButtonText
     *
     * @param doneButtonText String
     */
    public void setDoneButtonText(String doneButtonText) {
        this.doneButtonText = doneButtonText;
    }

    /**
     * getSelectedImages
     *
     * @return ArrayList<Image>
     */
    public ArrayList<Image> getSelectedImages() {
        ArrayList<Image> files = new ArrayList<>();
        if (!TextTool.isNullOrEmpty(selectedImages)) {
            files = (ArrayList<Image>) ZSONArray.stringToClassList(selectedImages, Image.class);
        }
        return files;
    }

    /**
     * setSelectedImages
     *
     * @param selectedImages String
     */
    public void setSelectedImages(String selectedImages) {
        this.selectedImages = selectedImages;
    }

    /**
     * getExcludedImages
     *
     * @return ArrayList<File>
     */
    public ArrayList<File> getExcludedImages() {
        return list;
    }

    /**
     * setExcludedImages
     *
     * @param excludedImages String
     */
    public void setExcludedImages(String excludedImages) {
        list = new ArrayList<>();
        this.excludedImages = excludedImages;
        if (!TextTool.isNullOrEmpty(excludedImages)) {
            ArrayList<Image> listImage = (ArrayList<Image>) ZSONArray.stringToClassList(excludedImages, Image.class);
            for (Image image : listImage) {
                list.add(new File(image.getPath()));
            }
        } else {
            list = null;
        }
    }

    /**
     * setExcludedImageFiles
     *
     * @param excludedImagesFiles String
     */
    public void setExcludedImageFiles(String excludedImagesFiles) {
        if (!TextTool.isNullOrEmpty(excludedImagesFiles)) {
            list = (ArrayList<File>) ZSONArray.stringToClassList(excludedImagesFiles, File.class);
        }
    }

    /**
     * isFolderMode
     *
     * @return boolean
     */
    public boolean isFolderMode() {
        return folderMode;
    }

    /**
     * setFolderMode
     *
     * @param folderMode boolean
     */
    public void setFolderMode(boolean folderMode) {
        this.folderMode = folderMode;
    }

    /**
     * setTheme
     *
     * @param theme int
     */
    public void setTheme(int theme) {
        this.theme = theme;
    }

    /**
     * getTheme
     *
     * @return int
     */
    public int getTheme() {
        return theme;
    }

    /**
     * setLanguage
     *
     * @param language String
     */
    public void setLanguage(String language) {
        this.language = language;
    }

    /**
     * getLanguage
     *
     * @return String
     */
    public String getLanguage() {
        return language;
    }

    /**
     * marshalling
     *
     * @param dest Parcel
     * @return boolean
     */
    public boolean marshalling(Parcel dest) {
        super.marshalling(dest);
        return dest.writeString(this.selectedImages)
                && dest.writeString(this.excludedImages)
                && dest.writeString(this.folderTitle)
                && dest.writeString(this.imageTitle)
                && dest.writeString(this.doneButtonText)
                && dest.writeInt(this.arrowColor)
                && dest.writeInt(this.mode)
                && dest.writeInt(this.limit)
                && dest.writeInt(this.theme)
                && dest.writeBoolean(this.folderMode)
                && dest.writeBoolean(this.includeVideo)
                && dest.writeBoolean(this.onlyVideo)
                && dest.writeBoolean(this.includeAnimation)
                && dest.writeBoolean(this.showCamera);
    }

    /**
     * unmarshalling
     *
     * @param in Parcel
     * @return boolean
     */
    public boolean unmarshalling(Parcel in) {
        super.unmarshalling(in);
        this.selectedImages = in.readString();
        this.excludedImages = in.readString();
        this.folderTitle = in.readString();
        this.imageTitle = in.readString();
        this.doneButtonText = in.readString();
        this.arrowColor = in.readInt();
        this.mode = in.readInt();
        this.limit = in.readInt();
        this.theme = in.readInt();
        this.folderMode = in.readBoolean();
        this.includeVideo = in.readBoolean();
        this.onlyVideo = in.readBoolean();
        this.includeAnimation = in.readBoolean();
        this.showCamera = in.readBoolean();
        return true;
    }
}
