package com.esafirm.imagepicker.features.imageloader;

import com.esafirm.imagepicker.model.Image;

/**
 * ImageLoader
 */
public interface ImageLoader {
    /**
     * loadImage
     *
     * @param image     Image
     * @param imageView imageView
     * @param imageType ImageType
     */
    void loadImage(Image image, ohos.agp.components.Image imageView, ImageType imageType);
}
