package com.esafirm.imagepicker.features.cameraonly;

import com.esafirm.imagepicker.features.common.BaseConfig;

import ohos.utils.Parcel;

/**
 * CameraOnlyConfig
 */
public class CameraOnlyConfig extends BaseConfig {
    /**
     * PRODUCER
     */
    public static final Producer PRODUCER = new Producer() {
        /**
         * createFromParcel
         *
         * @param in Parcel
         * @return Image
         */
        public BaseConfig createFromParcel(Parcel in) {
            // Initialize an instance first, then do customized unmarshlling.
            CameraOnlyConfig instance = new CameraOnlyConfig();
            instance.unmarshalling(in);
            return instance;
        }
    };

    @Override
    public boolean hasFileDescriptor() {
        return super.hasFileDescriptor();
    }

    @Override
    public boolean marshalling(Parcel dest) {
        return super.marshalling(dest);
    }

    @Override
    public boolean unmarshalling(Parcel in) {
        return super.unmarshalling(in);
    }
}

