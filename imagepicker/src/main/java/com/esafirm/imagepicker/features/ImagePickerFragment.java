package com.esafirm.imagepicker.features;

import com.esafirm.imagepicker.ResourceTable;
import com.esafirm.imagepicker.features.camera.CameraHelper;
import com.esafirm.imagepicker.features.camera.CameraPhotoAbility;
import com.esafirm.imagepicker.features.cameraonly.CameraOnlyConfig;
import com.esafirm.imagepicker.features.common.BaseConfig;
import com.esafirm.imagepicker.features.fileloader.DefaultImageFileLoader;
import com.esafirm.imagepicker.features.recyclers.RecyclerViewManager;
import com.esafirm.imagepicker.helper.*;
import com.esafirm.imagepicker.model.Folder;
import com.esafirm.imagepicker.model.Image;

import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.ability.IDataAbilityObserver;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;

import ohos.agp.components.*;
import ohos.agp.utils.TextTool;
import ohos.agp.window.dialog.ToastDialog;

import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;

import ohos.media.photokit.metadata.AVStorage;

import java.util.ArrayList;
import java.util.List;

import static com.esafirm.imagepicker.features.ImagePicker.RESULT_CANCELED;
import static com.esafirm.imagepicker.helper.ImagePickerPreferences.PREF_WRITE_EXTERNAL_STORAGE_REQUESTED;

import static ohos.bundle.IBundleManager.PERMISSION_GRANTED;

/**
 * ImagePickerFragment
 */
public class ImagePickerFragment extends Fraction implements ImagePickerView {
    private static final String STATE_KEY_CAMERA_MODULE = "Key.CameraModule";
    private static final String STATE_KEY_RECYCLER = "Key.Recycler";
    private static final String STATE_KEY_SELECTED_IMAGES = "Key.SelectedImages";
    private static final int RC_CAPTURE = 2000;
    private static final int RC_PERMISSION_REQUEST_WRITE_EXTERNAL_STORAGE = 23;
    private static final int RC_PERMISSION_REQUEST_CAMERA = 24;
    private ListContainer recyclerView;
    private ProgressBar progressBar;
    private Text emptyTextView;
    private RecyclerViewManager recyclerViewManager;
    private ImagePickerPresenter presenter;
    private ImagePickerPreferences preferences;
    private ImagePickerConfig config;
    private ImagePickerInteractionListener interactionListener;
    private EventHandler handler;
    private DataAbilityHelper dataAbilityHelper;
    private CameraOnlyConfig cameraOnlyConfig;
    private boolean isCameraOnly;
    private IDataAbilityObserver observer;
    private boolean loading;
    private ArrayList<Image> list;
    private int count;

    /**
     * 构造函数
     *
     * @param config             ImagePickerConfig
     * @param cameraOnlyConfig   CameraOnlyConfig
     * @param imagePickerAbility ImagePickerInteractionListener
     */
    public ImagePickerFragment(ImagePickerConfig config, CameraOnlyConfig cameraOnlyConfig, ImagePickerInteractionListener imagePickerAbility) {
        this.config = config;
        this.cameraOnlyConfig = cameraOnlyConfig;
        this.interactionListener = imagePickerAbility;
        isCameraOnly = cameraOnlyConfig != null;
    }

    /**
     * 构造函数
     *
     * @param config           ImagePickerConfig
     * @param cameraOnlyConfig CameraOnlyConfig
     */
    public ImagePickerFragment(ImagePickerConfig config, CameraOnlyConfig cameraOnlyConfig) {
        this.config = config;
        this.cameraOnlyConfig = cameraOnlyConfig;
        isCameraOnly = cameraOnlyConfig != null;
    }

    @Override
    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        setupComponents();
        if (interactionListener == null) {
            throw new NullPointerException("ImagePickerFragment needs an " +
                    "ImagePickerInteractionListener. This will be set automatically if the " +
                    "ability implements ImagePickerInteractionListener, and can be set manually " +
                    "with fragment.setInteractionListener(listener).");
        }
        if (isCameraOnly) {
            captureImageWithPermission();
        } else {
            ImagePickerConfig imagePickerConfig = getImagePickerConfig();
            LayoutScatter localInflater = LayoutScatter.getInstance(container.getContext());
            Component result = localInflater.parse(ResourceTable.Layout_ef_fragment_image_picker, container, false);
            setupView(result);
            setupRecyclerView(imagePickerConfig, imagePickerConfig.getSelectedImages());
            interactionListener.selectionChanged(recyclerViewManager.getSelectedImages());
            return result;
        }
        return null;
    }

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        dataAbilityHelper = DataAbilityHelper.creator(getFractionAbility(), AVStorage.Images.Media.EXTERNAL_DATA_ABILITY_URI, true);
        startContentObserver();
        if (!isCameraOnly) {
            getDataWithPermission();
        }
    }

    private BaseConfig getBaseConfig() {
        return isCameraOnly
                ? getCameraOnlyConfig()
                : getImagePickerConfig();
    }


    private ImagePickerConfig getImagePickerConfig() {
        if (config == null) {
            if (config == null && cameraOnlyConfig == null) {
//                IpCrasher.openIssue();
            }
            boolean hasImagePickerConfig = config != null;
            boolean hasCameraOnlyConfig = config != null;
            if (!hasCameraOnlyConfig && !hasImagePickerConfig) {
//                IpCrasher.openIssue();
            }
        }
        return config;
    }

    private CameraOnlyConfig getCameraOnlyConfig() {
        return cameraOnlyConfig;
    }

    private void setupView(Component rootView) {
        progressBar = (ProgressBar) rootView.findComponentById(ResourceTable.Id_progress_bar);
        emptyTextView = (Text) rootView.findComponentById(ResourceTable.Id_tv_empty_images);
        recyclerView = (ListContainer) rootView.findComponentById(ResourceTable.Id_recyclerView);
    }

    private void setupRecyclerView(ImagePickerConfig config, ArrayList<Image> selectedImages) {
        recyclerViewManager = new RecyclerViewManager(
                recyclerView,
                config,
                getFractionAbility().getAbilityInfo().getOrientation()
        );
        recyclerViewManager.setupAdapters(selectedImages, (isSelected) -> recyclerViewManager.selectImage(isSelected), bucket -> setImageAdapter(bucket.getImages()));
        recyclerViewManager.setImageSelectedListener(selectedImage -> {
            updateTitle();
            interactionListener.selectionChanged(recyclerViewManager.getSelectedImages());
            if (ConfigUtils.shouldReturn(config, false) && !selectedImage.isEmpty()) {
                onDone();
            }
        });
    }

    private void setupComponents() {
        preferences = new ImagePickerPreferences(getFractionAbility());
        presenter = new ImagePickerPresenter(new DefaultImageFileLoader(getFractionAbility()));
        presenter.attachView(this);
    }

    /**
     * Set image adapter
     * 1. Set new data
     * 2. Update item decoration
     * 3. Update title
     *
     * @param images List<Image>
     */
    void setImageAdapter(List<Image> images) {
        recyclerViewManager.setImageAdapter(images);
        updateTitle();
    }

    /**
     * setFolderAdapter
     *
     * @param folders List<Folder>
     */
    void setFolderAdapter(List<Folder> folders) {
        recyclerViewManager.setFolderAdapter(folders);
        updateTitle();
    }

    private void updateTitle() {
        interactionListener.setTitle(recyclerViewManager.getTitle());
    }

    /**
     * On finish selected image
     * Get all selected images then return image to caller ability
     */
    public void onDone() {
        presenter.onDoneSelectImages(recyclerViewManager.getSelectedImages());
    }

    /**
     * Check permission
     */
    private void getDataWithPermission() {
        if (getFractionAbility().verifySelfPermission("ohos.permission.READ_USER_STORAGE") != PERMISSION_GRANTED) {
            requestWriteExternalPermission();
        } else {
            getData();
        }
    }

    private void getData() {
        if (!loading) {
            presenter.abortLoad();
            ImagePickerConfig imagePickerConfig = getImagePickerConfig();
            if (imagePickerConfig != null) {
                isLoading(true);
                presenter.loadImages(imagePickerConfig);
            }
        }
    }

    /**
     * Request for permission
     * If permission denied or app is first launched, request for permission
     * If permission denied and user choose 'Never Ask Again', show snackbar with an action that navigate to app settings
     */
    private void requestWriteExternalPermission() {
        final String[] permissions = new String[]{"ohos.permission.READ_USER_STORAGE"};
        if (getFractionAbility().canRequestPermission("ohos.permission.READ_USER_STORAGE")) {
            getFractionAbility().requestPermissionsFromUser(permissions, RC_PERMISSION_REQUEST_WRITE_EXTERNAL_STORAGE);
        } else {
            final String permission = PREF_WRITE_EXTERNAL_STORAGE_REQUESTED;
            if (!preferences.isPermissionRequested(permission)) {
                getFractionAbility().requestPermissionsFromUser(permissions, RC_PERMISSION_REQUEST_WRITE_EXTERNAL_STORAGE);
            } else {
                ToastUtil.show(getFractionAbility(), ResourceTable.String_ef_msg_no_camera_permission);
                openAppSettings();
            }
        }
    }

    private void requestCameraPermissions() {
        ArrayList<String> permissions = new ArrayList();
        if (getFractionAbility().verifySelfPermission("ohos.permission.CAMERA") != PERMISSION_GRANTED) {
            permissions.add("ohos.permission.CAMERA");
        }
        if (getFractionAbility().verifySelfPermission("ohos.permission.READ_USER_STORAGE") != PERMISSION_GRANTED) {
            permissions.add("ohos.permission.READ_USER_STORAGE");
        }
        if (checkForRationale(permissions)) {
            getFractionAbility().requestPermissionsFromUser(permissions.toArray(new String[permissions.size()]), RC_PERMISSION_REQUEST_CAMERA);
        } else {
            final String permission = ImagePickerPreferences.PREF_CAMERA_REQUESTED;
            if (!preferences.isPermissionRequested(permission)) {
                getFractionAbility().requestPermissionsFromUser(permissions.toArray(new String[permissions.size()]), RC_PERMISSION_REQUEST_CAMERA);
            } else {
                if (isCameraOnly) {
                    new ToastDialog(getFractionAbility()).setText(ResourceUtil.getText(getFractionAbility(), ResourceTable.String_ef_msg_no_camera_permission)).show();
                    interactionListener.cancel();
                } else {
                    ToastUtil.show(getFractionAbility(), ResourceTable.String_ef_msg_no_camera_permission);
                    openAppSettings();
                }
            }
        }
    }

    private String[] getPermisionArray(ArrayList<String> permissions) {
        String[] permissionsList = new String[count];
        for (int i = 0; i < permissionsList.length; i++) {
            for (int j = 0; j < permissions.size(); j++) {
                if (canRequestPermission(permissions.get(j))) {
                    permissionsList[i] = permissions.get(j);
                }
            }
        }
        return permissionsList;
    }

    private boolean checkForRationale(ArrayList<String> permissions) {
        count = 0;
        for (int i = 0, size = permissions.size(); i < size; i++) {
            if (canRequestPermission(permissions.get(i))) {
                count++;
                return true;
            }
        }
        return false;
    }

    /**
     * Handle permission results
     *
     * @param requestCode  int
     * @param permissions  String[]
     * @param grantResults int[]
     */
    public void onRequestPermissionsFromUserResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case RC_PERMISSION_REQUEST_WRITE_EXTERNAL_STORAGE: {
                if (grantResults.length > 0 && grantResults[0] == PERMISSION_GRANTED) {
                    final String permission = ImagePickerPreferences.PREF_WRITE_EXTERNAL_STORAGE_REQUESTED;
                    preferences.setPermissionRequested(permission);
                    getData();
                    return;
                }
                interactionListener.cancel();
            }
            break;
            case RC_PERMISSION_REQUEST_CAMERA: {
                if (grantResults.length != 0 && grantResults[0] == PERMISSION_GRANTED) {
                    final String permission = ImagePickerPreferences.PREF_CAMERA_REQUESTED;
                    preferences.setPermissionRequested(permission);
                    captureImage();
                    return;
                }
                interactionListener.cancel();
                break;
            }
            default: {
                getFractionAbility().onRequestPermissionsFromUserResult(requestCode, permissions, grantResults);
                break;
            }
        }

    }

    /**
     * Open app settings screen
     */
    private void openAppSettings() {
        Intent intent = new Intent();
        intent.setAction("android.settings.SETTINGS");
        intent.addFlags(Intent.FLAG_NOT_OHOS_COMPONENT);
        getFractionAbility().startAbility(intent);
    }

    /**
     * Check if the captured image is stored successfully
     * Then reload data
     *
     * @param requestCode int
     * @param resultCode  int
     * @param resultData  Intent
     * @param startType   String
     */
    public void onAbilityResult(int requestCode, int resultCode, Intent resultData, String startType) {
        if (requestCode == RC_CAPTURE) {
            if (resultCode == ImagePicker.RESULT_OK) {
                presenter.finishCaptureImage(getFractionAbility(), resultData, getBaseConfig());
            } else if (resultCode == RESULT_CANCELED && isCameraOnly) {
                presenter.abortCaptureImage();
                interactionListener.cancel();
            } else if (requestCode == RC_CAPTURE) {
                getCapture(resultData, startType);
            } else {
            }
        }
    }

    private void getCapture(Intent resultData, String startType) {
        if (TextTool.isNullOrEmpty(startType)) {
            getData();
        } else if ("custom".equals(startType)) {
            getData();
        } else if ("camera".equals(startType)) {
            Image img = resultData.getSequenceableParam(CameraPhotoAbility.IMAGE_RESULTS);
            Intent intent = new Intent();
            if (list == null) {
                list = new ArrayList<>();
            }
            list.add(img);
            intent.setSequenceableArrayListParam(IpCons.EXTRA_SELECTED_IMAGES, list);
            getFractionAbility().setResult(IpCons.RC_IMAGE_CAMERA, intent);
            getFractionAbility().terminateAbility();
        } else {
        }
    }

    /**
     * Request for camera permission
     */
    public void captureImageWithPermission() {
        final boolean isCameraGranted = getFractionAbility().verifySelfPermission("ohos.permission.CAMERA") == PERMISSION_GRANTED;
        final boolean isWriteGranted = getFractionAbility().verifySelfPermission("ohos.permission.READ_USER_STORAGE") == PERMISSION_GRANTED;
        if (isCameraGranted && isWriteGranted) {
            captureImage();
        } else {
            requestCameraPermissions();
        }
    }

    /**
     * Start camera intent
     * Create a temporary file and pass file Uri to camera intent
     */
    private void captureImage() {
        if (!CameraHelper.checkCameraAvailability(getFractionAbility())) {
            return;
        }
        presenter.captureImage(this, getBaseConfig(), RC_CAPTURE);
    }

    private void startContentObserver() {
        if (isCameraOnly) {
            return;
        }
        if (handler == null) {
            handler = new EventHandler(EventRunner.getMainEventRunner());
        }
        observer = new IDataAbilityObserver() {
            @Override
            public void onChange() {
                onChangeData();
            }
        };
        dataAbilityHelper.registerObserver(AVStorage.Images.Media.EXTERNAL_DATA_ABILITY_URI, observer);
    }

    private void onChangeData() {
        handler.postTask(new Runnable() {
            @Override
            public void run() {
                LogUtil.loge("HHAHHHA", "获取数据");
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (presenter != null) {
            presenter.abortLoad();
            presenter.detachView();
        }
        if (observer != null) {
            dataAbilityHelper.unregisterObserver(AVStorage.Images.Media.EXTERNAL_DATA_ABILITY_URI, observer);
            observer = null;
        }
        if (handler != null) {
            handler.removeAllEvent();
            handler = null;
        }
    }

    /**
     * Returns true if this Fragment can do anything with a "back" event, such as the containing
     * Ability receiving onBackPressed(). Returns false if the containing Ability should handle
     * it.
     * This Fragment might handle a "back" event by, for example, going back to the list of folders.
     * Or it might have no "back" to go, and return false.
     *
     * @return boolean
     */
    public boolean handleBack() {
        if (isCameraOnly) {
            return false;
        }
        if (recyclerViewManager.handleBack()) {
            updateTitle();
            return true;
        }
        return false;
    }

    /**
     * isShowDoneButton
     *
     * @return boolean
     */
    public boolean isShowDoneButton() {
        return recyclerViewManager.isShowDoneButton();
    }

    /**
     * setInteractionListener
     *
     * @param listener ImagePickerInteractionListener
     */
    public void setInteractionListener(ImagePickerInteractionListener listener) {
        interactionListener = listener;
    }

    @Override
    public void finishPickImages(List<Image> images) {
        Intent data = new Intent();
        ArrayList<Image> imageArrayList = new ArrayList<>(images);
        data.setSequenceableArrayListParam(IpCons.EXTRA_SELECTED_IMAGES, imageArrayList);
        interactionListener.finishPickImages(data);
    }

    @Override
    public void showCapturedImage() {
        getDataWithPermission();
    }

    @Override
    public void showFetchCompleted(List<Image> images, List<Folder> folders) {
        ImagePickerConfig imagePickerConfig = getImagePickerConfig();
        if (imagePickerConfig != null && imagePickerConfig.isFolderMode()) {
            setFolderAdapter(folders);
        } else {
            setImageAdapter(images);
        }
    }

    @Override
    public void showError(Throwable throwable) {
        String message = "Unknown Error";
        if (throwable instanceof NullPointerException) {
            message = "Images do not exist";
        }
        new ToastDialog(getFractionAbility()).setText(message).show();
    }

    @Override
    public void isLoading(boolean isloading) {
        loading = isloading;
    }

    @Override
    public void showLoading(boolean isLoading) {
        progressBar.setVisibility(isLoading ? Component.VISIBLE : Component.HIDE);
        recyclerView.setVisibility(isLoading ? Component.HIDE : Component.VISIBLE);
        emptyTextView.setVisibility(Component.HIDE);
    }

    @Override
    public void showEmpty() {
        progressBar.setVisibility(Component.HIDE);
        recyclerView.setVisibility(Component.HIDE);
        emptyTextView.setVisibility(Component.VISIBLE);
    }
}