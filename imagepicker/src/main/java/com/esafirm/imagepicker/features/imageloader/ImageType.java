package com.esafirm.imagepicker.features.imageloader;

/**
 * ImageType
 */
public enum ImageType {
    FOLDER,
    GALLERY
}
