package com.esafirm.imagepicker.features.recyclers;

import com.esafirm.imagepicker.ResourceTable;
import com.esafirm.imagepicker.adapter.FolderPickerAdapter;
import com.esafirm.imagepicker.adapter.ImagePickerAdapter;
import com.esafirm.imagepicker.features.ImagePickerComponentHolder;
import com.esafirm.imagepicker.features.ImagePickerConfig;
import com.esafirm.imagepicker.features.IpCons;
import com.esafirm.imagepicker.features.ReturnMode;
import com.esafirm.imagepicker.features.imageloader.ImageLoader;
import com.esafirm.imagepicker.helper.ConfigUtils;
import com.esafirm.imagepicker.helper.ImagePickerUtils;
import com.esafirm.imagepicker.helper.ResourceUtil;
import com.esafirm.imagepicker.listeners.OnFolderClickListener;
import com.esafirm.imagepicker.listeners.OnImageClickListener;
import com.esafirm.imagepicker.listeners.OnImageSelectedListener;
import com.esafirm.imagepicker.model.Folder;
import com.esafirm.imagepicker.model.Image;

import ohos.agp.components.ListContainer;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;

import ohos.bundle.AbilityInfo;

import java.util.ArrayList;
import java.util.List;

import static com.esafirm.imagepicker.features.IpCons.MAX_LIMIT;
import static com.esafirm.imagepicker.features.IpCons.MODE_SINGLE;

/**
 * RecyclerViewManager
 */
public class RecyclerViewManager {
    private final Context context;
    private final ListContainer recyclerView;
    private final ImagePickerConfig config;
    private int imageColumns;
    private int folderColumns;
    private int columns;
    private FolderPickerAdapter folderAdapter;
    private ImagePickerAdapter imageAdapter;
    private List<Folder> list = new ArrayList<>();

    /**
     * 构建
     *
     * @param recyclerView ListContainer
     * @param config       ImagePickerConfig
     * @param orientation  AbilityInfo.DisplayOrientation
     */
    public RecyclerViewManager(ListContainer recyclerView, ImagePickerConfig config, AbilityInfo.DisplayOrientation orientation) {
        this.recyclerView = recyclerView;
        this.config = config;
        this.context = recyclerView.getContext();
        changeOrientation(orientation);
    }

    /**
     * Set item size, column size base on the screen orientation
     *
     * @param orientation AbilityInfo.DisplayOrientation
     */
    public void changeOrientation(AbilityInfo.DisplayOrientation orientation) {
        imageColumns = orientation == AbilityInfo.DisplayOrientation.PORTRAIT ? 3 : 5;
        folderColumns = orientation == AbilityInfo.DisplayOrientation.PORTRAIT ? 2 : 4;
        columns = imageColumns;
    }

    /**
     * setupAdapters
     *
     * @param selectedImages        ArrayList<Image>
     * @param onImageClickListener  OnImageClickListener
     * @param onFolderClickListener OnFolderClickListener
     */
    public void setupAdapters(ArrayList<Image> selectedImages, OnImageClickListener onImageClickListener, OnFolderClickListener onFolderClickListener) {
        final ImageLoader imageLoader = ImagePickerComponentHolder.getInstance().getImageLoader();
        if (config.getMode() == MODE_SINGLE && selectedImages != null && selectedImages.size() > 1) {
            imageAdapter = new ImagePickerAdapter(context, columns, imageLoader, null, onImageClickListener, config.getMode());
        } else {
            imageAdapter = new ImagePickerAdapter(context, columns, imageLoader, selectedImages, onImageClickListener, config.getMode());
        }
        List<Folder> data = new ArrayList<>();
        folderAdapter = new FolderPickerAdapter(context, folderColumns, data, imageLoader, new OnFolderClickListener() {
            @Override
            public void onFolderClick(Folder bucket) {
                onFolderClickListener.onFolderClick(bucket);
            }
        });
    }

    /**
     * 设置分割间距
     *
     * @param columns
     */
    private void setItemDecoration(int columns) {
    }

    /**
     * handleBack
     *
     * @return boolean
     */
    public boolean handleBack() {
        if (config.isFolderMode() && !isDisplayingFolderView()) {
            setFolderAdapter(null);
            setFolderAdapter(list);
            return true;
        }
        return false;
    }

    private boolean isDisplayingFolderView() {
        return recyclerView.getItemProvider() == null || recyclerView.getItemProvider() instanceof FolderPickerAdapter;
    }

    /**
     * getTitle
     *
     * @return String
     */
    public String getTitle() {
        if (isDisplayingFolderView()) {
            return ConfigUtils.getFolderTitle(context, config);
        }
        if (config.getMode() == MODE_SINGLE) {
            return ConfigUtils.getImageTitle(context, config);
        }
        final int imageSize = imageAdapter.getSelectedImages().size();
        final boolean useDefaultTitle = !ImagePickerUtils.isStringEmpty(config.getImageTitle()) && imageSize == 0;
        if (useDefaultTitle) {
            return ConfigUtils.getImageTitle(context, config);
        }
        return config.getLimit() == MAX_LIMIT
                ? String.format(context.getString(ResourceTable.String_ef_selected), imageSize)
                : String.format(context.getString(ResourceTable.String_ef_selected_with_limit), imageSize, config.getLimit());
    }

    /**
     * setImageAdapter
     *
     * @param images List<Image>
     */
    public void setImageAdapter(List<Image> images) {
        imageAdapter.setData(images);
        setItemDecoration(imageColumns);
        recyclerView.setItemProvider(imageAdapter);
    }

    /**
     * setFolderAdapter
     *
     * @param folders List<Folder>
     */
    public void setFolderAdapter(List<Folder> folders) {
        if (folders != null) {
            this.list = folders;
        }
        folderAdapter.setData(folders);
        setItemDecoration(folderColumns);
        recyclerView.setItemProvider(folderAdapter);
    }

    private void checkAdapterIsInitialized() {
        if (imageAdapter == null) {
            throw new IllegalStateException("Must call setupAdapters first!");
        }
    }

    /**
     * getSelectedImages
     *
     * @return List<Image>
     */
    public List<Image> getSelectedImages() {
        checkAdapterIsInitialized();
        return imageAdapter.getSelectedImages();
    }

    /**
     * setImageSelectedListener
     *
     * @param listener OnImageSelectedListener
     */
    public void setImageSelectedListener(OnImageSelectedListener listener) {
        checkAdapterIsInitialized();
        imageAdapter.setImageSelectedListener(listener);
    }

    /**
     * selectImage
     *
     * @param isSelected boolean
     * @return boolean
     */
    public boolean selectImage(boolean isSelected) {
        if (config.getMode() == IpCons.MODE_MULTIPLE) {
            if (imageAdapter.getSelectedImages().size() >= config.getLimit() && !isSelected) {
                new ToastDialog(context).setText(ResourceUtil.getText(context, ResourceTable.String_ef_msg_limit_images)).show();
                return false;
            }
        } else if (config.getMode() == MODE_SINGLE) {
            if (imageAdapter.getSelectedImages().size() > 0) {
                imageAdapter.removeAllSelectedSingleClick();
            }
        } else {
        }
        return true;
    }

    /**
     * isShowDoneButton
     *
     * @return boolean
     */
    public boolean isShowDoneButton() {
        return !isDisplayingFolderView()
                && !imageAdapter.getSelectedImages().isEmpty()
                && (config.getReturnMode() != ReturnMode.ALL && config.getReturnMode() != ReturnMode.GALLERY_ONLY);
    }
}
