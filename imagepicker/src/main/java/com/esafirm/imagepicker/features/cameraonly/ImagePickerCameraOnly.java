package com.esafirm.imagepicker.features.cameraonly;

import com.esafirm.imagepicker.features.ImagePickerAbility;
import com.esafirm.imagepicker.features.ImagePickerConfigFactory;
import com.esafirm.imagepicker.features.IpCons;

import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;

import static com.esafirm.imagepicker.features.ImagePicker.START_TYPE;

/**
 * ImagePickerCameraOnly
 */
public class ImagePickerCameraOnly {
    private CameraOnlyConfig config = ImagePickerConfigFactory.createCameraDefault();

    /**
     * imageDirectory
     *
     * @param directory String
     * @return ImagePickerCameraOnly
     */
    public ImagePickerCameraOnly imageDirectory(String directory) {
        config.setImageDirectory(directory);
        return this;
    }

    /**
     * imageFullDirectory
     *
     * @param fullPath String
     * @return ImagePickerCameraOnly
     */
    public ImagePickerCameraOnly imageFullDirectory(String fullPath) {
        config.setImageFullDirectory(fullPath);
        return this;
    }

    /**
     * start
     *
     * @param ability FractionAbility
     */
    public void start(FractionAbility ability) {
        start(ability, IpCons.RC_IMAGE_CAMERA);
    }

    /**
     * start
     *
     * @param ability     FractionAbility
     * @param requestCode int
     */
    public void start(FractionAbility ability, int requestCode) {
        ability.startAbilityForResult(getIntent(ability), requestCode);
    }

    /**
     * start
     *
     * @param fragment Fraction
     */
    public void start(Fraction fragment) {
        start(fragment, IpCons.RC_IMAGE_CAMERA);
    }

    /**
     * start
     *
     * @param fragment    Fraction
     * @param requestCode int
     */
    public void start(Fraction fragment, int requestCode) {
        fragment.getFractionAbility().startAbilityForResult(getIntent(fragment.getFractionAbility()), requestCode);
    }

    /**
     * getIntent
     *
     * @param context FractionAbility
     * @return Intent
     */
    public Intent getIntent(FractionAbility context) {
        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withBundleName(context.getBundleName())
                .withAbilityName(ImagePickerAbility.class.getName())
                .build();
        intent.setOperation(operation);
        intent.setParam(CameraOnlyConfig.class.getSimpleName(), config);
        intent.setParam(START_TYPE, "camera");
        return intent;
    }
}
