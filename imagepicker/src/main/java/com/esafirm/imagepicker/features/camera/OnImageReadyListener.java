package com.esafirm.imagepicker.features.camera;

import com.esafirm.imagepicker.model.Image;

import java.util.List;

/**
 * OnImageReadyListener
 */
public interface OnImageReadyListener {
    /**
     * onImageReady
     *
     * @param image List<Image>
     */
    void onImageReady(List<Image> image);
}
