package com.esafirm.imagepicker.features.camera;

import com.esafirm.imagepicker.ResourceTable;
import com.esafirm.imagepicker.helper.ResourceUtil;
import com.esafirm.imagepicker.helper.ToastUtil;

import ohos.app.Context;

import ohos.media.camera.CameraKit;
import ohos.media.camera.device.CameraAbility;

/**
 * CameraHelper
 */
public class CameraHelper {
    /**
     * checkCameraAvailability
     *
     * @param context Context
     * @return boolean
     */
    public static boolean checkCameraAvailability(Context context) {
        CameraKit camerakit = CameraKit.getInstance(context.getApplicationContext());
        boolean isAvailable = true;
        if (camerakit == null || camerakit.getCameraIds().length <= 0) {
            isAvailable = false;
        }
        String cameraId = camerakit.getCameraIds()[0];
        CameraAbility commCamera = camerakit.getCameraAbility(cameraId);
        if (commCamera == null) {
            isAvailable = false;
        }
        if (!isAvailable) {
            ToastUtil.show(context, ResourceUtil.getText(context, ResourceTable.String_ef_error_no_camera));
        }
        return isAvailable;
    }
}