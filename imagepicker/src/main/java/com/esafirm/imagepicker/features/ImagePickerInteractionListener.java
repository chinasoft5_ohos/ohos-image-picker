package com.esafirm.imagepicker.features;

import com.esafirm.imagepicker.model.Image;

import ohos.aafwk.content.Intent;

import java.util.List;

/**
 * ImagePickerInteractionListener
 */
public interface ImagePickerInteractionListener {
    /**
     * setTitle
     *
     * @param title String
     */
    void setTitle(String title);

    /**
     * cancel
     */
    void cancel();

    /**
     * finishPickImages
     *
     * @param result Intent
     */
    void finishPickImages(Intent result);

    /**
     * Called when the user selects or deselects sn image. Also called in onCreateView.
     * May include Images whose files no longer exist.
     *
     * @param imageList List<Image>
     */
    void selectionChanged(List<Image> imageList);
}
