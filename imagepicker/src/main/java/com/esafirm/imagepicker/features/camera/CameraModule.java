package com.esafirm.imagepicker.features.camera;

import com.esafirm.imagepicker.features.common.BaseConfig;

import ohos.aafwk.content.Intent;

import ohos.app.Context;

/**
 * CameraModule
 */
public interface CameraModule {
    /**
     * getCameraIntent
     *
     * @param context Context
     * @param config  BaseConfig
     * @return Intent
     */
    Intent getCameraIntent(Context context, BaseConfig config);

    /**
     * getImage
     *
     * @param context            Context
     * @param intent             Intent
     * @param imageReadyListener OnImageReadyListener
     */
    void getImage(Context context, Intent intent, OnImageReadyListener imageReadyListener);

    /**
     * removeImage
     */
    void removeImage();
}