package com.esafirm.imagepicker.features;

import com.esafirm.imagepicker.features.cameraonly.ImagePickerCameraOnly;
import com.esafirm.imagepicker.helper.ConfigUtils;
import com.esafirm.imagepicker.helper.LocaleManager;
import com.esafirm.imagepicker.model.Image;

import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;

import ohos.app.Context;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * ImagePicker
 */
public abstract class ImagePicker {
    /**
     * RESULT_OK
     */
    public final static int RESULT_OK = -1;
    /**
     * RESULT_CANCELED
     */
    public final static int RESULT_CANCELED = 0;
    /**
     * START_TYPE
     */
    public final static String START_TYPE = "type";

    private ImagePickerConfig config;

    /**
     * start
     */
    public abstract void start();

    /**
     * start
     *
     * @param requestCode int
     */
    public abstract void start(int requestCode);

    /**
     * ImagePickerWithAbility
     */
    public static class ImagePickerWithAbility extends ImagePicker {
        private FractionAbility ability;

        /**
         * 初始化
         *
         * @param ability FractionAbility
         */
        public ImagePickerWithAbility(FractionAbility ability) {
            this.ability = ability;
            init(ability);
        }

        @Override
        public void start(int requestCode) {
            ability.startAbilityForResult(getIntent(ability), requestCode);
        }

        @Override
        public void start() {
            ability.startAbilityForResult(getIntent(ability), IpCons.RC_IMAGE_PICKER);
        }
    }

    /**
     * ImagePickerWithFragment
     */
    public static class ImagePickerWithFragment extends ImagePicker {
        private Fraction fragment;

        /**
         * 初始化
         *
         * @param fragment Fraction
         */
        public ImagePickerWithFragment(Fraction fragment) {
            this.fragment = fragment;
            init(fragment.getContext());
        }

        @Override
        public void start(int requestCode) {
            fragment.getFractionAbility().startAbilityForResult(getIntent(fragment.getFractionAbility()), requestCode);
        }

        @Override
        public void start() {
            fragment.getFractionAbility().startAbilityForResult(getIntent(fragment.getFractionAbility()), IpCons.RC_IMAGE_PICKER);
        }
    }

    /**
     * init
     *
     * @param context Context
     */
    public void init(Context context) {
        config = ImagePickerConfigFactory.createDefault();
    }

    /**
     * create
     *
     * @param ability FractionAbility
     * @return ImagePickerWithAbility
     */
    public static ImagePickerWithAbility create(FractionAbility ability) {
        return new ImagePickerWithAbility(ability);
    }

    /**
     * create
     *
     * @param fragment Fraction
     * @return ImagePickerWithFragment
     */
    public static ImagePickerWithFragment create(Fraction fragment) {
        return new ImagePickerWithFragment(fragment);
    }

    /**
     * cameraOnly
     *
     * @return ImagePickerCameraOnly
     */
    public static ImagePickerCameraOnly cameraOnly() {
        return new ImagePickerCameraOnly();
    }

    /**
     * single
     *
     * @return ImagePicker
     */
    public ImagePicker single() {
        config.setMode(IpCons.MODE_SINGLE);
        return this;
    }

    /**
     * multi
     *
     * @return ImagePicker
     */
    public ImagePicker multi() {
        config.setMode(IpCons.MODE_MULTIPLE);
        return this;
    }

    /**
     * returnMode
     *
     * @param returnMode ReturnMode
     * @return ImagePicker
     */
    public ImagePicker returnMode(ReturnMode returnMode) {
        config.setReturnMode(returnMode);
        return this;
    }

    /**
     * limit
     *
     * @param count int
     * @return ImagePicker
     */
    public ImagePicker limit(int count) {
        config.setLimit(count);
        return this;
    }

    /**
     * showCamera
     *
     * @param show boolean
     * @return ImagePicker
     */
    public ImagePicker showCamera(boolean show) {
        config.setShowCamera(show);
        return this;
    }

    /**
     * toolbarArrowColor
     *
     * @param color int
     * @return ImagePicker
     */
    public ImagePicker toolbarArrowColor(int color) {
        config.setArrowColor(color);
        return this;
    }

    /**
     * toolbarFolderTitle
     *
     * @param title String
     * @return ImagePicker
     */
    public ImagePicker toolbarFolderTitle(String title) {
        config.setFolderTitle(title);
        return this;
    }

    /**
     * toolbarImageTitle
     *
     * @param title String
     * @return ImagePicker
     */
    public ImagePicker toolbarImageTitle(String title) {
        config.setImageTitle(title);
        return this;
    }

    /**
     * toolbarDoneButtonText
     *
     * @param text String
     * @return ImagePicker
     */
    public ImagePicker toolbarDoneButtonText(String text) {
        config.setDoneButtonText(text);
        return this;
    }

    /**
     * origin
     *
     * @param images String
     * @return ImagePicker
     */
    public ImagePicker origin(String images) {
        config.setSelectedImages(images);
        return this;
    }

    /**
     * exclude
     *
     * @param images ArrayList<Image> 的json
     * @return ImagePicker
     */
    public ImagePicker exclude(String images) {
        config.setExcludedImages(images);
        return this;
    }

    /**
     * excludeFiles
     *
     * @param files ArrayList<File> 的json
     * @return ImagePicker
     */
    public ImagePicker excludeFiles(String files) {
        config.setExcludedImageFiles(files);
        return this;
    }

    /**
     * folderMode
     *
     * @param folderMode boolean
     * @return ImagePicker
     */
    public ImagePicker folderMode(boolean folderMode) {
        Logger.getLogger("TAG").log(Level.SEVERE, "folderMode: =====$folderMode$=" + folderMode);
        config.setFolderMode(folderMode);
        return this;
    }

    /**
     * includeVideo
     *
     * @param includeVideo boolean
     * @return ImagePicker
     */
    public ImagePicker includeVideo(boolean includeVideo) {
        config.setIncludeVideo(includeVideo);
        return this;
    }

    /**
     * onlyVideo
     *
     * @param onlyVideo boolean
     * @return ImagePicker
     */
    public ImagePicker onlyVideo(boolean onlyVideo) {
        config.setOnlyVideo(onlyVideo);
        return this;
    }

    /**
     * includeAnimation
     *
     * @param includeAnimation boolean
     * @return ImagePicker
     */
    public ImagePicker includeAnimation(boolean includeAnimation) {
        config.setIncludeAnimation(includeAnimation);
        return this;
    }

    /**
     * imageDirectory
     *
     * @param directory String
     * @return ImagePicker
     */
    public ImagePicker imageDirectory(String directory) {
        config.setImageDirectory(directory);
        return this;
    }

    /**
     * imageFullDirectory
     *
     * @param fullPath String
     * @return ImagePicker
     */
    public ImagePicker imageFullDirectory(String fullPath) {
        config.setImageFullDirectory(fullPath);
        return this;
    }

    /**
     * theme
     *
     * @param theme StyleRes
     * @return ImagePicker
     */
    public ImagePicker theme(int theme) {
        config.setTheme(theme);
        return this;
    }

    /**
     * language
     *
     * @param language String
     * @return ImagePicker
     */
    public ImagePicker language(String language) {
        config.setLanguage(language);
        return this;
    }

    /**
     * getConfig
     *
     * @return ImagePickerConfig
     */
    public ImagePickerConfig getConfig() {
        LocaleManager.setLanguage(config.getLanguage());
        return ConfigUtils.checkConfig(config);
    }

    /**
     * getIntent
     *
     * @param context Context
     * @return Intent
     */
    public Intent getIntent(Context context) {
        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withBundleName(context.getBundleName())
                .withAbilityName(ImagePickerAbility.class.getName())
                .build();
        intent.setOperation(operation);
        intent.setParam(ImagePickerConfig.class.getSimpleName(), config);
        return intent;
    }

    /**
     * shouldHandle
     *
     * @param requestCode int
     * @param resultCode  int
     * @param data        Intent
     * @return boolean
     */
    public static boolean shouldHandle(int requestCode, int resultCode, Intent data) {
        return resultCode == RESULT_OK
                && requestCode == IpCons.RC_IMAGE_PICKER
                && data != null;
    }

    /**
     * getImages
     *
     * @param intent Intent
     * @return List<Image>
     */
    public static List<Image> getImages(Intent intent) {
        if (intent == null) {
            return new ArrayList<>();
        }
        if (intent.getSequenceableArrayListParam(IpCons.EXTRA_SELECTED_IMAGES) != null) {
            return intent.getSequenceableArrayListParam(IpCons.EXTRA_SELECTED_IMAGES);
        }
        return new ArrayList<>();
    }

    /**
     * getFirstImageOrNull
     *
     * @param intent Intent
     * @return Image
     */
    public static Image getFirstImageOrNull(Intent intent) {
        List<Image> images = getImages(intent);
        if (images == null || images.isEmpty()) {
            return null;
        }
        return images.get(0);
    }
}
