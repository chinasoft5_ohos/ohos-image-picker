package com.esafirm.imagepicker.features;

import com.esafirm.imagepicker.features.cameraonly.CameraOnlyConfig;

import ohos.utils.zson.ZSONObject;

/**
 * ImagePickerConfigFactory
 */
public class ImagePickerConfigFactory {
    /**
     * createCameraDefault
     *
     * @return CameraOnlyConfig
     */
    public static CameraOnlyConfig createCameraDefault() {
        CameraOnlyConfig config = new CameraOnlyConfig();
        config.setSavePath(ZSONObject.toZSONString(ImagePickerSavePath.DEFAULT));
        config.setReturnMode(ReturnMode.ALL);
        return config;
    }

    /**
     * createDefault
     *
     * @return ImagePickerConfig
     */
    public static ImagePickerConfig createDefault() {
        ImagePickerConfig config = new ImagePickerConfig();
        config.setMode(IpCons.MODE_MULTIPLE);
        config.setLimit(IpCons.MAX_LIMIT);
        config.setShowCamera(true);
        config.setFolderMode(false);
        config.setSelectedImages("");
        config.setSavePath(ZSONObject.toZSONString(ImagePickerSavePath.DEFAULT));
        config.setReturnMode(ReturnMode.NONE);
        return config;
    }
}
