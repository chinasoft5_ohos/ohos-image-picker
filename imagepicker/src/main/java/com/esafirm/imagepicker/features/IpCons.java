package com.esafirm.imagepicker.features;

/**
 * IpCons
 */
public class IpCons {
    /**
     * MODE_SINGLE
     */
    public static final int MODE_SINGLE = 1;
    /**
     * MODE_MULTIPLE
     */
    public static final int MODE_MULTIPLE = 2;
    /**
     * MAX_LIMIT
     */
    public static final int MAX_LIMIT = 999;
    /**
     * RC_IMAGE_PICKER
     */
    public static final int RC_IMAGE_PICKER = 0x229;
    /**
     * RC_IMAGE_CAMERA
     */
    public static final int RC_IMAGE_CAMERA = 0x239;
    /**
     * EXTRA_SELECTED_IMAGES
     */
    public static final String EXTRA_SELECTED_IMAGES = "selectedImages";
}
