package com.esafirm.imagepicker.features;

import ohos.utils.Parcel;
import ohos.utils.Sequenceable;

/**
 * ImagePickerSavePath
 */
public class ImagePickerSavePath implements Sequenceable {
    /**
     * ImagePickerSavePath
     */
    public static final ImagePickerSavePath DEFAULT = new ImagePickerSavePath("Camera", false);
    /**
     * Producer
     */
    public static final Producer PRODUCER = new Producer() {
        /**
         * createFromParcel
         *
         * @param in Parcel
         * @return Image
         */
        public ImagePickerSavePath createFromParcel(Parcel in) {
            // Initialize an instance first, then do customized unmarshlling.
            ImagePickerSavePath imagePickerSavePath = new ImagePickerSavePath();
            imagePickerSavePath.unmarshalling(in);
            return imagePickerSavePath;
        }
    };

    private String path;
    private boolean isFullPath;

    /**
     * 初始化
     *
     * @param path
     * @param isFullPath
     */
    public ImagePickerSavePath(String path, boolean isFullPath) {
        this.path = path;
        this.isFullPath = isFullPath;
    }

    /**
     * 构造函数
     */
    public ImagePickerSavePath() {
    }

    /**
     * getPath
     *
     * @return String
     */
    public String getPath() {
        return path;
    }

    /**
     * isFullPath
     *
     * @return boolean
     */
    public boolean isFullPath() {
        return isFullPath;
    }

    /**
     * marshalling
     *
     * @param out Parcel
     * @return boolean
     */
    public boolean marshalling(Parcel out) {
        return out.writeString(this.path) && out.writeBoolean(this.isFullPath);
    }

    /**
     * unmarshalling
     *
     * @param in Parcel
     * @return boolean
     */
    public boolean unmarshalling(Parcel in) {
        this.path = in.readString();
        this.isFullPath = in.readBoolean();
        return true;
    }
}
