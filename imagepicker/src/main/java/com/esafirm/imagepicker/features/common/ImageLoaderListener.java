package com.esafirm.imagepicker.features.common;

import com.esafirm.imagepicker.model.Folder;
import com.esafirm.imagepicker.model.Image;

import java.util.List;

/**
 * ImageLoaderListener
 */
public interface ImageLoaderListener {
    /**
     * onImageLoaded
     *
     * @param images  List<Image>
     * @param folders
     */
    void onImageLoaded(List<Image> images, List<Folder> folders);

    /**
     * onFailed
     *
     * @param throwable Throwable
     */
    void onFailed(Throwable throwable);
}
