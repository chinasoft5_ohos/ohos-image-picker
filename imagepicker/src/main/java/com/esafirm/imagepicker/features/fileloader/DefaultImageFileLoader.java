package com.esafirm.imagepicker.features.fileloader;

import com.esafirm.imagepicker.features.common.ImageLoaderListener;
import com.esafirm.imagepicker.helper.ImagePickerUtils;
import com.esafirm.imagepicker.helper.Utility;
import com.esafirm.imagepicker.model.Folder;
import com.esafirm.imagepicker.model.Image;
import com.esafirm.imagepicker.model.Options;

import ohos.aafwk.ability.DataAbilityHelper;
import ohos.aafwk.ability.DataAbilityRemoteException;

import ohos.app.Context;

import ohos.data.resultset.ResultSet;

import ohos.media.photokit.metadata.AVStorage;

import ohos.utils.net.Uri;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;

import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * DefaultImageFileLoader
 */
public class DefaultImageFileLoader implements ImageFileLoader {
    private Context context;
    private ExecutorService executorService;

    /**
     * 构造函数
     *
     * @param context Context
     */
    public DefaultImageFileLoader(Context context) {
        this.context = context.getApplicationContext();
    }

    private final String[] projection = new String[]{
            AVStorage.Images.Media.ID,
            AVStorage.Images.Media.DISPLAY_NAME,
            AVStorage.Images.Media.DATA,
//            AVStorage.Images.Media.BUCKET_DISPLAY_NAME,
            AVStorage.Images.Media.DATE_ADDED
    };

    @Override
    public void loadDeviceImages(
            final boolean isFolderMode,
            final boolean onlyVideo,
            final boolean includeVideo,
            final boolean includeAnimation,
            final ArrayList<File> excludedImages,
            final ImageLoaderListener listener
    ) {
        getExecutorService().execute(
                new ImageLoadRunnable(
                        isFolderMode,
                        onlyVideo,
                        includeVideo,
                        includeAnimation,
                        excludedImages,
                        listener
                ));
    }

    @Override
    public void abortLoadImages() {
        if (executorService != null) {
            executorService.shutdown();
            executorService = null;
        }
    }

    private ExecutorService getExecutorService() {
        if (executorService == null) {
            executorService = Executors.newScheduledThreadPool(2);
        }
        return executorService;
    }

    private class ImageLoadRunnable implements Runnable {
        private boolean includeVideo;
        private boolean onlyVideo;
        private boolean includeAnimation;
        private ArrayList<File> exlucedImages;
        private ImageLoaderListener listener;

        ImageLoadRunnable(
                boolean isFolderMode,
                boolean onlyVideo,
                boolean includeVideo,
                boolean includeAnimation,
                ArrayList<File> excludedImages,
                ImageLoaderListener listener
        ) {
            this.includeVideo = includeVideo;
            this.includeAnimation = includeAnimation;
            this.onlyVideo = onlyVideo;
            this.exlucedImages = excludedImages;
            this.listener = listener;
        }

        @Override
        public void run() {
            List<Image> temp = new ArrayList<>();
            List<Folder> folders = new ArrayList<>();
            findPicture(temp, folders);
            listener.onImageLoaded(temp, folders);
        }

        private void findPicture(List<Image> temp, List<Folder> folders) {
            ResultSet resultSet = null;
            try {
                DataAbilityHelper helper = DataAbilityHelper.creator(context);
                resultSet = Utility.getImageVideoResult(helper, Options.Mode.PICTURE);
                if (onlyVideo) {
                    resultSet = Utility.getImageVideoResult(helper, Options.Mode.VIDEO);
                } else {
                }
                if (includeVideo) {
                    resultSet = Utility.getImageVideoResult(helper, Options.Mode.ALL);
                } else {
                }
                if (resultSet != null) {
                    int picCount = resultSet.getRowCount();
                    resultSet.goToLastRow();
                    handleData(temp, folders, resultSet, picCount);
                    resultSet.close();
                } else {
                    listener.onFailed(new NullPointerException());
                }
            } catch (DataAbilityRemoteException e) {
                e.printStackTrace();
                listener.onFailed(e);
            }
        }

        private void handleData(List<Image> temp, List<Folder> folders, ResultSet resultSet, int picCount) {
            for (int i = 0; i < picCount; i++) {
                String filePath = resultSet.getString(resultSet
                        .getColumnIndexForName(AVStorage.Images.Media.DATA));
                File file = makeSafeFile(filePath);
                if (file == null) {
                    resultSet.goToPreviousRow();
                    continue;
                } else {
                }
                if (exlucedImages != null && exlucedImages.contains(file)) {
                    resultSet.goToPreviousRow();
                    continue;
                } else {
                }
                if (!includeAnimation) {
                    if (ImagePickerUtils.isGifFormat(filePath)) {
                        resultSet.goToPreviousRow();
                        continue;
                    } else {
                    }
                } else {
                }
                Image img = getImage(temp, resultSet, filePath);
                File imageParentFile = file.getParentFile();
                Folder imageFolder = new Folder(imageParentFile.getName());
                try {
                    imageFolder.path = imageParentFile.getCanonicalPath();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (!folders.contains(imageFolder)) {
                    ArrayList<Image> images = new ArrayList<>();
                    images.add(img);
                    imageFolder.setImages(images);
                    folders.add(imageFolder);
                } else {
                    folders.get(folders.indexOf(imageFolder)).getImages().add(img);
                }
                resultSet.goToPreviousRow();
            }
        }
    }

    @NotNull
    private Image getImage(List<Image> temp, ResultSet resultSet, String filePath) {
        String mediaType = resultSet.getString(resultSet
                .getColumnIndexForName(AVStorage.Images.Media.MIME_TYPE));
        int id = resultSet.getInt(resultSet
                .getColumnIndexForName(AVStorage.Images.Media.ID));
        Uri uri = Uri.appendEncodedPathToUri(AVStorage.Images.Media.EXTERNAL_DATA_ABILITY_URI, String.valueOf(id));
        Image img = new Image();
        int imageDate = resultSet.getInt(resultSet
                .getColumnIndexForName(AVStorage.Images.Media.DATE_MODIFIED));

        String fileName = resultSet.getString(resultSet
                .getColumnIndexForName(AVStorage.Images.Media.DISPLAY_NAME));
        long duration = resultSet.getLong(resultSet
                .getColumnIndexForName(AVStorage.Images.Media.DURATION));
        img.setUriSchema(uri.toString());
        img.setId(id);
        img.setName(fileName);
        img.setPath(filePath);
        img.setDuration(duration);
        temp.add(img);
        return img;
    }

    private static File makeSafeFile(String path) {
        if (path == null || path.isEmpty()) {
            return null;
        }
        return new File(path);
    }
}
