package com.esafirm.imagepicker.features;

import com.esafirm.imagepicker.features.camera.DefaultCameraModule;
import com.esafirm.imagepicker.features.common.BaseConfig;
import com.esafirm.imagepicker.features.common.BasePresenter;
import com.esafirm.imagepicker.features.common.ImageLoaderListener;
import com.esafirm.imagepicker.features.fileloader.DefaultImageFileLoader;
import com.esafirm.imagepicker.helper.ConfigUtils;
import com.esafirm.imagepicker.model.Folder;
import com.esafirm.imagepicker.model.Image;

import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;

import ohos.app.Context;

import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;

import java.io.File;

import java.util.ArrayList;
import java.util.List;

/**
 * ImagePickerPresenter
 */
class ImagePickerPresenter extends BasePresenter<ImagePickerView> {
    private DefaultImageFileLoader imageLoader;
    private DefaultCameraModule cameraModule;
    private EventHandler main = new EventHandler(EventRunner.getMainEventRunner());

    /**
     * 初始化
     *
     * @param imageLoader DefaultImageFileLoader
     */
    ImagePickerPresenter(DefaultImageFileLoader imageLoader) {
        this.imageLoader = imageLoader;
    }

    /**
     * getCameraModule
     *
     * @return DefaultCameraModule
     */
    DefaultCameraModule getCameraModule() {
        if (cameraModule == null) {
            cameraModule = new DefaultCameraModule();
        }
        return cameraModule;
    }

    /**
     * Set the camera module in onRestoreInstance
     *
     * @param cameraModule DefaultCameraModule
     */
    void setCameraModule(DefaultCameraModule cameraModule) {
        this.cameraModule = cameraModule;
    }

    /**
     * abortLoad
     */
    void abortLoad() {
        imageLoader.abortLoadImages();
    }

    /**
     * loadImages
     *
     * @param config ImagePickerConfig
     */
    void loadImages(ImagePickerConfig config) {
        if (!isViewAttached()) {
            return;
        }
        boolean isFolder = config.isFolderMode();
        boolean includeVideo = config.isIncludeVideo();
        boolean onlyVideo = config.isOnlyVideo();
        boolean includeAnimation = config.isIncludeAnimation();
        ArrayList<File> excludedImages = config.getExcludedImages();
        runOnUiIfAvailable(() -> getView().showLoading(true));
        imageLoader.loadDeviceImages(isFolder, onlyVideo, includeVideo, includeAnimation, excludedImages, new ImageLoaderListener() {
            @Override
            public void onImageLoaded(final List<Image> images, final List<Folder> folders) {
                runOnUiIfAvailable(() -> {
                    handleUI(images, folders);
                });
            }

            @Override
            public void onFailed(final Throwable throwable) {
                runOnUiIfAvailable(() -> getView().showError(throwable));
            }
        });
    }

    private void handleUI(List<Image> images, List<Folder> folders) {
        getView().isLoading(false);
        getView().showFetchCompleted(images, folders);
        final boolean isEmpty = folders != null ? folders.isEmpty() : images.isEmpty();
        if (isEmpty) {
            getView().showEmpty();
        } else {
            getView().showLoading(false);
        }
    }

    /**
     * onDoneSelectImages
     *
     * @param selectedImages List<Image>
     */
    void onDoneSelectImages(List<Image> selectedImages) {
        if (selectedImages != null && selectedImages.size() > 0) {
            for (int i = 0; i < selectedImages.size(); i++) {
                Image image = selectedImages.get(i);
                File file = new File(image.getPath());
                if (!file.exists()) {
                    selectedImages.remove(i);
                    i--;
                }
            }
            getView().finishPickImages(selectedImages);
        }
    }

    /**
     * 打开相机
     *
     * @param fragment    Fraction
     * @param config      BaseConfig
     * @param requestCode int
     */
    void captureImage(Fraction fragment, BaseConfig config, int requestCode) {
        Intent intent = getCameraModule().getCameraIntent(fragment.getFractionAbility(), config);
        fragment.getFractionAbility().startAbilityForResult(intent, requestCode);
    }

    /**
     * finishCaptureImage
     *
     * @param context Context
     * @param data    Intent
     * @param config  BaseConfig
     */
    void finishCaptureImage(Context context, Intent data, final BaseConfig config) {
        getCameraModule().getImage(context, data, images -> {
            if (ConfigUtils.shouldReturn(config, true)) {
                getView().finishPickImages(images);
            } else {
                getView().showCapturedImage();
            }
        });
    }

    /**
     * abortCaptureImage
     */
    void abortCaptureImage() {
        getCameraModule().removeImage();
    }

    private void runOnUiIfAvailable(Runnable runnable) {
        main.postTask(() -> {
            if (isViewAttached()) {
                runnable.run();
            }
        });
    }
}
