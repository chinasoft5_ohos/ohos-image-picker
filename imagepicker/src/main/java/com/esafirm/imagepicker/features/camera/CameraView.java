/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.esafirm.imagepicker.features.camera;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.surfaceprovider.SurfaceProvider;
import ohos.agp.window.service.WindowManager;
import ohos.app.Context;

import ohos.media.image.ImageReceiver;

/**
 * description : 自定义 SurfaceProvider
 *
 * @since 2021-04-15
 */
public class CameraView extends SurfaceProvider {
    private CameraManager mCameraManager;
    private boolean isFrontCamera;

    /**
     * 初始化 cameraView
     *
     * @param context context
     */
    public CameraView(Context context) {
        this(context, null);
    }

    /**
     * 初始化 cameraView
     *
     * @param context context
     * @param attrSet attrSet
     */
    public CameraView(Context context, AttrSet attrSet) {
        this(context, attrSet, null);
    }

    /**
     * 初始化 cameraView
     *
     * @param context   context
     * @param attrSet   attrSet
     * @param styleName styleName
     */
    public CameraView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        WindowManager.getInstance().getTopWindow().get().setTransparent(true);
        initView();
    }

    private void initView() {
        setVisibility(Component.VISIBLE);
        setFocusable(Component.FOCUS_ENABLE);
        setTouchFocusable(true);
        requestFocus();
        pinToZTop(false);
        mCameraManager = new CameraManager(mContext, this);
        if (getSurfaceOps().isPresent()) {
            getSurfaceOps().get().addCallback(mCameraManager);
        }
    }

    /**
     * 切换摄像头
     */
    public void switchCamera() {
        isFrontCamera = !isFrontCamera;
        mCameraManager.switchCamera();
    }

    /**
     * 拍照
     */
    public void capture() {
        mCameraManager.capture();
    }

    /**
     * 是否是前置摄像头
     *
     * @return true：是前置摄像头； false：不是前置摄像头
     */
    public boolean isFrontCamera() {
        return isFrontCamera;
    }

    /**
     * 设置为前置摄像头
     *
     * @param frontCamera true：设置为前置摄像头； false：设置为后置摄像头
     */
    public void setFrontCamera(boolean frontCamera) {
        isFrontCamera = frontCamera;
    }

    /**
     * 设置拍照时监听
     *
     * @param imageArrivalListener 拍照监听
     */
    public void setImageArrivalListener(ImageReceiver.IImageArrivalListener imageArrivalListener) {
        mCameraManager.setImageArrivalListener(imageArrivalListener);
    }

    /**
     * 设置闪光灯模式
     *
     * @param flashMode 闪光灯模式
     */
    public void setCameraFlashMode(int flashMode) {
        mCameraManager.setCameraFlashMode(flashMode);
    }
}
