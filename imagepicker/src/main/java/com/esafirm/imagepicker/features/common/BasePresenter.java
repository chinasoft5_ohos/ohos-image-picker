package com.esafirm.imagepicker.features.common;

/**
 * BasePresenter
 *
 * @param <T> param
 */
public class BasePresenter<T extends MvpView> {
    private T view;

    /**
     * attach view
     *
     * @param view view
     */
    public void attachView(T view) {
        this.view = view;
    }

    /**
     * getView
     *
     * @return view
     */
    public T getView() {
        return view;
    }

    /**
     * detachView
     */
    public void detachView() {
        view = null;
    }

    /**
     * isViewAttached
     *
     * @return boolean
     */
    protected boolean isViewAttached() {
        return view != null;
    }
}
