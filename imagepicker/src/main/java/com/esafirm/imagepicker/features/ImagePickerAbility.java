package com.esafirm.imagepicker.features;

import com.esafirm.imagepicker.ResourceTable;
import com.esafirm.imagepicker.features.cameraonly.CameraOnlyConfig;
import com.esafirm.imagepicker.model.Folder;
import com.esafirm.imagepicker.model.Image;
import com.esafirm.imagepicker.view.ToolBar;

import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.content.Intent;

import ohos.agp.components.StackLayout;

import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;

import java.util.List;

import static com.esafirm.imagepicker.features.ImagePicker.RESULT_CANCELED;
import static com.esafirm.imagepicker.features.ImagePicker.START_TYPE;

/**
 * ImagePickerAbility
 */
public class ImagePickerAbility extends FractionAbility implements ImagePickerInteractionListener, ImagePickerView {
    private ToolBar actionBar;
    private ImagePickerFragment imagePickerFragment;
    private ImagePickerConfig config;
    private ToolBar.ClickListener clickListener = new ToolBar.ClickListener() {
        @Override
        public void click(int component) {
            switch (component) {
                case 1:
                    onBackPressed();
                    break;
                case 2:
                    imagePickerFragment.captureImageWithPermission();
                    break;
                case 3:
                    imagePickerFragment.onDone();
                    break;
                default:
                    break;
            }
        }
    };
    /**
     * 哪一种类型的启动 区分从相册启动相机还是直接启动相机
     */
    private String startType;

    @Override
    protected void onStart(Intent intent) {
        try {
            int color = getResourceManager().getElement(ResourceTable.Color_status_bar_color).getColor();
            getWindow().setStatusBarColor(color);
        } catch (IOException | NotExistException | WrongTypeException e) {
            e.printStackTrace();
        }
        super.onStart(intent);
        setResult(RESULT_CANCELED, null);
        if (intent == null) {
            terminateAbility();
            return;
        }
        config = intent.getSequenceableParam(ImagePickerConfig.class.getSimpleName());
        CameraOnlyConfig cameraOnlyConfig = intent.getSequenceableParam(CameraOnlyConfig.class.getSimpleName());
        startType = intent.getStringParam(START_TYPE);
        boolean isCameraOnly = cameraOnlyConfig != null;
        if (!isCameraOnly) {
            setTheme(config.getTheme());
            setUIContent(ResourceTable.Layout_ef_ability_image_picker);
            setupView();
        } else {
            setUIContent(createCameraLayout());
        }
        imagePickerFragment = new ImagePickerFragment(config, cameraOnlyConfig, this);
        getFractionManager()
                .startFractionScheduler()
                .add(ResourceTable.Id_ef_imagepicker_fragment_placeholder, imagePickerFragment)
                .show(imagePickerFragment)
                .submit();
    }

    private StackLayout createCameraLayout() {
        StackLayout frameLayout = new StackLayout(this);
        frameLayout.setId(ResourceTable.Id_ef_imagepicker_fragment_placeholder);
        return frameLayout;
    }

    @Override
    public void onBackPressed() {
        if (!imagePickerFragment.handleBack()) {
            super.onBackPressed();
        }
    }

    private void setupView() {
        actionBar = (ToolBar) findComponentById(ResourceTable.Id_toolbar);
        actionBar.setOnActionClickListener(clickListener);
    }

    @Override
    public void setTitle(String title) {
        actionBar.setTitle(title);
    }

    @Override
    public void cancel() {
        terminateAbility();
    }


    @Override
    public void selectionChanged(List<Image> imageList) {
        if (imageList != null && imageList.size() > 0) {
            actionBar.setVisible(true);
        } else {
            actionBar.setVisible(false);
        }
    }

    @Override
    public void finishPickImages(Intent result) {
        setResult(ImagePicker.RESULT_OK, result);
        terminateAbility();
    }

    @Override
    public void isLoading(boolean isloading) {
        imagePickerFragment.isLoading(isloading);
    }

    @Override
    public void showLoading(boolean isLoading) {
        imagePickerFragment.showLoading(isLoading);
    }

    @Override
    public void showFetchCompleted(List<Image> images, List<Folder> folders) {
        imagePickerFragment.showFetchCompleted(images, folders);
    }

    @Override
    public void showError(Throwable throwable) {
        imagePickerFragment.showError(throwable);
    }

    @Override
    public void showEmpty() {
        imagePickerFragment.showEmpty();
    }

    @Override
    public void showCapturedImage() {
        imagePickerFragment.showCapturedImage();
    }

    @Override
    public void finishPickImages(List<Image> images) {
        imagePickerFragment.finishPickImages(images);
    }

    @Override
    public void onRequestPermissionsFromUserResult(int requestCode, String[] permissions, int[] grantResults) {
        imagePickerFragment.onRequestPermissionsFromUserResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onAbilityResult(int requestCode, int resultCode, Intent resultData) {
        super.onAbilityResult(requestCode, resultCode, resultData);
        imagePickerFragment.onAbilityResult(requestCode, resultCode, resultData, startType);
    }
}
