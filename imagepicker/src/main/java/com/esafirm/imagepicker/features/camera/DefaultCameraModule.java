package com.esafirm.imagepicker.features.camera;

import com.esafirm.imagepicker.ResourceTable;
import com.esafirm.imagepicker.features.ImagePickerConfigFactory;
import com.esafirm.imagepicker.features.common.BaseConfig;
import com.esafirm.imagepicker.helper.LogUtil;
import com.esafirm.imagepicker.helper.ResourceUtil;
import com.esafirm.imagepicker.helper.UriUtils;
import com.esafirm.imagepicker.model.Options;

import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;

import ohos.agp.utils.TextTool;
import ohos.app.Context;

import ohos.bundle.AbilityInfo;

import ohos.utils.net.Uri;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;

/**
 * DefaultCameraModule
 */
public class DefaultCameraModule implements Serializable, CameraModule {
    private String currentImagePath = null;
    private String currentUri = null;

    /**
     * Helper function to get camera Intent without config
     *
     * @param context Context
     * @return Intent
     */
    public Intent getCameraIntent(Context context) {
        return getCameraIntent(context, ImagePickerConfigFactory.createDefault());
    }

    private void prepareForNewIntent() {
        currentImagePath = null;
        currentUri = null;
    }

    private Uri createCameraUri(File imageFile) {
        if (imageFile != null && imageFile.exists()) {
            try {
                currentImagePath = "file:" + imageFile.getCanonicalPath();
            } catch (IOException e) {
                LogUtil.loge(e.getMessage());
            }
            return UriUtils.uriForFile(imageFile);
        }
        return null;
    }

    /**
     * getCameraIntent
     *
     * @param context Context
     * @param config  BaseConfig
     * @return Intent
     */
    @Override
    public Intent getCameraIntent(Context context, BaseConfig config) {
        prepareForNewIntent();
        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withBundleName(context.getBundleName())
                .withAbilityName(CameraPhotoAbility.class.getName())
                .build();
        Options options = Options.init()
                .setRequestCode(100)
                .setCount(5)
                .setFrontfacing(false)
                .setMode(Options.Mode.ALL)
                .setSpanCount(4)
                .setVideoDurationLimitinSeconds(10)
                .setScreenOrientation(AbilityInfo.DisplayOrientation.PORTRAIT)
                .setPath(ResourceUtil.getText(context, ResourceTable.String_path));
        intent.setOperation(operation);
        intent.setParam("options", options);
        return intent;
    }

    /**
     * getImage
     *
     * @param context            Context
     * @param intent             Intent
     * @param imageReadyListener OnImageReadyListener
     */
    @Override
    public void getImage(Context context, Intent intent, OnImageReadyListener imageReadyListener) {
        if (imageReadyListener == null) {
            throw new IllegalStateException("OnImageReadyListener must not be null");
        }
        if (currentImagePath == null) {
            imageReadyListener.onImageReady(null);
            return;
        }
        Uri imageUri = Uri.parse(currentImagePath);
        LogUtil.loge(imageUri.toString());
    }

    @Override
    public void removeImage() {
        if (TextTool.isNullOrEmpty(currentImagePath)) {
            return;
        }
        File file = new File(currentImagePath);
        if (file.exists()) {
            boolean delete = file.delete();
        }
    }
}