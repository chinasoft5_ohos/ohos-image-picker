package com.esafirm.imagepicker.features;

import com.esafirm.imagepicker.features.common.MvpView;
import com.esafirm.imagepicker.model.Folder;
import com.esafirm.imagepicker.model.Image;

import java.util.List;

/**
 * ImagePickerView
 */
public interface ImagePickerView extends MvpView {
    /**
     * isLoading
     *
     * @param isloading boolean
     */
    void isLoading(boolean isloading);

    /**
     * showLoading
     *
     * @param isLoading boolean
     */
    void showLoading(boolean isLoading);

    /**
     * showFetchCompleted
     *
     * @param images  List<Image>
     * @param folders List<Folder>
     */
    void showFetchCompleted(List<Image> images, List<Folder> folders);

    /**
     * showError
     *
     * @param throwable Throwable
     */
    void showError(Throwable throwable);

    /**
     * showEmpty
     */
    void showEmpty();

    /**
     * showCapturedImage
     */
    void showCapturedImage();

    /**
     * finishPickImages
     *
     * @param images List<Image>
     */
    void finishPickImages(List<Image> images);
}
