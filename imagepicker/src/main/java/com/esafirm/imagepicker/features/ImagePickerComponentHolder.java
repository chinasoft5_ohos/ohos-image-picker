package com.esafirm.imagepicker.features;

import com.esafirm.imagepicker.features.fileloader.DefaultImageFileLoader;
import com.esafirm.imagepicker.features.fileloader.ImageFileLoader;
import com.esafirm.imagepicker.features.imageloader.DefaultImageLoader;
import com.esafirm.imagepicker.features.imageloader.ImageLoader;

import ohos.app.Context;

/**
 * ImagePickerComponentHolder
 */
public class ImagePickerComponentHolder {
    private static ImagePickerComponentHolder INSTANCE;
    private Context context;
    private ImageLoader imageLoader;
    private ImageFileLoader imageFileLoader;
    private ImageLoader defaultImageLoader;
    private ImageFileLoader defaultImageFileLoader;

    /**
     * 单例
     *
     * @return ImagePickerComponentHolder
     */
    public static ImagePickerComponentHolder getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new ImagePickerComponentHolder();
        }
        return INSTANCE;
    }

    /**
     * init
     *
     * @param context Context
     */
    public void init(Context context) {
        this.context = context;
    }

    /**
     * getImageLoader
     *
     * @return ImageLoader
     */
    public ImageLoader getImageLoader() {
        if (imageLoader == null) {
            return getDefaultImageLoader();
        }
        return imageLoader;
    }

    /**
     * setImageLoader
     *
     * @param imageLoader ImageLoader
     */
    public void setImageLoader(ImageLoader imageLoader) {
        this.imageLoader = imageLoader;
    }

    /**
     * getImageFileLoader
     *
     * @return ImageFileLoader
     */
    public ImageFileLoader getImageFileLoader() {
        if (imageFileLoader == null) {
            return getDefaultImageFileLoader();
        }
        return imageFileLoader;
    }

    /**
     * setImageFileLoader
     *
     * @param imageFileLoader ImageFileLoader
     */
    public void setImageFileLoader(ImageFileLoader imageFileLoader) {
        this.imageFileLoader = imageFileLoader;
    }

    /**
     * getDefaultImageFileLoader
     *
     * @return ImageFileLoader
     */
    public ImageFileLoader getDefaultImageFileLoader() {
        if (defaultImageFileLoader == null) {
            defaultImageFileLoader = new DefaultImageFileLoader(context);
        }
        return defaultImageFileLoader;
    }

    /**
     * getDefaultImageLoader
     *
     * @return ImageLoader
     */
    public ImageLoader getDefaultImageLoader() {
        if (defaultImageLoader == null) {
            defaultImageLoader = new DefaultImageLoader();
        }
        return defaultImageLoader;
    }
}
